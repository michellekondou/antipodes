<?php
/**
 * Template used to display post content.
 *
 * @package storefront
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if ( get_post_type() == 'authors' ) : ?>

		<h2 class="entry-title"><a href="<?php esc_url( the_permalink() ); ?>" rel="bookmark"><?php the_field('author_first_name'); ?> <?php the_field('author_last_name'); ?></a></h2>

	<?php else : ?>
	
		<?php
		/**
		 * Functions hooked in to storefront_loop_post action.
		 *
		 * @hooked storefront_post_header          - 10
		 * @hooked storefront_post_content         - 30
		 */
		do_action( 'storefront_loop_post' );
		?>
	<?php endif; ?>

</article><!-- #post-## -->
