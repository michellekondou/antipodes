<?php

/* Template Name: News */ 

get_header(); ?>

	<div id="primary" class="content-area archive category">
		<main id="main" class="site-main" role="main">
		<div class="featured-post">
			<?php 
				$args = array( 
					'post_type'   => 'post',
					'posts_per_page' => 1
				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ) : 
			?>
				<?php while( $query->have_posts() ) : $query->the_post();
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					// are we on page one?
					$post_subtitle = get_field('post_subtitle');
				?>
				<?php if(1 == $paged) : ?>
				<!-- Display Post Here -->
				<?php $latest_post_id = get_the_ID(); ?>
				<figure class="post-thumbnail">
					<a href="<?php the_permalink()?>" title="<?php echo the_title(); ?>"><?php the_post_thumbnail(); ?></a>
				</figure>
				<div class="post-details flex">
					<header class="post-header">
						<h1><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h1>
						<h2><?php echo $post_subtitle; ?></h2>
						<a href="<?php the_permalink(); ?>" rel="bookmark" class="post-link arrow-link colored">
							Δείτε περισσότερα
							<svg class="icon icon-arrow-right-small-black">
								<use xlink:href="/wp-content/themes/antipodes/public/svg/symbols.svg#icon-arrow-right-small-black" />
							</svg>
						</a>
					</header>
					<article class="post-excerpt">
						<?php echo the_excerpt(); ?>
					</article>
				</div>
				<?php    	  
					wp_reset_query();
				?>
				<?php endif; ?>
				<?php endwhile; ?>
			<?php else : ?>
				<!-- Content If No Posts -->
			<?php endif ?>
			</div>
			<div class="horizontal-grid">
			<?php 
				$args = array( 
					'post_type' => 'post',
					'offset' => 1
				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ) : 
				?>
					<?php while( $query->have_posts() ) : $query->the_post() ?>
						<!-- Display Post Here -->
						<?php get_template_part( 'loop' ); ?>
						<?php    	  
							wp_reset_query();
						?>
					<?php endwhile ?>
				<?php else : ?>
					<!-- Content If No Posts -->
				<?php endif ?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
do_action( 'storefront_sidebar' );
get_footer();
