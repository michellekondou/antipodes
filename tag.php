<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package storefront
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php if ( get_post_type() == 'authors' ) : ?>
			<?php if ( have_posts() ) : ?>
				<div class="row">
				<?php
					$alpha_letter = '';
					/* Start the Loop */
					while ( have_posts() ) :
					the_post(); 
					$artist_name = get_the_title();
					$a_arr = explode(" ",$artist_name);
					$artist_first_name = isset($a_arr[1]) ? $a_arr[1] : '';
					$artist_first_name_arr = explode("-",$artist_first_name); 
					$artist_first_name = join(" ", $artist_first_name_arr);
					
					if (sizeof($a_arr) > 1) {
						$artist_first_name = $a_arr[1];
						$artist_first_name_arr = explode("-",$artist_first_name); 
						$artist_first_name = join(" ", $artist_first_name_arr);
					} else {
						$artist_first_name = '';
					}
					$artist_last_name = $a_arr[0];
					
					$letter = mb_substr( $artist_last_name, 0, 1, 'UTF-8' );
					$letter_ = mb_convert_case($letter, MB_CASE_UPPER, "UTF-8");

					$artist_extra = isset($a_arr[2]) ? ' ' . $a_arr[2] : '';
			
					if($alpha_letter != $letter_ )
					{
						
						if ($alpha_letter != '' ) {
							echo '';
						} 
						if($letter_ === "Ί" ) {
							$alpha_letter = $letter_;
						} else {
							$alpha_letter = $letter_;
							echo '<h4>'.$letter_.'</h4>';
						}
						
					}
				?>
				<h2 class="entry-title"><a href="<?php esc_url( the_permalink() ); ?>" rel="bookmark"><?php the_field('author_first_name'); ?> <?php the_field('author_last_name'); ?></a></h2>
				<?php 
					endwhile; 
					/**
					 * Functions hooked in to storefront_paging_nav action
					 *
					 * @hooked storefront_paging_nav - 10
					 */
					do_action( 'storefront_loop_after' );
				
				?>
				</div>
			<?php endif; ?>
		<?php elseif ( get_post_type() == 'anagnoseis' ) : ?>	
			<?php if ( have_posts() ) : ?>
				<div class="row anagnoseis-list">
				<?php
					/* Start the Loop */
					while ( have_posts() ) :
					the_post(); 
				?>
					<article class="anagnoseis-item">
						<a href="<?php esc_url( the_permalink() ); ?>" rel="bookmark">
							<h2 class="entry-title"><?php the_title(); ?></h2>
							<?php the_excerpt(); ?>
							<svg class="icon icon-arrow-right-small-white">
								<use xlink:href="/wp-content/themes/antipodes/public/svg/symbols.svg#icon-arrow-right-small-white" />
							</svg>
						</a>
					</article>
					<?php 
						endwhile; 
						/**
						 * Functions hooked in to storefront_paging_nav action
						 *
						 * @hooked storefront_paging_nav - 10
						 */
						do_action( 'storefront_loop_after' );
					
					?>
				</div>
			<?php endif; ?>
		<?php else : ?>
		<?php
			$latest_post_id = '';
			$args = array(
				'posts_per_page' => 1,
				'post_type' => 'post'
			);
			$query = new WP_Query( $args );
		?>
		<?php if ( $query->have_posts()) : ?>
		<?php while( $query->have_posts() ) : ?>
		<?php
			$query->the_post();
			// get current page we are on. If not set we can assume we are on page 1.
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			// are we on page one?
			$post_subtitle = get_field('post_subtitle');
			if(1 == $paged) : ?>			
			<div class="featured-post">
			<?php $latest_post_id = get_the_ID(); ?>
				<figure class="post-thumbnail">
					<a href="<?php the_permalink()?>" title="<?php echo the_title(); ?>"><?php the_post_thumbnail(); ?></a>
				</figure>
				<div class="post-details flex">
					<header class="post-header">
						<h1><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h1>
						<h2><?php echo $post_subtitle; ?></h2>
						<a href="<?php the_permalink(); ?>" rel="bookmark" class="post-link arrow-link colored">
							Δείτε περισσότερα
							<svg class="icon icon-arrow-right-small-black">
								<use xlink:href="/wp-content/themes/antipodes/public/svg/symbols.svg#icon-arrow-right-small-black" />
							</svg>
						</a>
					</header>
					<article class="post-excerpt">
						<?php echo the_excerpt(); ?>
					</article>
				</div>
			</div>
			
			<?php endif; ?>
			<?php    	  
				wp_reset_query();
			?>
			<?php endwhile; ?>	
			<?php endif; ?>
			<?php if ( have_posts() ) : ?>
			<div class="horizontal-grid">
			<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post(); 
			?>
				<?php if (get_the_ID() !== $latest_post_id) : ?>
				<?php get_template_part( 'loop' ); ?>
				<?php endif; ?>
			<?php endwhile; ?>

			<?php else :

				get_template_part( 'content', 'none' );

			endif;
			?>
			</div>
		<?php endif; ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
do_action( 'storefront_sidebar' );
get_footer();
