
const path = require('path');

module.exports = {
    plugins: {
        'autoprefixer': {},
        'cssnano': { preset: 'default' },
        'postcss-easings': {
            easings: { easeJump: 'cubic-bezier(.86,0,.69,1.57)' }
        },
        'postcss-extract-media-query': {
            entry: path.join(__dirname, 'dist/assets/app.css'),
            output: {
                path: path.join(__dirname, 'dist/')
            },
            queries: 'screen and (min-width: 768px)'
        }
    }
};