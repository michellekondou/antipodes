<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>
<header class="woocommerce-products-header">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
	<?php endif; ?>

	<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' );
	?>
	<div class="book-layout-menu">
        <div class="layout-switch">
			<a href="/vivlia/?orderby=date" class="button--circle layout-switch--spines selected">
				<svg class="button-spines" width="113px" height="113px" viewBox="0 0 113 113">
					<title>button-spine</title>
					<defs>
						<circle id="path-1-button-spines" cx="33.5" cy="33.5" r="33.5"></circle>
						<filter x="-53.7%" y="-53.7%" width="207.5%" height="207.5%" filterUnits="objectBoundingBox" id="filter-2-button-spines">
							<feOffset dx="0" dy="0" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
							<feGaussianBlur stdDeviation="12" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
							<feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
						</filter>
						<polygon id="path-3-button-spines" points="0.20004 0.0810936709 27.2 0.0810936709 27.2 6.2380557 0.20004 6.2380557"></polygon>
						<polygon id="path-5-button-spines" points="0.20016 0.121559494 27.2 0.121559494 27.2 6.27852152 0.20016 6.27852152"></polygon>
						<polygon id="path-7-button-spines" points="0.20016 0.162106329 27.2 0.162106329 27.2 6.31906835 0.20016 6.31906835"></polygon>
					</defs>
					<g id="Symbols-button-spines" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<g id="button-spines-button-spines" transform="translate(3.000000, 3.000000)">
							<g id="button-list-button-spines" transform="translate(20.000000, 20.000000)">
								<g id="bg-button-spines">
									<use fill="black" fill-opacity="1" filter="url(#filter-2-button-spines)" xlink:href="#path-1-button-spines"></use>
									<use class="circle" fill="#FFFFFF" fill-rule="evenodd" xlink:href="#path-1-button-spines"></use>
								</g>
								<g id="icon-spines-button-spines" transform="translate(23.000000, 20.000000)">
									<g class="line" id="Group-5-button-spines" transform="translate(-1.445871, 13.878481) rotate(-90.000000) translate(1.445871, -13.878481) translate(-15.045871, 10.637975)">
										<mask id="mask-4-button-spines" fill="white">
											<use xlink:href="#path-3-button-spines"></use>
										</mask>
										<g id="Clip-4-button-spines"></g>
										<path d="M3.24004,6.2380557 L24.16004,6.2380557 C25.84004,6.2380557 27.20004,4.86084051 27.20004,3.15957468 C27.20004,1.45830886 25.84004,0.0810936709 24.16004,0.0810936709 L3.24004,0.0810936709 C1.56004,0.0810936709 0.20004,1.45830886 0.20004,3.15957468 C0.24004,4.86084051 1.60004,6.2380557 3.24004,6.2380557" id="Fill-3-button-spines" fill="#000000" mask="url(#mask-4-button-spines)"></path>
									</g>
									<g class="line" id="Group-10-button-spines" transform="translate(10.706028, 13.878481) rotate(-90.000000) translate(-10.706028, -13.878481) translate(-2.893972, 10.637975)">
										<mask id="mask-6-button-spines" fill="white">
											<use xlink:href="#path-5-button-spines"></use>
										</mask>
										<g id="Clip-9-button-spines"></g>
										<path d="M24.16016,0.121559494 L3.24016,0.121559494 C1.56016,0.121559494 0.20016,1.49877468 0.20016,3.20004051 C0.20016,4.90130633 1.56016,6.27852152 3.24016,6.27852152 L24.16016,6.27852152 C25.83976,6.27852152 27.20016,4.90130633 27.20016,3.20004051 C27.20016,1.49877468 25.83976,0.121559494 24.16016,0.121559494" id="Fill-8-button-spines" fill="#000000" mask="url(#mask-6-button-spines)"></path>
									</g>
									<g class="line" id="Group-15-button-spines" transform="translate(22.857927, 13.878481) rotate(-90.000000) translate(-22.857927, -13.878481) translate(9.257927, 10.637975)">
										<mask id="mask-8-button-spines" fill="white">
											<use xlink:href="#path-7-button-spines"></use>
										</mask>
										<g id="Clip-14-button-spines"></g>
										<path d="M24.16016,0.162106329 L3.24016,0.162106329 C1.56016,0.162106329 0.20016,1.53891646 0.20016,3.24058734 C0.20016,4.94185316 1.56016,6.31906835 3.24016,6.31906835 L24.16016,6.31906835 C25.83976,6.31906835 27.20016,4.94185316 27.20016,3.24058734 C27.20016,1.53891646 25.83976,0.162106329 24.16016,0.162106329" id="Fill-13-button-spines" fill="#000000" mask="url(#mask-8-button-spines)"></path>
									</g>
								</g>
							</g>
						</g>
					</g>
				</svg>
			</a>
            <a href="/vivlia/?orderby=date" class="button--circle layout-switch--thumbs">
				<svg class="button-thumbs" width="113px" height="113px" viewBox="0 0 113 113">
					<title>button-thumbs</title>
					<defs>
						<circle id="path-1-button-thumbs" cx="33.5" cy="33.5" r="33.5"></circle>
						<filter x="-53.7%" y="-53.7%" width="207.5%" height="207.5%" filterUnits="objectBoundingBox" id="filter-2-button-thumbs">
							<feOffset dx="0" dy="0" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
							<feGaussianBlur stdDeviation="12" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
							<feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
						</filter>
					</defs>
					<g id="Symbols-button-thumbs" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<g id="button-thumbs-button-thumbs" transform="translate(3.000000, 3.000000)">
							<g id="button-list-button-thumbs" transform="translate(20.000000, 20.000000)">
								<g id="bg-button-thumbs">
									<use fill="black" fill-opacity="1" filter="url(#filter-2-button-thumbs)" xlink:href="#path-1-button-thumbs"></use>
									<use id="circle_ap" fill="#FFFFFF" fill-rule="evenodd" xlink:href="#path-1-button-thumbs"></use>
								</g>
								<g class="dots" id="icon-thumbs-1" transform="translate(18.000000, 18.000000)" fill="#000000">
									<path d="M3.6,4.05063291e-05 C1.6,4.05063291e-05 0,1.62029367 0,3.64561013 C0,5.67092658 1.6,7.29117975 3.6,7.29117975 C5.6,7.29117975 7.2,5.67092658 7.2,3.64561013 C7.2,1.62029367 5.56,4.05063291e-05 3.6,4.05063291e-05" id="Fill-1-button-thumbs-1"></path>
									<path d="M3.6,12.1924861 C1.6,12.1924861 0,13.8127392 0,15.8380557 C0,17.8633722 1.6,19.4836253 3.6,19.4836253 C5.6,19.4836253 7.2,17.8633722 7.2,15.8380557 C7.2,13.8127392 5.56,12.1924861 3.6,12.1924861" id="Fill-6-button-thumbs-1"></path>
									<path d="M3.6,24.3847291 C5.5884,24.3847291 7.2,26.0167291 7.2,28.0302987 C7.2,30.0438684 5.5884,31.6758684 3.6,31.6758684 C1.6116,31.6758684 0,30.0438684 0,28.0302987 C0,26.0167291 1.6116,24.3847291 3.6,24.3847291" id="Fill-11-button-thumbs-1"></path>
								</g>
								<g class="dots" id="icon-thumbs-2" transform="translate(30.000000, 18.000000)" fill="#000000">
									<path d="M3.6,4.05063291e-05 C1.6,4.05063291e-05 0,1.62029367 0,3.64561013 C0,5.67092658 1.6,7.29117975 3.6,7.29117975 C5.6,7.29117975 7.2,5.67092658 7.2,3.64561013 C7.2,1.62029367 5.56,4.05063291e-05 3.6,4.05063291e-05" id="Fill-1-button-thumbs-2"></path>
									<path d="M3.6,12.1924861 C1.6,12.1924861 0,13.8127392 0,15.8380557 C0,17.8633722 1.6,19.4836253 3.6,19.4836253 C5.6,19.4836253 7.2,17.8633722 7.2,15.8380557 C7.2,13.8127392 5.56,12.1924861 3.6,12.1924861" id="Fill-6-button-thumbs-2"></path>
									<path d="M3.6,24.3847291 C5.5884,24.3847291 7.2,26.0167291 7.2,28.0302987 C7.2,30.0438684 5.5884,31.6758684 3.6,31.6758684 C1.6116,31.6758684 0,30.0438684 0,28.0302987 C0,26.0167291 1.6116,24.3847291 3.6,24.3847291" id="Fill-11-button-thumbs-2"></path>
								</g>
								<g class="dots" id="icon-thumbs-3" transform="translate(42.000000, 18.000000)" fill="#000000">
									<path d="M3.6,4.05063291e-05 C1.6,4.05063291e-05 0,1.62029367 0,3.64561013 C0,5.67092658 1.6,7.29117975 3.6,7.29117975 C5.6,7.29117975 7.2,5.67092658 7.2,3.64561013 C7.2,1.62029367 5.56,4.05063291e-05 3.6,4.05063291e-05" id="Fill-1-button-thumbs-3"></path>
									<path d="M3.6,12.1924861 C1.6,12.1924861 0,13.8127392 0,15.8380557 C0,17.8633722 1.6,19.4836253 3.6,19.4836253 C5.6,19.4836253 7.2,17.8633722 7.2,15.8380557 C7.2,13.8127392 5.56,12.1924861 3.6,12.1924861" id="Fill-6-button-thumbs-3"></path>
									<path d="M3.6,24.3847291 C5.5884,24.3847291 7.2,26.0167291 7.2,28.0302987 C7.2,30.0438684 5.5884,31.6758684 3.6,31.6758684 C1.6116,31.6758684 0,30.0438684 0,28.0302987 C0,26.0167291 1.6116,24.3847291 3.6,24.3847291" id="Fill-11-button-thumbs-3"></path>
								</g>
							</g>
						</g>
					</g>
				</svg>
			</a>
            <a href="/vivlia/?orderby=oldest_to_recent" class="button--circle layout-switch--list">
				<svg class="button-list" width="113px" height="113px" viewBox="0 0 113 113">
					<title>button-list</title>
					<defs>
						<circle id="path-1-button-list" cx="33.5" cy="33.5" r="33.5"></circle>
						<filter x="-53.7%" y="-53.7%" width="207.5%" height="207.5%" filterUnits="objectBoundingBox" id="filter-2-button-list">
							<feOffset dx="0" dy="0" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
							<feGaussianBlur stdDeviation="12" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
							<feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
						</filter>
						<polygon id="path-3-button-list" points="0.20004 0.0810936709 27.2 0.0810936709 27.2 6.2380557 0.20004 6.2380557"></polygon>
						<polygon id="path-5-button-list" points="0.20016 0.121559494 27.2 0.121559494 27.2 6.27852152 0.20016 6.27852152"></polygon>
						<polygon id="path-7-button-list" points="0.20016 0.162106329 27.2 0.162106329 27.2 6.31906835 0.20016 6.31906835"></polygon>
					</defs>
					<g id="Symbols-button-list" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<g id="button-list-1" transform="translate(3.000000, 3.000000)">
							<g transform="translate(20.000000, 20.000000)">
								<g id="bg-button-list">
									<use fill="black" fill-opacity="1" filter="url(#filter-2-button-list)" xlink:href="#path-1-button-list"></use>
									<use class="circle" fill="#FFFFFF" fill-rule="evenodd" xlink:href="#path-1-button-list"></use>
								</g>
								<g id="icon-list" transform="translate(14.000000, 18.000000)">
									<path class="circle" d="M3.6,4.05063291e-05 C1.6,4.05063291e-05 0,1.62029367 0,3.64561013 C0,5.67092658 1.6,7.29117975 3.6,7.29117975 C5.6,7.29117975 7.2,5.67092658 7.2,3.64561013 C7.2,1.62029367 5.56,4.05063291e-05 3.6,4.05063291e-05" id="Fill-1" fill="#000000"></path>
									<g class="line" id="Group-5-button-list" transform="translate(10.800000, 0.486076)">
										<mask id="mask-4-button-list" fill="white">
											<use xlink:href="#path-3-button-list"></use>
										</mask>
										<g id="Clip-4-button-list"></g>
										<path d="M3.24004,6.2380557 L24.16004,6.2380557 C25.84004,6.2380557 27.20004,4.86084051 27.20004,3.15957468 C27.20004,1.45830886 25.84004,0.0810936709 24.16004,0.0810936709 L3.24004,0.0810936709 C1.56004,0.0810936709 0.20004,1.45830886 0.20004,3.15957468 C0.24004,4.86084051 1.60004,6.2380557 3.24004,6.2380557" id="Fill-3-button-list" fill="#000000" mask="url(#mask-4-button-list)"></path>
									</g>
									<path class="circle" d="M3.6,12.1924861 C1.6,12.1924861 0,13.8127392 0,15.8380557 C0,17.8633722 1.6,19.4836253 3.6,19.4836253 C5.6,19.4836253 7.2,17.8633722 7.2,15.8380557 C7.2,13.8127392 5.56,12.1924861 3.6,12.1924861" id="Fill-6-button-list" fill="#000000"></path>
									<g class="line" id="Group-10-button-list" transform="translate(10.800000, 12.637975)">
										<mask id="mask-6-button-list" fill="white">
											<use xlink:href="#path-5-button-list"></use>
										</mask>
										<g id="Clip-9-button-list"></g>
										<path d="M24.16016,0.121559494 L3.24016,0.121559494 C1.56016,0.121559494 0.20016,1.49877468 0.20016,3.20004051 C0.20016,4.90130633 1.56016,6.27852152 3.24016,6.27852152 L24.16016,6.27852152 C25.83976,6.27852152 27.20016,4.90130633 27.20016,3.20004051 C27.20016,1.49877468 25.83976,0.121559494 24.16016,0.121559494" id="Fill-8-button-list" fill="#000000" mask="url(#mask-6-button-list)"></path>
									</g>
									<path class="circle" d="M3.6,24.3847291 C5.5884,24.3847291 7.2,26.0167291 7.2,28.0302987 C7.2,30.0438684 5.5884,31.6758684 3.6,31.6758684 C1.6116,31.6758684 0,30.0438684 0,28.0302987 C0,26.0167291 1.6116,24.3847291 3.6,24.3847291" id="Fill-11-button-list" fill="#000000"></path>
									<g class="line" id="Group-15-button-list" transform="translate(10.800000, 24.789873)">
										<mask id="mask-8-button-list" fill="white">
											<use xlink:href="#path-7-button-list"></use>
										</mask>
										<g id="Clip-14-button-list"></g>
										<path d="M24.16016,0.162106329 L3.24016,0.162106329 C1.56016,0.162106329 0.20016,1.53891646 0.20016,3.24058734 C0.20016,4.94185316 1.56016,6.31906835 3.24016,6.31906835 L24.16016,6.31906835 C25.83976,6.31906835 27.20016,4.94185316 27.20016,3.24058734 C27.20016,1.53891646 25.83976,0.162106329 24.16016,0.162106329" id="Fill-13-button-list" fill="#000000" mask="url(#mask-8-button-list)"></path>
									</g>
								</g>
							</g>
						</g>
					</g>
				</svg>
			</a>
		</div>
		<aside class="sorting-menu">
			<h3>Ταξινόμηση</h3>
			<ul class="sorting-options">
				<li>
					<a href="/vivlia/?orderby=date" class="newest-to-oldest selected">Νεότερο → Παλαιότερο</a>
				</li>
				<li>
					<a href="/vivlia/?orderby=oldest_to_recent" class="oldest-to-newest">Παλαιότερο → Νεότερο</a>
				</li>
				<li>
					<a href="/vivlia/?orderby=author" class="by-author">Ανά συγγραφέα</a>
				</li>
			</ul>
		</aside>
		
    </div>
</header>
<svg class="loader" width="48px" height="125px" viewBox="0 0 48 125">
    <title>Loading</title>
    <g id="Artboard" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Group" transform="translate(5.000000, 4.000000)">
            <circle id="Oval" fill="#000000" cx="19" cy="58" r="19"></circle>
            <path d="M19.2333333,1.23333333 L19.2333333,115.690698" id="Line-3-loader" stroke="#000000" stroke-width="6" transform="translate(19.233333, 57.966667) rotate(-180.000000) translate(-19.233333, -57.966667) "></path>
        </g>
    </g>
</svg>

<?php
if ( woocommerce_product_loop() ) {
	echo '<section class="products-list visibility-hidden">';
	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	//do_action( 'woocommerce_before_shop_loop' );

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 *
			 * @hooked WC_Structured_Data::generate_product_data() - 10
			 */
			do_action( 'woocommerce_shop_loop' );
		
			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
	echo '</section>';
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );
