<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

?>
<li <?php wc_product_class(); ?>>
	<?php
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	//do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	//do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	//o_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	//do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	//do_action( 'woocommerce_after_shop_loop_item' );
	?>

	<?php 
		$book_first_edition = get_field('book_first_edition'); //Text
		$book_spine_image = get_field('book_spine_image_horizontal');
		$book_author = get_field('book_author');
		if ( has_post_thumbnail() ) {
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full');
			$image_s = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'thumbnail');
			$image_m = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium');
			$image_ml = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium_large');
			$image_l = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large');
			$thumb_id = get_post_thumbnail_id();
		}
	?>
	<div class="row">
		<a href="<?php the_permalink(); ?>" class="book__details" rel="bookmark">
			<h2 class="book__title"><?php the_title(); ?></h2>
			<?php
				if ($book_author) : ?>
			<h2 class="book__author">
			<?php
				foreach( $book_author as $post) :
				setup_postdata($post);
				$author_first_name = get_field('author_first_name');
				$author_last_name = get_field('author_last_name');
			?>
			<?php echo $author_first_name . ' ' . $author_last_name; ?>
			<?php endforeach; ?>
			</h2>
			<?php endif; 
				wp_reset_postdata(); 
			?>
			<p class="book__first-edition"><?php echo date('Y', strtotime($book_first_edition)); ?></p>
		</a>
		<?php if ( has_post_thumbnail() ) : ?>
		<a href="<?php the_permalink(); ?>" class="post-thumbnail__container">
			<figure class="thumbnail post-thumbnail">
				<img
					data-src="<?php echo $image_ml[0]; ?>"
					src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
					class="attachment-post-thumbnail size-post-thumbnail wp-post-image lazyload"
					alt="<?php the_title(); ?>">
				<figcaption class="visually-hidden"><?php the_title( '<h2 class="entry-title">', '</h2>' ); ?></figcaption>
			</figure>
		</a>
		<?php else : ?>
		<a href="<?php the_permalink(); ?>" class="post-thumbnail__container">
			<img 
				data-src="https://antipodes.cg-dev.eu/wp-content/plugins/woocommerce/assets/images/placeholder.png"
				src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" 
				alt="Δείκτης τοποθέτησης" 
				class="attachment-post-thumbnail size-post-thumbnail wp-post-image lazyload">
		</a>
		<?php endif; ?>
		<?php if($book_spine_image) : ?>
		<a href="<?php the_permalink(); ?>" class="book__spine-image" aria-expanded="false" aria-controls="section-<?php echo $product->get_id(); ?>" id="accordion-<?php echo $product->get_id(); ?>-id">
			<picture class="lazyload">
				<source media="(max-width: 1439px)" data-srcset="<?php echo $book_spine_image['sizes']['spine_horizontal_md']; ?>" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
				<source media="(min-width: 1440px)" data-srcset="<?php echo $book_spine_image['url']; ?>" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
				<img src="<?php echo $book_spine_image['url']; ?>" alt="<?php the_title(); ?>" width="<?php echo $book_spine_image["width"]; ?>" class="attachment-post-thumbnail size-post-thumbnail wp-post-image lazyload test">
			</picture>
		</a>
		<article class="book__spine-details" id="section-<?php echo $product->get_id(); ?>" aria-labelledby="accordion-<?php echo $product->get_id(); ?>-id" role="region" data-collapsed="true" aria-hidden="true">
        <a href="<?php the_permalink(); ?>">
			<figure class="thumbnail post-thumbnail">
				<?php 
					if ( has_post_thumbnail() ) :
				?>
					<img
						data-src="<?php echo $image_ml[0]; ?>"
						src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
						class="attachment-post-thumbnail size-post-thumbnail wp-post-image lazyload"
						alt="<?php the_title(); ?>">
					<?php else : ?>
					<img 
						data-src="https://antipodes.cg-dev.eu/wp-content/plugins/woocommerce/assets/images/placeholder.png" 
						src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
						alt="Placeholder image" 
						class="wp-post-image attachment-post-thumbnail size-post-thumbnail lazyload">
					<?php endif; ?>
					<figcaption class="visually-hidden"><?php the_title( '<h2 class="entry-title">', '</h2>' ); ?></figcaption>
			</figure>
				</a>
			<div>
				<?php echo the_excerpt(); ?>
				<a href="<?php the_permalink(); ?>" rel="bookmark" class="post-link arrow-link">
					Επισκεφθείτε τη σελίδα του βιβλίου 
					<svg class="icon icon-arrow-right-small-black">
						<use xlink:href="/wp-content/themes/antipodes/public/svg/symbols.svg#icon-arrow-right-small-black" />
					</svg>
				</a>
			</div>
		</article>
		<?php endif; ?>
	</div>
</li>
