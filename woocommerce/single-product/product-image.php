<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$post_thumbnail_id = $product->get_image_id();
$wrapper_classes   = apply_filters( 'woocommerce_single_product_image_gallery_classes', array(
	'woocommerce-product-gallery',
	'woocommerce-product-gallery--' . ( $product->get_image_id() ? 'with-images' : 'without-images' ),
	'woocommerce-product-gallery--columns-' . absint( $columns ),
	'images',
) );

$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_ID() ), 'full');
$image_s = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_ID() ), 'thumbnail');
$image_m = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_ID()), 'medium');
$image_ml = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_ID() ), 'medium_large');
$image_l = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_ID()), 'large');
$thumb_id = get_post_thumbnail_id();
//var_dump($image_l);
?>

<div class="<?php echo esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) ); ?>" data-columns="<?php echo esc_attr( $columns ); ?>" style="opacity: 0; transition: opacity .25s ease-in-out;">
	<figure class="woocommerce-product-gallery__wrapper">
		<picture class="lazyload">
			<source media="(max-width: 1439px)" data-srcset="<?php echo $image_ml[0];?>" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
			<source media="(min-width: 1440px)" data-srcset="<?php echo $image_l[0]; ?>" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
			<img src="<?php echo $image_l[0]; ?>" alt="<?php the_title(); ?>" class="attachment-post-thumbnail size-post-thumbnail wp-post-image lazyload test">
		</picture>
	</figure>
</div>

