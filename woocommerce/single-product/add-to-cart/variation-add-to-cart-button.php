<?php
/**
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
?>
<div class="woocommerce-variation-add-to-cart variations_button">
	<?php //do_action( 'woocommerce_before_add_to_cart_button' ); ?>

	<?php
	//do_action( 'woocommerce_before_add_to_cart_quantity' );

	//do_action( 'woocommerce_after_add_to_cart_quantity' );
	?>

	<button type="submit" class="single_add_to_cart_button">
		<span class="product-price"><?php echo $product->get_price_html(); ?></span>
		<svg width="143px" height="143px" viewBox="0 0 143 143" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<title></title>
			<defs>
				<circle id="path-1" cx="48.5" cy="48.5" r="48.5"></circle>
				<filter x="-37.1%" y="-37.1%" width="174.2%" height="174.2%" filterUnits="objectBoundingBox" id="filter-2">
					<feOffset dx="0" dy="0" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
					<feGaussianBlur stdDeviation="12" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
					<feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
				</filter>
			</defs>
			<g id="Symbols_buy" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				<g id="button-αγορά" transform="translate(-5.000000, -5.000000)">
					<g id="button-buy-large" transform="translate(28.000000, 28.000000)">
						<g id="bg">
							<use fill="black" fill-opacity="1" filter="url(#filter-2)" xlink:href="#path-1"></use>
							<use fill="#000000" fill-rule="evenodd" xlink:href="#path-1"></use>
						</g>
						<text x="20" y="71.5" font-family="sans-serif" font-size="20px" fill="red"></text>
						<path id="svg-buy-text" d="M29.652,53.24 C27.668,53.24 26.196,51.56 26.196,49.368 C26.196,47.192 27.668,45.512 29.652,45.512 C30.644,45.512 31.396,45.944 31.812,46.536 L31.812,45.752 L34.164,45.752 L34.164,53 L31.812,53 L31.812,52.232 C31.396,52.808 30.644,53.24 29.652,53.24 Z M30.212,51.16 C31.204,51.16 31.812,50.408 31.812,49.368 C31.812,48.344 31.204,47.56 30.212,47.56 C29.22,47.56 28.532,48.344 28.532,49.368 C28.532,50.408 29.204,51.16 30.212,51.16 Z M43.54,45.752 L40.548,53.016 L40.548,56.728 L38.196,56.728 L38.196,53.016 L35.22,45.752 L37.812,45.752 L39.428,50.056 L40.996,45.752 L43.54,45.752 Z M43.972,49.368 C43.972,47.112 45.748,45.512 47.94,45.512 C50.116,45.512 51.892,47.112 51.892,49.368 C51.892,51.608 50.116,53.24 47.94,53.24 C45.748,53.24 43.972,51.608 43.972,49.368 Z M46.308,49.368 C46.308,50.424 46.964,51.176 47.94,51.176 C48.916,51.176 49.556,50.424 49.556,49.368 C49.556,48.328 48.916,47.56 47.94,47.56 C46.964,47.56 46.308,48.328 46.308,49.368 Z M57.22,47.56 C56.244,47.56 55.604,48.344 55.604,49.384 C55.604,50.424 56.196,51.16 57.204,51.16 C58.212,51.16 58.852,50.408 58.852,49.368 C58.852,48.328 58.212,47.56 57.22,47.56 Z M57.22,45.512 C59.412,45.512 61.188,47.112 61.188,49.368 C61.188,51.56 59.748,53.24 57.764,53.24 C56.788,53.24 56.036,52.856 55.604,52.28 L55.604,56.728 L53.268,56.728 L53.268,49.368 C53.268,47.112 55.044,45.512 57.22,45.512 Z M65.86,53.24 C63.876,53.24 62.404,51.56 62.404,49.368 C62.404,47.192 63.876,45.512 65.86,45.512 C66.852,45.512 67.604,45.944 68.02,46.536 L68.02,45.752 L70.372,45.752 L70.372,53 L68.02,53 L68.02,52.232 C67.604,52.808 66.852,53.24 65.86,53.24 Z M66.42,51.16 C67.412,51.16 68.02,50.408 68.02,49.368 C68.02,48.344 67.412,47.56 66.42,47.56 C65.428,47.56 64.74,48.344 64.74,49.368 C64.74,50.408 65.412,51.16 66.42,51.16 Z M67.364,44.616 L65.716,44.616 L66.58,41.8 L68.996,41.8 L67.364,44.616 Z" fill="#FFFFFF" fill-rule="nonzero"></path>
					</g>
				</g>
			</g>
		</svg>
		<span class="screen-reader-text"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></span>
	</button>

	<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

	<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="variation_id" class="variation_id" value="0" />
</div>
