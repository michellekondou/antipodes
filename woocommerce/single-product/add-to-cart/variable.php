<?php
/**
 * Variable product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/variable.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.5
 */

defined( 'ABSPATH' ) || exit;

global $product;

$attribute_keys  = array_keys( $attributes );
$variations_json = wp_json_encode( $available_variations );
$variations_attr = function_exists( 'wc_esc_json' ) ? wc_esc_json( $variations_json ) : _wp_specialchars( $variations_json, ENT_QUOTES, 'UTF-8', true );

do_action( 'woocommerce_before_add_to_cart_form' ); ?>

<form class="variations_form cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product->get_id() ); ?>" data-product_variations="<?php echo $variations_attr; // WPCS: XSS ok. ?>">
	<?php do_action( 'woocommerce_before_variations_form' ); ?>

	<?php if ( empty( $available_variations ) && false !== $available_variations ) : ?>
		<p class="stock out-of-stock"><?php esc_html_e( 'This product is currently out of stock and unavailable.', 'woocommerce' ); ?></p>
	<?php else : ?>
		<table class="variations">
			<tbody>
				<?php foreach ( $attributes as $attribute_name => $options ) : ?>
					<tr>
						<td class="label"><label for="<?php echo esc_attr( sanitize_title( $attribute_name ) ); ?>"><?php echo wc_attribute_label( $attribute_name ); // WPCS: XSS ok. ?></label></td>
						<td class="value">
							<?php
								wc_dropdown_variation_attribute_options( array(
									'options'   => $options,
									'attribute' => $attribute_name,
									'product'   => $product,
								) );
								echo end( $attribute_keys ) === $attribute_name ? wp_kses_post( apply_filters( 'woocommerce_reset_variations_link', '<a class="reset_variations" href="#">' . esc_html__( 'Clear', 'woocommerce' ) . '</a>' ) ) : '';
							?>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

		<div class="single_variation_wrap">
			<?php
				/**
				 * Hook: woocommerce_before_single_variation.
				 */
				do_action( 'woocommerce_before_single_variation' );

				/**
				 * Hook: woocommerce_single_variation. Used to output the cart button and placeholder for variation data.
				 *
				 * @since 2.4.0
				 * @hooked woocommerce_single_variation - 10 Empty div for variation data.
				 * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.
				 */
				do_action( 'woocommerce_single_variation' );

				/**
				 * Hook: woocommerce_after_single_variation.
				 */
				do_action( 'woocommerce_after_single_variation' );
			?>
			<button type="button" class="button--read-sample tooltip" data-tippy-content="Διαβάστε ένα απόσπασμα">
				<svg width="113px" height="113px" viewBox="0 0 113 113">
					<title></title>
					<defs>
						<circle id="path-1-button-read_sample" cx="76.5" cy="33.5" r="33.5"></circle>
						<filter x="-53.7%" y="-53.7%" width="207.5%" height="207.5%" filterUnits="objectBoundingBox" id="filter-2-button-read_sample">
							<feOffset dx="0" dy="0" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
							<feGaussianBlur stdDeviation="12" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
							<feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
						</filter>
						<polygon id="path-3-button-read_sample" points="0 0 40 0 40 28 0 28"></polygon>
					</defs>
					<g id="Symbols-button-read_sample" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<g id="button-button-read_sample" transform="translate(-20.000000, -20.000000)">
							<g transform="translate(0.000000, 43.000000)">
								<g id="bg-button-read_sample">
									<use fill="black" fill-opacity="1" filter="url(#filter-2-button-read_sample)" xlink:href="#path-1-button-read_sample"></use>
									<use fill="#B1B1B1" fill-rule="evenodd" xlink:href="#path-1-button-read_sample"></use>
								</g>
								<g id="icon-read-button-read_sample" transform="translate(57.000000, 20.000000)">
									<mask id="mask-4-button-read_sample" fill="white">
										<use xlink:href="#path-3-button-read_sample"></use>
									</mask>
									<g id="Clip-2-button-read_sample"></g>
									<path d="M31.1110948,25.6656122 C27.4288773,25.6656122 24.4444366,22.5320743 24.4444366,18.6658998 C24.4444366,14.7997253 27.4288773,11.6661874 31.1110948,11.6661874 C34.7933123,11.6661874 37.777753,14.7997253 37.777753,18.6658998 C37.7733085,22.529741 34.7910901,25.6609457 31.1110948,25.6656122 L31.1110948,25.6656122 Z M8.88890089,25.6656122 C5.20668337,25.6656122 2.22224273,22.5320743 2.22224273,18.6658998 C2.22224273,14.7997253 5.20668337,11.6661874 8.88890089,11.6661874 C12.5711184,11.6661874 15.5555591,14.7997253 15.5555591,18.6658998 C15.5511146,22.529741 12.5688962,25.6609457 8.88890089,25.6656122 L8.88890089,25.6656122 Z M39.9999723,5.83309368 C39.9955279,2.61322597 37.5110866,0.00233323747 34.4444239,0 L32.2222045,0 C31.6088719,0 31.1110948,0.522645194 31.1110948,1.16661874 C31.1110948,1.81059228 31.6088719,2.33323747 32.2222045,2.33323747 L34.4444239,2.33323747 C36.2844215,2.33323747 37.7755307,3.90117305 37.777753,5.83309368 L37.777753,12.5504844 C34.597757,8.67497692 29.0288752,8.23866151 25.3377688,11.5751911 C23.6666598,13.0847957 22.5955501,15.1987089 22.3355504,17.499281 L17.6666675,17.499281 C17.0888904,12.4198231 12.7022294,8.79397203 7.86445776,9.39828054 C5.67334944,9.67126932 3.66001867,10.7958898 2.22224273,12.5504844 L2.22224273,5.83309368 C2.22446495,3.90117305 3.71557416,2.33323747 5.55557181,2.33323747 L7.7777912,2.33323747 C8.39112375,2.33323747 8.88890089,1.81059228 8.88890089,1.16661874 C8.88890089,0.522645194 8.39112375,0 7.7777912,0 L5.55557181,0 C2.48890905,0.00233323747 0.00224555727,2.61322597 2.33378848e-05,5.83309368 L2.33378848e-05,18.6658998 C-0.0110877591,23.8060219 3.94668497,27.9848502 8.84445651,27.9988497 C13.3288952,28.0105159 17.1155571,24.5059932 17.6666675,19.8325185 L22.3355504,19.8325185 C22.9355497,24.9353089 27.3622107,28.5588267 32.2222045,27.9288525 C36.6733099,27.3502096 40.0110834,23.374373 39.9999723,18.6658998 L39.9999723,5.83309368 Z" id="Fill-1-button-read_sample" fill="#FFFFFF" mask="url(#mask-4-button-read_sample)"></path>
								</g>
							</g>
						</g>
					</g>
				</svg>
			</button>
			<?php 
				echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );  
			?>
			<!-- <button type="button" class="button--favorite-idle">
				<svg id="wishlist-icon" width="115px" height="115px" viewBox="0 0 115 115">
					<title>button-favorite-idle</title>
					<defs>
						<circle id="path-1-button-favorite-idle" cx="34.5" cy="34.5" r="34.5"></circle>
						<filter x="-52.2%" y="-52.2%" width="204.3%" height="204.3%" filterUnits="objectBoundingBox" id="filter-2-button-favorite-idle">
							<feOffset dx="0" dy="0" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
							<feGaussianBlur stdDeviation="12" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
							<feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
						</filter>
						<polygon id="path-3-button-favorite-idle" points="0 0.36575 23.0000134 0.36575 23.0000134 32.6333333 0 32.6333333"></polygon>
					</defs>
					<g id="Symbols-button-favorite-idle" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<g id="button-favorite-idle" transform="translate(-19.000000, -19.000000)">
							<g id="button-favorite-idle-1" transform="translate(42.000000, 42.000000)">
								<g id="bg">
									<use fill="black" fill-opacity="1" filter="url(#filter-2-button-favorite-idle)" xlink:href="#path-1-button-favorite-idle"></use>
									<use fill="#B1B1B1" fill-rule="evenodd" xlink:href="#path-1-button-favorite-idle"></use>
								</g>
								<g id="icon-bookmark" transform="translate(21.000000, 19.000000)">
									<g id="Group-3-button-favorite-idle">
										<mask id="mask-4-button-favorite-idle" fill="white">
											<use xlink:href="#path-3-button-favorite-idle"></use>
										</mask>
										<g id="Clip-2-button-favorite-idle"></g>
										<path d="M4.3125,3.29908333 L18.6875,3.29908333 C19.4813594,3.29908333 20.125,2.64238333 20.125,1.83241667 C20.125,1.02245 19.4813594,0.36575 18.6875,0.36575 L4.3125,0.36575 C1.93092188,0.36575 -8.47032947e-21,2.33585 -8.47032947e-21,4.76575 L-8.47032947e-21,30.1134167 C-0.004671875,31.5005167 1.0939375,32.62875 2.45345313,32.6335167 C3.10859375,32.6357167 3.7375,32.37135 4.20109375,31.8990833 L11.5,24.44475 L18.7989063,31.8990833 C19.7609531,32.8791833 21.3195625,32.8780833 22.2801719,31.89615 C22.7430469,31.4235167 23.0021563,30.7814833 23,30.1134167 L23,17.96575 C23,17.1557833 22.3563594,16.4990833 21.5625,16.4990833 C20.7686406,16.4990833 20.125,17.1557833 20.125,17.96575 L20.125,29.0973833 L12.5170313,21.33175 C11.9560469,20.75865 11.04575,20.7579167 10.4840469,21.33065 C10.4836875,21.3310167 10.4833281,21.3313833 10.4829688,21.33175 L2.875,29.0973833 L2.875,4.76575 C2.875,3.95578333 3.51864063,3.29908333 4.3125,3.29908333" id="Fill-1-button-favorite-idle" fill="#FFFFFF" mask="url(#mask-4-button-favorite-idle)"></path>
									</g>
									<path d="M32.5,5.6 L28,5.6 L28,1.4 C28,0.62685 27.328375,0 26.5,0 C25.671625,0 25,0.62685 25,1.4 L25,5.6 L20.5,5.6 C19.671625,5.6 19,6.22685 19,7 C19,7.77315 19.671625,8.4 20.5,8.4 L25,8.4 L25,12.6 C25,13.37315 25.671625,14 26.5,14 C27.328375,14 28,13.37315 28,12.6 L28,8.4 L32.5,8.4 C33.328375,8.4 34,7.77315 34,7 C34,6.22685 33.328375,5.6 32.5,5.6" id="Fill-4-button-favorite-idle" fill="#FFFFFF"></path>
								</g>
							</g>
						</g>
					</g>
				</svg>
			</button> -->
		</div>
	<?php endif; ?>

	<?php do_action( 'woocommerce_after_variations_form' ); ?>
</form>

<?php
do_action( 'woocommerce_after_add_to_cart_form' );
