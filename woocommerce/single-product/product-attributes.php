<?php
/**
 * Product attributes
 *
 * Used by list_attributes() in the products class.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-attributes.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

	$book_pages_total = get_field('book_pages_total'); //Text
	$book_first_edition = get_field('book_first_edition'); //Date Picker
	$dateformatstring = "F Y";
	$book_first_edition = strtotime(get_field('book_first_edition'));
	$book_publication = get_field('book_publication'); //Text
	$book_editors = get_field('book_editors'); //Text
	$book_cover_design = get_field('book_cover_design'); //Text
	$book_epimetro = get_field('book_epimetro'); //Text
	$book_intro = get_field('book_intro'); //Text

	$array_rule_sets = get_post_meta( get_the_ID(), '_pricing_rules', true );
	// $discount = '';
	if ($array_rule_sets) {
		foreach( $array_rule_sets as $pricing_rule_sets ) {

			foreach ( $pricing_rule_sets['rules'] as $key => $value ) {

				switch ( $pricing_rule_sets['rules'][$key]['type'] ) {

					case 'percentage_discount':

					$woosymbol = '%';

					break;

					case 'price_discount':

					$woosymbol = get_woocommerce_currency_symbol();

					break;

				}

				$discountHtml = $pricing_rule_sets['rules'][$key]['amount'] . "" . $woosymbol;
				$discount = $pricing_rule_sets['rules'][$key]['amount'];
				$price = $product->get_price();
				$discountAmmount = ($pricing_rule_sets['rules'][$key]['amount']/100)*$price;
				$finalPriceAfterDiscount = $price - $discountAmmount;
			}
		}
	}
?>

<table class="shop_attributes">
	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>
		<tr>
			<th><?php esc_html_e( 'ISBN', 'woocommerce' ); ?></th>
			<td class="product_isbn"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></td>
		</tr>
	<?php endif; ?>

	<?php if ( $book_pages_total ) : ?>
		<tr>
			<th><?php _e( 'Σελίδες', 'woocommerce' ) ?></th>
			<td class="product_pages"><?php echo esc_html( $book_pages_total ); ?></td>
		</tr>
	<?php endif; ?>

	<?php if ( $display_dimensions && $product->has_dimensions() ) : ?>
		<tr>
			<th><?php _e( 'Σχήμα', 'woocommerce' ) ?></th>
			<td class="product_dimensions"><?php echo esc_html( wc_format_dimensions( $product->get_dimensions( false ) ) ); ?></td>
		</tr>
	<?php endif; ?>

	<?php if ( $display_dimensions && $product->has_weight() ) : ?>
		<tr>
			<th><?php _e( 'Βάρος', 'woocommerce' ) ?></th>
			<td class="product_weight"><?php echo esc_html( wc_format_weight( $product->get_weight() ) ); ?></td>
		</tr>
	<?php endif; ?>

	<?php if ( $product->get_price_html() ) : ?>
		<tr>
			<th><?php _e( 'Τιμή', 'woocommerce' ) ?></th>
			<td class="product_price">
				<?php echo $product->get_price_html(); ?>
			</td>
		</tr>
	<?php endif; ?>

	<?php if ( $book_first_edition ) : ?>
		<tr>
			<th><?php _e( 'Κυκλοφορία', 'woocommerce' ) ?></th>
			<td class="product_first_edition"><?php echo date_i18n($dateformatstring, $book_first_edition); ?></td>	
		</tr>
	<?php endif; ?>

	<?php if ( $book_publication ) : ?>
		<tr>
			<th><?php _e( 'Έκδοση', 'woocommerce' ) ?></th>
			<td class="product_pulbication"><?php echo esc_html( $book_publication ); ?></td>
		</tr>
	<?php endif; ?>

	<?php if ( $book_editors ) : ?>
		<tr>
			<th><?php _e( 'Επιμέλεια', 'woocommerce' ) ?></th>
			<td class="product_editors"><?php echo esc_html( $book_editors ); ?></td>
		</tr>
	<?php endif; ?>

	<?php if ( $book_cover_design ) : ?>
		<tr>
			<th><?php _e( 'Σχεδιασμός εξωφύλλου', 'woocommerce' ) ?></th>
			<td class="product_cover_design"><?php echo esc_html( $book_cover_design ); ?></td>
		</tr>
	<?php endif; ?>

	<?php if ( $book_epimetro ) : ?>
		<tr>
			<th><?php _e( 'Επίμετρο', 'woocommerce' ) ?></th>
			<td class="product_epimetro"><?php echo esc_html( $book_epimetro ); ?></td>
		</tr>
	<?php endif; ?>

	<?php if ( $book_intro ) : ?>
		<tr>
			<th><?php _e( 'Εισαγωγή', 'woocommerce' ) ?></th>
			<td class="product_intro"><?php echo esc_html( $book_intro ); ?></td>
		</tr>
	<?php endif; ?>

	<?php foreach ( $attributes as $attribute ) : ?>
		<tr>
			<th><?php echo wc_attribute_label( $attribute->get_name() ); ?></th>
			<td><?php
				$values = array();

				if ( $attribute->is_taxonomy() ) {
					$attribute_taxonomy = $attribute->get_taxonomy_object();
					$attribute_values = wc_get_product_terms( $product->get_id(), $attribute->get_name(), array( 'fields' => 'all' ) );

					foreach ( $attribute_values as $attribute_value ) {
						$value_name = esc_html( $attribute_value->name );

						if ( $attribute_taxonomy->attribute_public ) {
							$values[] = '<a href="' . esc_url( get_term_link( $attribute_value->term_id, $attribute->get_name() ) ) . '" rel="tag">' . $value_name . '</a>';
						} else {
							$values[] = $value_name;
						}
					}
				} else {
					$values = $attribute->get_options();

					foreach ( $values as &$value ) {
						$value = make_clickable( esc_html( $value ) );
					}
				}

				echo apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );
			?></td>
		</tr>
	<?php endforeach; ?>
</table>
