<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}

if ( has_post_thumbnail() ) {
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'full');
	$image_s = wp_get_attachment_image_src( get_post_thumbnail_id( $post ), 'thumbnail');
	$image_m = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'medium');
	$image_ml = wp_get_attachment_image_src( get_post_thumbnail_id( $post ), 'medium_large');
	$image_l = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'large');
	$thumb_id = get_post_thumbnail_id();
}

$link_color = get_field('book_custom_color');
?>

<div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>
	<?php 
		if (false) :
	?>
	<!-- <style type="text/css">
		h2.book__author a,
		.tags-awards .book__posted_in a,
		.arrow-link.colored {
			color: !important;
		}
	</style> -->
	<?php endif; ?>
	<div class="product-top">
		<div class="product-top__inner col-constrained">
			<?php
				/**
				 * Hook: woocommerce_before_single_product_summary.
				 *
				 * @hooked woocommerce_show_product_sale_flash - 10
				 * @hooked woocommerce_show_product_images - 20
				 */
				do_action( 'woocommerce_before_single_product_summary' );
			?>

			<section class="summary entry-summary">
				
				<?php 
					/**
					 * 
					 */
					$book_translators = get_field('book_translators');
					$book_author = get_field('book_author');
				?>

				<?php
					the_title( '<h1 class="product_title entry-title">', '</h1>' );
				?>

				<?php
					if ($book_author) ?>
				<?php 
					$book_author_total = count($book_author);
					if ($book_author_total === 1) : ?>
				<?php foreach( $book_author as $post) :
					setup_postdata($post);
					$author_first_name = get_field('author_first_name');
					$author_last_name = get_field('author_last_name');
					$author_first_name_genitive = get_field('author_first_name_genitive');
					$author_last_name_genitive = get_field('author_last_name_genitive');
					$author_gender = get_field('author_gender');
					
					if ($author_first_name_genitive) {
						$author_first_name_display = $author_first_name_genitive;
					} else {
						$author_first_name_display = $author_first_name;
					}

					if ($author_last_name_genitive) {
						$author_last_name_display = $author_last_name_genitive;
					} else {
						$author_last_name_display = $author_last_name;
					}
					
					if ($author_gender === 'Male') {
						$gender_pronoun = 'του';
					} else {
						$gender_pronoun = 'της';
					}
				?>
				<h2 class="book__author"><span class="gender-pronoun"><?php echo $gender_pronoun; ?></span> <a href="<?php the_permalink(); ?>" rel="bookmark" <?php echo ($link_color)? "style=color:".$link_color."!important":"" ?>><?php echo $author_first_name_display . ' ' . $author_last_name_display; ?></a></h2>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
				<?php else : ?>
				<h2 class="book__author"><span class="gender-pronoun">των </span>
				<?php foreach( $book_author as $post) :
						setup_postdata($post);
						$author_first_name = get_field('author_first_name');
						$author_last_name = get_field('author_last_name');
						$author_first_name_genitive = get_field('author_first_name_genitive');
						$author_last_name_genitive = get_field('author_last_name_genitive');
						$author_gender = get_field('author_gender');
						
						if ($author_first_name_genitive) {
							$author_first_name_display = $author_first_name_genitive;
						} else {
							$author_first_name_display = $author_first_name;
						}

						if ($author_last_name_genitive) {
							$author_last_name_display = $author_last_name_genitive;
						} else {
							$author_last_name_display = $author_last_name;
						}
				?>
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo $author_first_name_display . ' ' . $author_last_name_display; ?></a><span class="comma">,</span>
				<?php endforeach; ?>
				</h2>
				<?php endif; 
					wp_reset_postdata(); 
				?>
				<?php
					if ($book_translators) echo '<h3 class="book__translators">Μετάφραση: <strong>' . $book_translators . '</strong></h3>';
				?>
				<?php 
				global $product;
				do_action( 'woocommerce_product_meta_start' ); ?>
					<?php echo wc_get_product_category_list( $product->get_id(), '<span class="comma">,</span> ', '<span class="book__posted_in">' . _n( '', '', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>
				<?php do_action( 'woocommerce_product_meta_end' ); ?>

				<?php the_content(); ?>

				<?php
					/**
					 * Hook: woocommerce_single_product_summary.
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 * @hooked WC_Structured_Data::generate_product_data() - 60
					 */
					do_action( 'woocommerce_single_product_summary' );
				?>
			</section>
		</div>
		<?php
			/**
			 * Hook: woocommerce_after_single_product_summary.
			 *
			 * @hooked woocommerce_output_product_data_tabs - 10
			 * @hooked woocommerce_upsell_display - 15
			 * @hooked woocommerce_output_related_products - 20
			 */
			do_action( 'woocommerce_after_single_product_summary' );
		?>
	</div>

	<?php
	/**
	 * Advanced custom fields
	 * Book Awards
	 * 
	 */
	?>
	<hr class="product-top__divider">
	<section class="col-constrained tags-awards">
		<article class="left flex">
			<svg class="icon icon-tags">
				<use xlink:href="/wp-content/themes/antipodes/public/svg/symbols.svg#icon-tags" />
			</svg>
			<div class="row">
			<?php 
				global $product;
				do_action( 'woocommerce_product_meta_start' ); ?>
					<?php echo wc_get_product_category_list( $product->get_id(), ' ', '<span class="book__posted_in">' . _n( '', '', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>
				<?php do_action( 'woocommerce_product_meta_end' ); ?>
			</div>
		</article>
		<?php
		/**
		 * Advanced custom fields
		 * Book Authors
		 * 
		 */
		$book_awards = get_field('book_awards');
		if ($book_awards) : ?>
		<article class="right flex">
			<svg class="icon icon-awards">
				<use xlink:href="/wp-content/themes/antipodes/public/svg/symbols.svg#icon-awards" />
			</svg>
			<div class="row">
			<?php // loop through the rows of data
			while ( have_rows('book_awards') ) : the_row();
				// display a sub field value
				$award_text = get_sub_field('text');
				$award_year = get_sub_field('date'); 
			?>
				<p class="award"><?php echo $award_text; ?> - <?php echo $award_year; ?></p>
			<?php endwhile; ?>
			</div>
		</article>
		<?php endif; ?>
	</section> 
	 
	<?php
		/**
		 * Advanced custom fields
		 * Book Authors
		 * 
		 */
		if ($book_author) : ?>
	<section class="book-authors">
		<?php 
			$book_author_total = count($book_author);
			if ($book_author_total === 1) : ?>
		<?php 
			foreach( $book_author as $post) : 
				setup_postdata($post);
				$author_first_name = get_field('author_first_name');
				$author_last_name = get_field('author_last_name');
				$author_gender = get_field('author_gender');
				$author_cv = get_field('author_cv');

				if ($author_gender === 'Male') {
					$gender_pronoun = 'ο';
				} else {
					$gender_pronoun = 'η';
				}

				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full');
				$image_s = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'thumbnail');
				$image_m = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium');
				$image_ml = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium_large');
				$image_l = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large');
				$thumb_id = get_post_thumbnail_id();
		?>
			<h2 class="lined-heading"><span class="line"></span><span class="text"><span class="uppercase"><?php echo $gender_pronoun; ?></span> συγγραφέας</span></h2>
			<article class="col-centered">
				<figure class="thumbnail post-thumbnail">
					<a href="<?php the_permalink()?>" title="<?php echo the_title(); ?>">
					<?php if ( has_post_thumbnail() ) : ?>
						<img
							data-mobile-width="<?php echo $image_m[1]; ?>"
							data-mobile-height="<?php echo $image_m[2]; ?>"
							data-srcset="<?php echo $image_m[0]; ?> 330w,
										<?php echo $image_s[0]; ?> 200w"
							data-sizes="(max-width: 320px) 280px"
							data-ie="<?php echo $image_m[0]; ?>"
							src="<?php echo $image_m[0]; ?>"
							class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
							alt="<?php the_title(); ?>">
					<?php else : ?>
						<img
							src="https://antipodes.cg-dev.eu/wp-content/uploads/author-placeholder.png"
							class="attachment-post-thumbnail size-post-thumbnail wp-post-image lazyload"
							alt="<?php the_title(); ?>">
					<?php endif; ?>
					</a>
				</figure>
				<div>
					<?php echo $author_cv; ?>
					<a href="<?php the_permalink(); ?>" rel="bookmark" class="arrow-link colored" <?php echo ($link_color)? "style=color:".$link_color."!important":"" ?>>
						Επισκεφθείτε τη σελίδα του συγγραφέα 
						<svg class="icon icon-arrow-right-small-black">
							<use xlink:href="/wp-content/themes/antipodes/public/svg/symbols.svg#icon-arrow-right-small-black" />
						</svg>
					</a>
				</div>
			</article>
		<?php endforeach; ?>
		<?php wp_reset_postdata(); ?>
		<?php else : ?>
			<h2 class="lined-heading"><span class="line"></span><span class="text">Οι συγγραφείς</span></h2>
			<?php 
				foreach( $book_author as $post) : 
					setup_postdata($post);
					$author_first_name = get_field('author_first_name');
					$author_last_name = get_field('author_last_name');
					$author_gender = get_field('author_gender');
					$author_cv = get_field('author_cv');

					if ($author_gender === 'Male') {
						$gender_pronoun = 'ο';
					} else {
						$gender_pronoun = 'η';
					}

					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full');
					$image_s = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'thumbnail');
					$image_m = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium');
					$image_ml = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium_large');
					$image_l = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large');
					$thumb_id = get_post_thumbnail_id();
			?>
			<article class="col-centered">
				<figure class="thumbnail post-thumbnail">
					<a href="<?php the_permalink()?>" title="<?php echo the_title(); ?>">
					<?php if ( has_post_thumbnail() ) : ?>
						<img
							data-mobile-width="<?php echo $image_m[1]; ?>"
							data-mobile-height="<?php echo $image_m[2]; ?>"
							data-srcset="<?php echo $image_m[0]; ?> 330w,
										<?php echo $image_s[0]; ?> 200w"
							data-sizes="(max-width: 320px) 280px"
							data-ie="<?php echo $image_m[0]; ?>"
							src="<?php echo $image_m[0]; ?>"
							class="attachment-post-thumbnail size-post-thumbnail wp-post-image lazyload"
							alt="<?php the_title(); ?>">
					<?php else : ?>
						<img
							src="https://antipodes.cg-dev.eu/wp-content/uploads/author-placeholder.png"
							class="attachment-post-thumbnail size-post-thumbnail wp-post-image lazyload"
							alt="<?php the_title(); ?>">
					<?php endif; ?>
					</a>
				</figure>
				<div>
					<?php echo $author_cv; ?>
					<a href="<?php the_permalink(); ?>" rel="bookmark" class="arrow-link colored" <?php echo ($link_color)? "style=color:".$link_color."!important":"" ?>>
						Επισκεφθείτε τη σελίδα του συγγραφέα 
						<svg class="icon icon-arrow-right-small-black">
							<use xlink:href="/wp-content/themes/antipodes/public/svg/symbols.svg#icon-arrow-right-small-black" />
						</svg>
					</a>
				</div>
			</article>
			<?php endforeach; ?>
	
	<?php endif; wp_reset_postdata(); ?>
	</section>
	<?php endif; ?>

	<?php
		/**
		 * Advanced custom fields
		 * Book Reviews
		 * 
		 */
		if( have_rows('book_reviews') ): ?>
		<section class="book-reviews">
			<h2 class="lined-heading"><span class="line"></span><span class="text"><?php _e( 'Κριτικές', 'woocommerce' ) ?></span></h2>
			<div class="main-carousel">
			<?php // loop through the rows of data
			while ( have_rows('book_reviews') ) : the_row();
				// display a sub field value
				$review_text = get_sub_field('text');
				$review_author = get_sub_field('author'); 
				$review_source = get_sub_field('source');
			?>
			<div class="row carousel-cell">
				<blockquote>
						<?php echo $review_text; ?>
				</blockquote>
				<p class="book-review__source"><?php echo $review_author; ?>,  <em><?php echo $review_source; ?></em></p>
			</div>
			<?php endwhile; ?>
			</div>
		</section>  
		<?php else :
		// no rows found
		endif;
	?>

	<?php $book_related_books = get_field('book_related_books'); ?>
	<?php 
		/**
		 * Advanced custom fields
		 * Related books
		 * 
		 */
	if( $book_related_books ): ?>
	<?php 
		$book_related_books_total = count($book_related_books);
		if ($book_related_books_total === 1) : ?>
		<?php else : ?>
		<?php 
			$featured_book = $book_related_books[0];
		?>
		<?php endif; ?>
		<section class="related-books">
			<h2 class="lined-heading"><span class="line"></span><span class="text"><?php echo 'Σχετικά βιβλία'; ?></span></h2>
			<div class="related-books__container">
				 <?php
					$post = $featured_book;
					setup_postdata( $post );
					$book_author = get_field('book_author');
					$book_author_IDs = array();
					$link_color = get_field('book_custom_color');
				
					foreach( $book_author as $author ) {
						$book_author_IDs[] = $author->ID;
					}
				?>
				<article  class="related-books__featured">
					<h2 class="book__title"><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h2>
					<?php 
					$book_author_IDs_total = count($book_author_IDs);
					if ($book_author_IDs_total === 1) : ?>
					<?php foreach( $book_author_IDs as $book_author_ID ) :
						$author_first_name = get_field('author_first_name', $book_author_ID);
						$author_last_name = get_field('author_last_name', $book_author_ID);
						$author_first_name_genitive = get_field('author_first_name_genitive', $book_author_ID);
						$author_last_name_genitive = get_field('author_last_name_genitive', $book_author_ID);
						$author_gender = get_field('author_gender', $book_author_ID);
						$author_slug = get_post_field( 'post_name', $book_author_ID );
						
						
						if ($author_first_name_genitive) {
							$author_first_name_display = $author_first_name_genitive;
						} else {
							$author_first_name_display = $author_first_name;
						}

						if ($author_last_name_genitive) {
							$author_last_name_display = $author_last_name_genitive;
						} else {
							$author_last_name_display = $author_last_name;
						}
						
						if ($author_gender === 'Male') {
							$gender_pronoun = 'του';
						} else {
							$gender_pronoun = 'της';
						}
					?>
					<h3 class="book__author"><span class="gender-pronoun"><?php echo $gender_pronoun; ?></span> <a href="<?php echo site_url() . '/authors' . '/' .$author_slug; ?>" rel="bookmark" class="colored" style="color:<?php echo $link_color;?>;"><?php echo $author_first_name_display . ' ' . $author_last_name_display; ?></a></h3>
					<?php endforeach; ?>
					<?php else : ?>
					<h3 class="book__author"><span class="gender-pronoun">των </span>
					<?php foreach( $book_author_IDs as $book_author_ID ) :
						$author_first_name = get_field('author_first_name', $book_author_ID);
						$author_last_name = get_field('author_last_name', $book_author_ID);
						$author_first_name_genitive = get_field('author_first_name_genitive', $book_author_ID);
						$author_last_name_genitive = get_field('author_last_name_genitive', $book_author_ID);
						$author_gender = get_field('author_gender', $book_author_ID);
						$author_slug = get_post_field( 'post_name', $book_author_ID );
						
						if ($author_first_name_genitive) {
							$author_first_name_display = $author_first_name_genitive;
						} else {
							$author_first_name_display = $author_first_name;
						}

						if ($author_last_name_genitive) {
							$author_last_name_display = $author_last_name_genitive;
						} else {
							$author_last_name_display = $author_last_name;
						}
					?>
					<a href="<?php echo site_url() . '/authors' . '/' .$author_slug; ?>" rel="bookmark" class="colored"><?php echo $author_first_name_display . ' ' . $author_last_name_display; ?></a><span class="comma">,</span>
					<?php endforeach; ?>
					</h3>
					<?php endif; ?>
					<div class="book__excerpt">
						<div class="book__excerpt__text"><?php the_excerpt(); ?></div>
						<a href="<?php the_permalink(); ?>" rel="bookmark" class="arrow-link" >
							Επισκεφθείτε τη σελίδα του βιβλίου
							<svg class="icon icon-arrow-right-small-white">
								<use xlink:href="/wp-content/themes/antipodes/public/svg/symbols.svg#icon-arrow-right-small-white" />
							</svg>
						</a>
					</div>
				</article>

				<div class="related-books__spines">
					<?php
						foreach( $book_related_books as $post ): 
						setup_postdata($post);
						$book_spine_image = get_field('book_spine_image_vertical');  
							if ( has_post_thumbnail() ) {
								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'full');
								$image_s = wp_get_attachment_image_src( get_post_thumbnail_id( $post ), 'thumbnail');
								$image_m = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'medium');
								$image_ml = wp_get_attachment_image_src( get_post_thumbnail_id( $post ), 'medium_large');
								$image_l = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'large');
								$thumb_id = get_post_thumbnail_id();
							}
						$book_related_author = get_field('book_author');
						$book_author_IDs = array();
						$link_color = get_field('book_custom_color');
						foreach( $book_related_author as $author ) {
							$book_author_IDs[] = $author->ID;
						}
					?>
					<article  class="related-book__trigger">
						<?php if($book_spine_image) : ?>
							<a class="related-book__trigger__btn" href="<?php the_permalink()?>"><img class="book_spine_image" src="<?php echo $book_spine_image["url"]; ?>" alt="<?php the_title(); ?>"></a>
						<?php else : ?>
							<a class="related-book__trigger__btn" href="<?php the_permalink()?>"><img class="book_spine_image" src="https://antipodes.cg-dev.eu/wp-content/uploads/spine-placeholder-vertical-e1557913569973.jpg" alt="<?php the_title(); ?>"></a>
						<?php endif; ?>	
						<div  class="related-book__details">
							<h2 class="book__title"><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h2>
							<?php 
								$book_author_IDs_total = count($book_author_IDs);
								if ($book_author_IDs_total === 1) : ?>
							<?php foreach( $book_author_IDs as $book_author_ID ) :
								$author_first_name = get_field('author_first_name', $book_author_ID);
								$author_last_name = get_field('author_last_name', $book_author_ID);
								$author_first_name_genitive = get_field('author_first_name_genitive', $book_author_ID);
								$author_last_name_genitive = get_field('author_last_name_genitive', $book_author_ID);
								$author_gender = get_field('author_gender', $book_author_ID);
								$author_slug = get_post_field( 'post_name', $book_author_ID );
								
								if ($author_first_name_genitive) {
									$author_first_name_display = $author_first_name_genitive;
								} else {
									$author_first_name_display = $author_first_name;
								}

								if ($author_last_name_genitive) {
									$author_last_name_display = $author_last_name_genitive;
								} else {
									$author_last_name_display = $author_last_name;
								}
								
								if ($author_gender === 'Male') {
									$gender_pronoun = 'του';
								} else {
									$gender_pronoun = 'της';
								}
							?>
							<h3 class="book__author"><span class="gender-pronoun"><?php echo $gender_pronoun; ?></span> <a href="<?php echo site_url() . '/authors' . '/' .$author_slug; ?>" rel="bookmark" style="color:<?php echo $link_color;?>;"><?php echo $author_first_name_display . ' ' . $author_last_name_display; ?></a></h3>
							<?php endforeach; ?>
							<?php else : ?>
							<h3 class="book__author"><span class="gender-pronoun">των </span>
							<?php foreach( $book_author_IDs as $book_author_ID ) :
								$author_first_name = get_field('author_first_name', $book_author_ID);
								$author_last_name = get_field('author_last_name', $book_author_ID);
								$author_first_name_genitive = get_field('author_first_name_genitive', $book_author_ID);
								$author_last_name_genitive = get_field('author_last_name_genitive', $book_author_ID);
								$author_gender = get_field('author_gender', $book_author_ID);
								$author_slug = get_post_field( 'post_name', $book_author_ID );
								
								if ($author_first_name_genitive) {
									$author_first_name_display = $author_first_name_genitive;
								} else {
									$author_first_name_display = $author_first_name;
								}

								if ($author_last_name_genitive) {
									$author_last_name_display = $author_last_name_genitive;
								} else {
									$author_last_name_display = $author_last_name;
								}
							?>
							<a href="<?php echo site_url() . '/authors' . '/' .$author_slug; ?>" rel="bookmark"><?php echo $author_first_name_display . ' ' . $author_last_name_display; ?></a><span class="comma">,</span>
							<?php endforeach; ?>
							</h3>
							<?php endif; ?>
							<div class="book__excerpt">
								<?php the_excerpt(); ?>
								<a href="<?php the_permalink(); ?>" rel="bookmark" class="arrow-link" >
									Επισκεφθείτε τη σελίδα του βιβλίου
									<svg class="icon icon-arrow-right-small-white">
										<use xlink:href="/wp-content/themes/antipodes/public/svg/symbols.svg#icon-arrow-right-small-white" />
									</svg>
								</a>
							</div>
						</div>
					</article>
					<?php endforeach; ?>
					<?php wp_reset_postdata(); ?>
				</div>	
			</div>
		</section>
	<?php endif; ?>

	<?php $book_related_posts = get_field('book_related_posts'); ?>
	<?php 
		/**
		 * Advanced custom fields
		 * Author Related Events
		 * 
		 */
	if( $book_related_posts ): ?>
		<section class="related-posts">
			<h2 class="lined-heading"><span class="line"></span><span class="text"><?php echo 'Σχετικές εκδηλώσεις'; ?></span></h2>
			<div class="horizontal-grid">
			<?php
				foreach( $book_related_posts as $post ): ?>
				<?php setup_postdata($post); 
					//event fields
					
					if ( has_post_thumbnail() ) {
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'full');
						$image_s = wp_get_attachment_image_src( get_post_thumbnail_id( $post ), 'thumbnail');
						$image_m = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'medium');
						$image_ml = wp_get_attachment_image_src( get_post_thumbnail_id( $post ), 'medium_large');
						$image_l = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'large');
						$news_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'news_thumbnail_m');
					}
				?>
					<article class="vertical-grid">
						<figure class="thumbnail post-thumbnail">
							<a href="<?php the_permalink()?>" title="<?php echo the_title(); ?>">
								<img
									src="<?php echo $news_thumbnail[0]; ?>"
									class="attachment-post-thumbnail size-post-thumbnail wp-post-image lazyload"
									alt="<?php the_title(); ?>">
							</a>
						</figure>
						
						<div class="post-details">
							<h2 class="post-title"><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h2>
							<div class="post-excerpt"><?php the_excerpt(); ?></div>
							<a href="<?php the_permalink(); ?>" rel="bookmark" class="post-link arrow-link colored" <?php echo ($link_color)? "style=color:".$link_color."!important":"" ?>>
								Δείτε περισσότερα 
								<svg class="icon icon-arrow-right-small-black">
									<use xlink:href="/wp-content/themes/antipodes/public/svg/symbols.svg#icon-arrow-right-small-black" />
								</svg>
							</a>
						</div>		
					</article>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
			</div>
		</section>
	<?php endif; ?>

	<?php $book_excerpts = get_field('book_excerpts'); ?>
	<?php 
		/**
		 * Advanced custom fields
		 * Author Related Events
		 * 
		 */
	if( $book_excerpts ): ?>
		<section class="book-excerpts" data-status="hidden">
			<div class="book-excerpts__inner">
				<?php echo $book_excerpts; ?>
			</div>
		</section>
	<?php endif; ?>
        
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
<script src="<?php echo esc_url( site_url( '/' ) . 'wp-content/themes/antipodes/src/js/flickity.pkgd.min.js' ); ?>"></script>
<script src="<?php echo esc_url( site_url( '/' ) . 'wp-content/themes/antipodes/public/js/flickity.bundle.js' ); ?>"></script>