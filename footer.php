<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */
			
?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="col-full">

			<?php
			/**
			 * Functions hooked in to storefront_footer action
			 *
			 * @hooked storefront_footer_widgets - 10
			 * @hooked storefront_credit         - 20
			 */
			do_action( 'storefront_footer' );
			?>
			<div class="footer-data">
				<aside class="info">
				<?php 
					$post_id = 244;
					$queried_post = get_post($post_id);
					$content = $queried_post->post_content;
					$content = apply_filters('the_content', $content);
					$content = str_replace(']]>', ']]&gt;', $content);
					echo $content;

					wp_reset_postdata();
				?>
				</aside>
				<?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
				<nav class="social-media-links">
					<ul class="unstyled">
						<li><a href="https://www.facebook.com/ekd.antipodes/" target="_blank">facebook</a></li>
						<li><a href="https://www.instagram.com/antipodes_books/?igshid=zcs8zk0t4w6a" target="_blank">instagram</a></li>
						<li><a href="/newsletter">newsletter</a></li>
					</ul>
					<ul class="unstyled bank-icons">
						<li><img src="/wp-content/themes/antipodes/public/svg/Visa.jpg" alt="Pay with Visa"></li>
						<li><img src="/wp-content/themes/antipodes/public/svg/Maestro.jpg" alt="Pay with Maestro"></li>
						<li><img src="/wp-content/themes/antipodes/public/svg/Mastercard.jpg" alt="Pay with Mastercard"></li>
						<li class="vbv"><a href="https://paycenter.piraeusbank.gr/redirection/Content/HTML/3DSecure_el.html" target="_blank"><img src="/wp-content/themes/antipodes/public/svg/vbv.jpg" alt="VBV security"></a></li>
						<li><a href="https://paycenter.piraeusbank.gr/redirection/Content/HTML/3DSecure_el.html" target="_blank"><img src="/wp-content/themes/antipodes/public/svg/sc_156x83.gif" alt="3d security"></a></li>
					</ul>
					<a class="credits" href="http://thinking.gr" target="_blank">designed by thinking</a>
				</nav>
			</div>
		</div><!-- .col-full -->
		<div class="visually-hidden" aria-hidden="true">
			<a class="remove remove_from_cart_button load-cart-icon">call close button</a>
			<span class="load-search-form-close-icon"></span>
			<!-- <em>call italic font</em>
			<i>call italic font</i> -->
		</div>
	</footer><!-- #colophon -->

	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
