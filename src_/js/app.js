"use strict";
import { LayoutSwitch, AccordionMenu } from './book_archive'
import CallPost from './ajax_test'
import { RelatedBooks, BookData } from './book_page'

const WebApp = {
    init() {
        console.log('starting app')   
        LayoutSwitch.init()
        AccordionMenu.init()
        RelatedBooks.init()
        BookData.init()
        const productList = document.querySelector('.products-list')
        if (productList) {
            CallPost.sortMenu(productList)
        }
        
        window.addEventListener('load', (e) => {
            console.log('load event')
            const loader = document.querySelector('.loader')
               
            //loader.classList.remove('visible')
        })
    } 
}

WebApp.init()
