import { hasClass } from './helpers'

const BookData = {
    settings() {
        this.bookDataWrapper = document.querySelector('.wc-tabs-wrapper') 
        if (this.bookDataWrapper) {
            this.bookDataTrigger = this.bookDataWrapper.querySelector('#tab-title-additional_information')
        }
    },
    init() {
        this.settings()
        if (this.bookDataWrapper) {
            this.addEventListeners()
        }
    },
    addEventListeners() {
        this.bookDataTrigger.addEventListener('click', (e) => {
            const data = document.querySelector('#tab-additional_information')
            console.log(data)
            data.classList.toggle('hidden') 
        }) 
    }
}

const RelatedBooks = {
    settings() {
        this.relatedBooksContainer = document.querySelector('.related-books')
        if (this.relatedBooksContainer) {
            this.relatedBooksFeatured = this.relatedBooksContainer.querySelector('.related-books__featured').innerHTML
            this.relatedBookTriggers = this.relatedBooksContainer.querySelectorAll('.related-book__trigger')
        }
    },
    init() {
        this.settings()
        if (this.relatedBooksContainer) {
            this.addEventListeners()
        }
    },
    addEventListeners() {
        const featuredBook = this.relatedBooksContainer.querySelector('.related-books__featured')
       
        Array.from(this.relatedBookTriggers).forEach(trigger => {
            const bookDetails = trigger.querySelector('.related-book__details')
            const button = trigger.querySelector('.related-book__trigger__btn')
            button.addEventListener('click', (e) => {
                e.preventDefault()
                e.target.classList.add('selected')
                featuredBook.innerHTML = bookDetails.innerHTML
                this.closeButton = featuredBook.querySelector('.related-books__featured .close-btn')
                if (this.closeButton) {
                    this.closeButton.addEventListener('click', (e) => {
                        featuredBook.innerHTML = this.relatedBooksFeatured
                        Array.from(this.relatedBookTriggers).forEach(el => {
                            const link = el.querySelector('.related-book__trigger__btn')
                            link.classList.remove('selected')
                        })
                    })
                }
                Array.from(this.relatedBookTriggers).filter(el => {
                    const link = el.querySelector('.related-book__trigger__btn')
                    if (link !== e.target) {
                        link.classList.remove('selected')
                    }
                })
            })
       })
    }
}

export { RelatedBooks, BookData }