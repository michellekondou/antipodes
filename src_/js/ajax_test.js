"use strict"

import { hasClass } from './helpers'
import { LayoutSwitch, AccordionMenu } from './book_archive'

const CallPost = {
    init(url, layout) {
        console.log('callPost init', url)  
        this.makeRequestProductList(url, layout)
        this.appPushState(url)
    },
    makeRequestSingleProduct(url) {
        fetch(url)
            .then((response) => {
                // When the page is loaded convert it to text
                return response.text()
            })
            .then((data) => {
                console.log('loading page')
                // Initialize the DOM parser
                const parser = new DOMParser()
                // Parse the text
                const doc = parser.parseFromString(data, "text/html")
                // Parse the document title
                //const docTitle = doc.title
                // Parse ...
                const productContent = doc.querySelector('.product.type-product')
                const page = document.querySelector('.ajax-container')
                if (productContent) {
                    const docBody = productContent.innerHTML
                    if (page) {
                        page.innerHTML = docBody
                    }
                } 
            })
            .catch(function (err) {
                console.log('Failed to fetch page: ', err);
            })
    },
    makeRequestProductList(url, layout) {
        fetch(url)
            .then((response) => {
                // When the page is loaded convert it to text
                return response.text()
            })
            .then((data) => {
                console.log('loading page')
                // Initialize the DOM parser
                const parser = new DOMParser()
                // Parse the text
                const doc = parser.parseFromString(data, "text/html")
                // Parse the document title
                //const docTitle = doc.title
                // Parse ...
                const productList = doc.querySelector('.products-list')
                const sortMenu = doc.querySelector('.sorting-menu')
                const productListReplace = document.querySelector('.products-list')
                const sortMenuReplace = document.querySelector('.sorting-menu')
                const loader = document.querySelector('.loader')
                // const productContent = doc.querySelector('.product.type-product')
                // const page = document.querySelector('.ajax-container')
                if (productList) {
                    const docBody = productList.innerHTML
                    if (productListReplace) {
                        productListReplace.innerHTML = docBody
                        productListReplace.querySelector('.products').setAttribute('data-layout', layout)
                        productListReplace.classList.remove('visibility-hidden')
                        sortMenuReplace.innerHTML = sortMenu.innerHTML
                        setTimeout(() => {
                            loader.classList.remove('visible')
                        }, 500)

                        this.pagination(productListReplace)
                        this.sorting(productListReplace)
                        this.sortMenu(productListReplace)
                        AccordionMenu.init()

                    }
                }

                // Νεότερο > Παλαιότερο /vivlia/?orderby=date&paged=1
                // Παλαιότερο > Νεότερο /vivlia/?orderby=oldest_to_recent&paged=1
                // Άνα συγγραφέα /vivlia/?orderby=author&paged=1

            })
            .catch(function (err) {
                console.log('Failed to fetch page: ', err);
            })
    },
    appPushState(url) {
        const pageInfo = {
            title: null,
            url: location.href
        }
        history.pushState(pageInfo, null, url)
    },
    pagination(element) {
        const paginationContainer = element.querySelectorAll('.page-numbers')
        Array.from(paginationContainer).forEach(el => {
            const productsList = el.closest('.products-list').querySelector('.products')
            if (el) {
                const paginationItems = el.querySelectorAll('li')
                Array.from(paginationItems).forEach(item => {
                    const link = item.querySelector('a')
                    if (link) {
                        link.addEventListener('click', loadPage.bind(this), true)

                        function loadPage(e) {
                            e.preventDefault()
                            this.makeRequestProductList(e.target.getAttribute('href'), productsList.getAttribute('data-layout'))
                            this.appPushState(e.target.getAttribute('href'))
                        }
                    }

                })
            }
        })

        const top = document.querySelector('#page')
        top.scrollIntoView()
        
    },
    sorting(element) {
        const sortOptionsContainer = element.querySelector('.orderby')
        const productsList = element.closest('.products-list').querySelector('.products')

        sortOptionsContainer.addEventListener('change', (e) => {
            e.preventDefault()
            this.makeRequestProductList(window.location.origin + window.location.pathname + '?orderby=' + e.target.value, productsList.getAttribute('data-layout'))
            this.appPushState(window.location.origin + window.location.pathname + '?orderby=' + e.target.value)
        }, false)

        if (sortOptionsContainer) {
            const sortOptions = sortOptionsContainer.querySelectorAll('option')
            Array.from(sortOptions).forEach(el => {
                if (el) {
                    el.addEventListener('click', (e) => {
                    }, true)
                    //loadPage.bind(this)
                    function loadPage(e) {
                        //e.preventDefault()
                        this.makeRequestProductList(el.getAttribute('value'))
                        this.appPushState(el.getAttribute('value'))
                    }
                }

            })
        } 
    },
    sortMenu(element) {
        const sortOptionsContainer = document.querySelector('.sorting-options')
        const productsList = element.querySelector('.products')
        const sortItems = sortOptionsContainer.querySelectorAll('li a')
        Array.from(sortItems).forEach(item => {
            item.classList.remove('selected')
            item.addEventListener('click', (e) => {
                e.preventDefault()
                e.target.classList.add('selected')
                this.makeRequestProductList(e.target.getAttribute('href'), productsList.getAttribute('data-layout'))
                this.appPushState(e.target.getAttribute('href'))
            }, false)
        })

        if (window.location.search === '?orderby=date' || window.location.search === '') {
            sortOptionsContainer.querySelector('.newest-to-oldest').classList.add('selected')
        } else if (window.location.search === '?orderby=oldest_to_recent') {
            sortOptionsContainer.querySelector('.oldest-to-newest').classList.add('selected')
        } else if (window.location.search === '?orderby=author') {
            sortOptionsContainer.querySelector('.by-author').classList.add('selected')
        }
    }
}

export default CallPost