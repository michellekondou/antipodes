"use strict";
//set webpack public_path for wordpress
__webpack_public_path__ = process.env.ASSET_PATH;

import { detect } from 'detect-browser'
const browser = detect()
import 'core-js/modules/es.array.iterator' //to make dynamic imports work in IE 11

import IOlazy from './iolazy' 
import { hasClass } from './helpers'  
const getHomePosts = () => import(/* webpackChunkName: 'homePosts' */'./homePage')
const getMobileMenu = () => import(/* webpackChunkName: 'mobileMenu' */'./navigation')
const getBookArchive = () => import(/* webpackChunkName: 'bookArchive' */'./book_archive')
const getBookPage = () => import(/* webpackChunkName: 'bookPage' */'./book_page')
const getTippy = () => import(/* webpackChunkName: 'tippy' */'tippy.js')  
const getCallPost = () => import(/* webpackChunkName: 'callPost' */'./ajax_test')
const getAnagnoseis = () => import(/* webpackChunkName: 'anagnoseis' */'./anagnoseis')  
const getSearchForm = () => import(/* webpackChunkName: 'searchForm' */'./search_form') 

const WebApp = {
    init() { 
        
         if (browser.name === 'ie') {
            import(/* webpackChunkName: 'whatwg-fetch-polyfill' */ 'whatwg-fetch')
            import(/* webpackChunkName: 'element-remove-polyfill' */ 'element-remove-polyfill')
            import(/* webpackChunkName: 'picturefill' */ 'picturefill')
            //import(/* webpackChunkName: 'events-polyfill' */ 'events-polyfill')
        } else if (browser.name === 'safari') {
            import(/* webpackChunkName: 'intersection-observer' */ 'intersection-observer')
        }

        if ('IntersectionObserver' in window) {
            console.log('IntersectionObserver in window')
        } else {
            console.log('IntersectionObserver not in window')
            import(/* webpackChunkName: 'intersection-observer' */ 'intersection-observer')
        }
        
        window.addEventListener('load', (e) => {

            let contactPosition = ''
            const searchTrigger = document.querySelector('.search-trigger')
            if (window.innerWidth > 1279) {
                contactPosition = document.querySelector('.menu-item-39')
                searchTrigger.style.left = contactPosition.getBoundingClientRect().right + 12 + 'px'
            } else {
                contactPosition = document.querySelector('.mobile-menu-trigger')
                searchTrigger.style.left = contactPosition.getBoundingClientRect().right - 102 + 'px'
            }
            
            searchTrigger.style.display = 'none'
            searchTrigger.style.top = contactPosition.getBoundingClientRect().top + contactPosition.getBoundingClientRect().height / 2 - 12 + 'px'
            searchTrigger.style.display = 'flex'

            new IOlazy({
                image: '.lazyload',
                threshold: 0.06
            })

            if (window.innerWidth < 1024) {
                getMobileMenu().then(method => {
                    method.MobileMenu.init()
                })
            } else {
                window.addEventListener('mousemove', initMenu)

                function initMenu() {
                    getMobileMenu().then(method => {
                        method.MobileMenu.init()
                    })
                    window.removeEventListener('mousemove', initMenu)
                }
            }
            
            if (hasClass(document.body, 'home')) {
                getHomePosts().then(method => {
                    method.HomePosts.init()
                })
            } else {
                getSearchForm().then(method => {
                    method.default.init()
                })
            }

             if (hasClass(document.body, 'single-anagnoseis')) {
                getAnagnoseis().then(method => {
                    method.Anagnoseis.init()
                })
            }

            if (hasClass(document.body, 'post-type-archive-product') || hasClass(document.body, 'tax-product_cat')) {
                getBookArchive().then(method => {
                    method.LayoutSwitch.init()
                    method.AccordionMenu.init()
                })

                const productList = document.querySelector('.products-list')
                if (productList) {
                   getCallPost().then(method => {
                       method.default.sortMenu(productList)
                   }) 
                }
            } 

            if (hasClass(document.body, 'single-product')) {
                getBookPage().then(method => {
                    method.RelatedBooks.init()
                    method.BookData.init()
                    method.WishListButton.init()
                    method.BookExcerpt.init()
                    method.MiniCartExtras.init()
                    method.AddToCartBtn.init()
                })
            }

            if (hasClass(document.body, 'single-authors')) {
                getBookPage().then(method => {
                    method.RelatedBooks.init()
                })
            }
        })

        if (hasClass(document.body, 'single-product')) {
            let documentTop = document.body.getBoundingClientRect().top
            if (documentTop < 0) {
                import(/* webpackChunkName: 'tippy' */'tippy.js') 
            } else {
                window.addEventListener('scroll', (e) => {
                    import(/* webpackChunkName: 'tippy' */'tippy.js')
                })
            }

            const tooltip = document.querySelectorAll('.tooltip')
            if (tooltip) {
                Array.from(tooltip).forEach(item => {
                    item.addEventListener('mouseover', (e) => {
                        getTippy().then((tippyModule) => {
                            tippyModule.default('.tooltip', {
                                animation: 'fade',
                                arrow: 'true'
                            })
                        })
                    })
                })
            }
        }
        
        window.addEventListener('popstate', popStateWatchForward, true)

        function popStateWatchForward(e) {
            if (e.state !== null) {
                if (typeof e.state.title === 'undefined') {
                    getCallPost().then(method => {
                        method.default.makeRequestPage(window.location.search)
                    })
                } else {
                    getCallPost().then(method => {
                        method.default.makeRequestPage(window.history.state.title)
                    }) 
                } 
                const navLinks = document.querySelectorAll('.menu-item')
                navLinks.forEach(el => {
                    el.classList.remove('current-menu-item')
                })
            } else {
                if (window.location.pathname === '/') {
                    location.reload()
                }
            }
        }

    } 
}

WebApp.init()
