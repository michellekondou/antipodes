var worksCarousel = document.querySelectorAll('.main-carousel')

Array.from(worksCarousel).forEach(el => {
    var flkty = new Flickity(el, {
        // options
        cellAlign: 'left',
        lazyLoad: true,
        imagesLoaded: true,
        pageDots: false,
        prevNextButtons: true,
        fade: true,
        arrowShape: {
            x0: 10,
            x1: 60, y1: 50,
            x2: 70, y2: 40,
            x3: 30
        }
    })

    var galleryCells = el.querySelectorAll('.carousel-cell')

    if (galleryCells.length === 1) {
        el.classList.add('is-hiding-nav-ui');
    }
})

var anagnoseisCarousel = document.querySelectorAll('.anagnoseis__carousel')

Array.from(anagnoseisCarousel).forEach(el => {
    const flkty = new Flickity(el, {
        // options
        cellAlign: 'left',
        pageDots: true,
        prevNextButtons: false,
        lazyLoad: true,
        imagesLoaded: true,
        fade: true
    })

    const galleryCells = el.querySelectorAll('.slide')

    const navDots = el.querySelector('.flickity-page-dots')

    const container = el.parentElement.parentElement.querySelector('.anagnoseis__item--menu-content')

    const anagnoseisDots = container.querySelector('.flickity-page-dots.anagnoseis')
    const contentForReplacement = navDots.innerHTML
    anagnoseisDots.innerHTML = contentForReplacement
    
    var items = Array.prototype.slice.call( anagnoseisDots.children )

    flkty.on( 'select', function() {
        var previousSelectedButton = anagnoseisDots.querySelector('.is-selected')
        var selectedButton = anagnoseisDots.children[ flkty.selectedIndex ]
        previousSelectedButton.classList.remove('is-selected')
        selectedButton.classList.add('is-selected')
    })

    anagnoseisDots.addEventListener( 'click', function( e ) {
        // only link clicks
        if ( !matchesSelector( e.target, 'li' ) ) {
            return;
        }
        e.preventDefault()
        var item = e.target
        var index =  items.indexOf( item )
        flkty.select( index )
    })



    const carouselStatus = document.querySelector('.carousel-status');

    function updateStatus() {
        var slideNumber = flkty.selectedIndex + 1;
        carouselStatus.textContent = 'Απόσπασμα ' + slideNumber + '/' + flkty.slides.length;
    }
    updateStatus();

    flkty.on( 'select', updateStatus );


    flkty.on( 'change', function(){
        console.log('getting next slide')
    });
})