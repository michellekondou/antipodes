import { hasClass } from './helpers'

const Anagnoseis = {
    settings() {
        this.anagnoseisListContainer = document.querySelector('.anagnoseis__list-container')
        this.anagnoseisList = document.querySelector('.anagnoseis__list')
    }, 
    init() {
        console.log('anagnoseis init')
        this.settings()
        if (this.anagnoseisList) {
            this.anagnoseisRow = this.anagnoseisList.querySelectorAll('.anagnoseis__item')
                this.anagnoseisListContainer.classList.remove('visibility-hidden')
                if (this.anagnoseisRow) {
                    Array.from(this.anagnoseisRow).forEach(item => {
                        const flickityViewport = item.querySelector('.flickity-viewport')
                        console.log(flickityViewport)
                        if (flickityViewport) {
                            flickityViewport.style.overflow = 'auto'
                            flickityViewport.setAttribute('data-simplebar', 'init')
                        }

                        //const itemBookCover = item.querySelector('.post-thumbnail__container img')
                        // const itemSpine = item.querySelector('.book-spine')
                        // itemSpine.style.height = itemBookCover.clientHeight + 'px'
                        // itemSpine.src = itemSpine.getAttribute('data-src')  
                        // if (itemSpine) {

                        // }
                        //itemSpine.style.height = itemBookCover.getBoundingClientRect().height + 'px'
                    })
                }

            
            
            
        }
    }
}

export { Anagnoseis }