import { hasClass } from './helpers'
import { TimelineMax } from 'gsap'
import CallPost from './ajax_test'

const MobileMenu = {
    settings() {
        this.mobileMenuTrigger = document.querySelector('.mobile-menu-trigger')
        this.siteHeader = document.querySelector('.site-header')
        this.mobileMenuBackground = document.querySelector('.mobile-menu-background')
        this.mobileMenuContainer = document.querySelector('.storefront-primary-navigation')
        this.mobileMenuInner = this.mobileMenuContainer.querySelector('.primary-navigation')
        
        this.mobileMenuInnerLink = this.mobileMenuInner.querySelectorAll('a')
        this.mobileMenuCart = this.mobileMenuContainer.querySelector('#site-header-cart')
        this.logo = document.querySelector('.logo-antipodes_large')
        this.mainNav = document.querySelector('.storefront-primary-navigation')
        this.siteFooter = document.querySelector('.site-footer')
    },
    init() {
        this.settings()
        this.addEventListeners()
    },
    addEventListeners() {
        var isIE11 = /Trident.*rv[ :]*11\./.test(navigator.userAgent);
        if (this.mobileMenuTrigger) {
            this.mobileMenuTrigger.addEventListener('click', this.click.bind(this))
        }
        
        Array.from(this.mobileMenuInnerLink).forEach(link => {
            if (link.getAttribute('href') !== 'https://antipodes.cg-dev.eu/contact/' && !isIE11) {
                const linkParent = link.parentElement
                linkParent.classList.remove('focus')
                linkParent.classList.remove('current-menu-item')
                linkParent.classList.remove('current_page_item')
                link.removeEventListener('click', this.fetchPageListener.bind(this), true)
                link.addEventListener('click', this.fetchPageListener.bind(this), true)
               
            }
        })
    }, 
    fetchPageListener(e) {
        if (this.fetchPageListener) {
            console.log('/********fetching page********/', e)
            if (e) {
                e.preventDefault()
            }
            
            const loader = document.querySelector('.loading')
            this.timerID = setTimeout(() => {
                loader.setAttribute('aria-hidden', false)
            }, 1000);
            this.isMobileMenu = false
            if (hasClass(document.body, 'home') || window.innerWidth < 1280) {
                this.isMobileMenu = true
            }            
            const targetParent = e.target.parentElement
            targetParent.classList.add('focus')
            targetParent.classList.add('current-menu-item')
            targetParent.classList.add('current_page_item')

            console.log('e.target', e.target, e.target.getAttribute('href').split('/'))  
            CallPost.makeRequestPage(e.target, this.isMobileMenu, this.timerID)
            CallPost.appPushState(e.target, `${e.target}`)


            Array.from(this.mobileMenuInnerLink).filter(link => {
                const linkParent = link.parentElement
                if (linkParent !== targetParent) {
                        linkParent.classList.remove('focus')
                        linkParent.classList.remove('current-menu-item')
                        linkParent.classList.remove('current_page_item')
                    }
            })
            
        } else {
            return
        }
    },
    click() {
        this.siteHeader.classList.toggle("menu-mobile-opened")
        this.opened ? this.closeMenu() : this.openMenu()
    },
    openMenu() {
        this.timeline && this.timeline.kill()

        this.opened = true

        this.timeline = new TimelineMax({
            onStart: () => {
                this.logo.style = 'clip-path: circle(50%)'
                this.logo.classList.add('menu-open')
                this.mainNav.style.display = 'flex'
                document.body.classList.add('menu-open')
            },
            onComplete: () => {
                // this.siteFooter.classList.add('beneath')
                // this.siteFooter.classList.add('is-mutating')
            }
        })

        this.timeline.fromTo(this.mobileMenuBackground, 1, {
            scaleX: 0,
            transformOrigin: "0 50%"
        }, {
            scaleX: 1,
            ease: Quart.easeOut
        }, 0)

        this.timeline
            .fromTo([this.mobileMenuInner, this.mobileMenuCart], 1, {
                x: -500,
                clearProps: "clip"
            }, {
                x: 0,
                ease: Quart.easeOut
            }, "-=1")
            .set('.mobile-menu-trigger span', {
                backgroundColor: 'black'
            }, "-=0.5")

    },
    closeMenu() {
        this.timeline && this.timeline.kill()

        this.opened = false

        this.timeline = new TimelineMax({
            onStart: () => {
                this.logo.classList.remove('menu-open')
                document.body.classList.remove('menu-open')
                // this.siteFooter.classList.remove('beneath')
                // this.siteFooter.classList.remove('visible')
            },
            onComplete: () => {
                this.mainNav.style.display = ''
                document.body.classList.add('menu-close-complete')
            }
        })
       
        
        this.timeline.fromTo(this.mobileMenuBackground, 1, {
            scaleX: 1,
            transformOrigin: "100% 50%"
        }, {
            scaleX: 0,
            ease: Quart.easeOut
        }, 0)

        this.timeline
            .fromTo([this.mobileMenuInner, this.mobileMenuCart], 1, {
                x: 0,
            }, {
                x: 200,
                ease: Quart.easeOut
            }, 0)
            .set([this.mobileMenuInner, this.mobileMenuCart], {
                clip: "rect(0px, 1016px, 798px, 1016px)",
            }, "-=0.8")
            .set(this.logo, {
                clearProps: "clip"
            }, "-=0.9")
            .set('.mobile-menu-trigger span', {
                clearProps: "background-color"
            }, "-=0.5")
    }
}

export { MobileMenu }


// var u = function (t) {
//     function e(t) {
//         !function (t, e) {
//             if (!(t instanceof e))
//                 throw new TypeError("Cannot call a class as a function")
//         }(this, e);
//         var n = l(this, (e.__proto__ || Object.getPrototypeOf(e)).call(this, t));
//         return n.resize = n.resize.bind(n),
//             n.click = n.click.bind(n),
//             n.link = n.container,
//             n.header = document.querySelector("#header"),
//             n.background = document.querySelector(".mobile-menu-background"),
//             n.menuMobileContainer = document.querySelector("#mobile-menu-container"),
//             n.menuMobileContainerInner = n.menuMobileContainer.querySelector(".mobile-menu-container-inner"),
//             n.menuMobile = n.menuMobileContainer.querySelector("#mobile-menu"),
//             n.menuMobileFooter = n.menuMobileContainer.querySelector(".mobile-menu-footer"),
//             n.opened = !1,
//             n.scrollbar = o.default.init(n.menuMobileContainer, {
//                 damping: .1,
//                 thumbMinSize: 20,
//                 renderByPixels: !0,
//                 alwaysShowTracks: !1,
//                 continuousScrolling: !0
//             }),
//             n.resize(),
//             l(n, n)
//     }
//     return function (t, e) {
//         if ("function" != typeof e && null !== e)
//             throw new TypeError("Super expression must either be null or a function, not " + typeof e);
//         t.prototype = Object.create(e && e.prototype, {
//             constructor: {
//                 value: t,
//                 enumerable: !1,
//                 writable: !0,
//                 configurable: !0
//             }
//         }),
//             e && (Object.setPrototypeOf ? Object.setPrototypeOf(t, e) : t.__proto__ = e)
//     }(e, r.default),
//         i(e, [{
//             key: "start",
//             value: function () {
//                 this.link.addEventListener("click", this.click),
//                     window.addEventListener("resize", this.resize)
//             }
//         }, {
//             key: "stop",
//             value: function () {
//                 this.link.removeEventListener("click", this.click),
//                     window.removeEventListener("resize", this.resize)
//             }
//         }, {
//             key: "resize",
//             value: function () {
//                 TweenMax.set(this.menuMobile, {
//                     minHeight: window.innerHeight - this.menuMobileFooter.offsetHeight
//                 }),
//                     this.scrollbar.update()
//             }
//         }, {
//             key: "click",
//             value: function () {
//                 (arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
//                     preventDefault: function () { }
//                 }).preventDefault(),
//                     this.moving || (this.opened ? this.close() : this.open())
//             }
//         }, {
//             key: "open",
//             value: function () {
//                 var t = this;
//                 this.moving = !0,
//                     this.opened = !0,
//                     this.timeline && this.timeline.kill(),
//                     this.timeline = new TimelineMax({
//                         onComplete: function () {
//                             return t.moving = !1
//                         }
//                     }),
//                     this.timeline.addCallback(function () {
//                         t.header.classList.toggle("menu-mobile-opened", !0),
//                             t.scrollbar.scrollTo(0, 0, 0),
//                             t.resize()
//                     }),
//                     this.timeline.set(this.menuMobileContainerInner, {
//                         display: "flex"
//                     }),
//                     this.timeline.fromTo(this.background, 1, {
//                         scaleX: 0,
//                         transformOrigin: "0 50%"
//                     }, {
//                             scaleX: 1,
//                             ease: Quart.easeOut
//                         }, 0),
//                     this.timeline.fromTo(this.menuMobileContainer, 1, {
//                         clip: "rect(0px, 0px, " + this.menuMobileContainer.offsetHeight + "px, 0px)"
//                     }, {
//                             clip: "rect(0px, " + this.menuMobileContainer.offsetWidth + "px, " + this.menuMobileContainer.offsetHeight + "px, 0px)",
//                             clearProps: "clip",
//                             ease: Quart.easeOut
//                         }, 0),
//                     this.timeline.fromTo(this.menuMobileContainerInner, 1, {
//                         x: -200
//                     }, {
//                             x: 0,
//                             ease: Quart.easeOut
//                         }, 0);
//                 var e = document.querySelector("article[data-router-view]")
//                     , n = [].concat(a(e.querySelectorAll(".page, .menu-page-navigation"))).concat([].concat(a(document.querySelectorAll("#footer"))));
//                 return this.timeline.fromTo(n, 1, {
//                     x: 0
//                 }, {
//                         x: 200,
//                         clearProps: "x",
//                         ease: Quart.easeOut
//                     }, 0),
//                     this.timeline
//             }
//         }, {
//             key: "close",
//             value: function () {
//                 var t = this
//                     , e = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0];
//                 if (this.moving = !0,
//                     this.opened = !1,
//                     this.timeline && this.timeline.kill(),
//                     this.timeline = new TimelineMax({
//                         onComplete: function () {
//                             return t.moving = !1
//                         }
//                     }),
//                     this.timeline.addCallback(function () {
//                         t.header.classList.toggle("menu-mobile-opened", !1)
//                     }),
//                     this.timeline.fromTo(this.background, 1, {
//                         scaleX: 1,
//                         transformOrigin: "100% 50%"
//                     }, {
//                             scaleX: 0,
//                             ease: Quart.easeOut
//                         }, 0),
//                     this.timeline.fromTo(this.menuMobileContainer, 1, {
//                         clip: "rect(0px, " + this.menuMobileContainer.offsetWidth + "px, " + this.menuMobileContainer.offsetHeight + "px, 0px)"
//                     }, {
//                             clip: "rect(0px, " + this.menuMobileContainer.offsetWidth + "px, " + this.menuMobileContainer.offsetHeight + "px, " + this.menuMobileContainer.offsetWidth + "px)",
//                             ease: Quart.easeOut
//                         }, 0),
//                     this.timeline.fromTo(this.menuMobileContainerInner, 1, {
//                         x: 0
//                     }, {
//                             x: 200,
//                             ease: Quart.easeOut
//                         }, 0),
//                     e) {
//                     var n = document.querySelector("article[data-router-view]")
//                         , i = [].concat(a(n.querySelectorAll(".page, .menu-page-navigation"))).concat([].concat(a(document.querySelectorAll("#footer"))));
//                     this.timeline.fromTo(i, 1, {
//                         x: -200
//                     }, {
//                             x: 0,
//                             clearProps: "x",
//                             ease: Quart.easeOut
//                         }, 0)
//                 }
//                 return this.timeline.addCallback(function () {
//                     t.scrollbar.scrollTo(0, 0, 0)
//                 }),
//                     this.timeline.set(this.menuMobileContainerInner, {
//                         display: "none"
//                     }),
//                     this.timeline
//             }
//         }], [{
//             key: "createElements",
//             value: function (t) {
//                 var n = [];
//                 return [].concat(a(t.querySelectorAll(".mobile-menu-trigger"))).forEach(function (t) {
//                     t.element = new e(t),
//                         n.push(t.element)
//                 }),
//                     n
//             }
//         }]),
//         e
// }();
// e.default = u
// }