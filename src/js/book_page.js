import { hasClass } from './helpers'
const getTippy = () => import(/* webpackChunkName: 'tippy' */'tippy.js')  

const BookData = {
    settings() {
        this.bookDataWrapper = document.querySelector('.wc-tabs-wrapper') 
        if (this.bookDataWrapper) {
            this.bookDataTrigger = this.bookDataWrapper.querySelector('#tab-title-additional_information')
            this.bookDataArrow = this.bookDataWrapper.querySelector('.icon-arrow-right-white')
        }
    },
    init() {
        this.settings()
        if (this.bookDataWrapper) {
            this.addEventListeners()
        }
    },
    addEventListeners() {
        this.bookDataTrigger.addEventListener('click', (e) => {
            const data = document.querySelector('#tab-additional_information')
            data.classList.toggle('hidden') 
            this.bookDataArrow.classList.toggle('rotated')
        }) 
    }
}

const WishListButton = {
    settings() {
        this.wishListLink = document.querySelector('.yith-wcwl-add-button.show .add_to_wishlist')
        this.wishListIcon = document.getElementById('wishlist-icon')
        this.wishListBrowseLink = document.querySelector('.yith-wcwl-wishlistexistsbrowse.show a, .yith-wcwl-wishlistaddedbrowse.show a')
        this.wishListAddedLink = document.querySelector('.yith-wcwl-wishlistaddedbrowse a')
        this.wishListExistsLink = document.querySelector('.yith-wcwl-wishlistexistsbrowse a')
    },
    init() {
        this.settings()
        if (this.wishListLink) {
            this.wishListLink.innerHTML = ''
            this.wishListLink.parentElement.style.display = 'flex'
            this.wishListLink.classList.add('tooltip')
            this.wishListLink.setAttribute('data-tippy-content', 'Προσθήκη στα αγαπημένα')

            if (this.wishListLink) {
                this.wishListLink.addEventListener('mouseover', (e) => {
                    getTippy().then((tippyModule) => {
                        tippyModule.default('.tooltip', {
                            animation: 'fade',
                            arrow: 'true'
                        })
                    })
                })
            }
        }
        
        if (this.wishListBrowseLink) {
            this.wishListBrowseLink.innerHTML = ''
        }

        if (this.wishListAddedLink) {
            this.wishListAddedLink.classList.add('tooltip')
            this.wishListAddedLink.setAttribute('data-tippy-content', 'Έχει προστεθεί στα αγαπημένα. Δείτε τα αγαπημένα')
        }

        if (this.wishListExistsLink) {
            this.wishListExistsLink.classList.add('tooltip')
            this.wishListExistsLink.setAttribute('data-tippy-content', 'Δείτε τα αγαπημένα')
        }
    }
}

const RelatedBooks = {
    settings() {
        this.relatedBooksContainer = document.querySelector('.related-books')
        if (this.relatedBooksContainer) {
            this.relatedBooksFeatured = this.relatedBooksContainer.querySelector('.related-books__featured').innerHTML
            this.relatedBookTriggers = this.relatedBooksContainer.querySelectorAll('.related-book__trigger')
        }
    },
    init() {
        this.settings()
        if (this.relatedBooksContainer) {
            this.addEventListeners()
        }
    },
    addEventListeners() {
        const featuredBook = this.relatedBooksContainer.querySelector('.related-books__featured')
       
        Array.from(this.relatedBookTriggers).forEach(trigger => {
            const bookDetails = trigger.querySelector('.related-book__details')
            const button = trigger.querySelector('.related-book__trigger__btn')
            button.addEventListener('click', (e) => {
                e.preventDefault()
                e.target.classList.add('selected')
                featuredBook.innerHTML = bookDetails.innerHTML
                this.closeButton = featuredBook.querySelector('.related-books__featured .close-btn')
                if (this.closeButton) {
                    this.closeButton.addEventListener('click', (e) => {
                        featuredBook.innerHTML = this.relatedBooksFeatured
                        Array.from(this.relatedBookTriggers).forEach(el => {
                            const link = el.querySelector('.related-book__trigger__btn')
                            link.classList.remove('selected')
                        })
                    })
                }
                Array.from(this.relatedBookTriggers).filter(el => {
                    const link = el.querySelector('.related-book__trigger__btn')
                    if (link !== e.target) {
                        link.classList.remove('selected')
                    }
                })
            })
       })
    }
}

const BookExcerpt = {
    settings() {
        this.bookExcerptContainer = document.querySelector('.book-excerpts')
        
        if (this.bookExcerptContainer) {
            this.bookExcerptContainerInner = this.bookExcerptContainer.querySelector('.book-excerpts__inner')
            this.bookExcerptTrigger = document.querySelector('.button--read-sample')
        }
    },
    init() {
        this.settings()
        if (this.bookExcerptContainer) {
            this.closeExcerpt()
            if (this.bookExcerptTrigger) {
                this.bookExcerptTrigger.classList.add('visible')
                this.addEventListeners()
            }
        }
    },
    addEventListeners() {
        this.bookExcerptTrigger.addEventListener('click', (e) => {
            this.bookExcerptContainer.setAttribute('data-status', 'visible')
            document.body.classList.add('overlay-open')
        })

        this.closeExcerptButton.addEventListener('click', (e) => {
            this.bookExcerptContainer.setAttribute('data-status', 'hidden')
            document.body.classList.remove('overlay-open')
        })
    },
    closeExcerpt() {
        this.closeExcerptButton = document.createElement('button')
        if (this.closeExcerptButton) {
            this.closeExcerptButton.classList.add('close-excerpt')
            this.bookExcerptContainerInner.appendChild(this.closeExcerptButton) 
        }
    }
}

const MiniCartExtras = {
    init() {
        document.body.addEventListener('click', (e) => {
            if (hasClass(e.target, 'remove_from_cart_button')) {
                const wcMessage = document.querySelector('.woocommerce-message')
                if (wcMessage) {
                    wcMessage.remove()
                }
            }
        })
    }
}

const AddToCartBtn = {
    init() {
        const btn = document.querySelector('.single_add_to_cart_button')
        const price = btn.querySelector('.product-price')
        const buyText = btn.querySelector('#svg-buy-text')

        if (btn) {
            const mouseOverBtn = (e) => {
                price.style.display = 'none'
                buyText.style.display = 'block'
            }

            const mouseOutBtn = (e) => {
                price.style.display = 'block'
                buyText.style.display = 'none'
            }

            btn.addEventListener('mouseenter', mouseOverBtn)
            btn.addEventListener('mouseleave', mouseOutBtn)
        }
    }
}

export { RelatedBooks, BookData, WishListButton, BookExcerpt, MiniCartExtras, AddToCartBtn }