const HomePosts = {
    settings() {
        this.homePage = document.querySelector('.home .site')
        this.homeLoader = document.querySelector('.loader--home')
        this.homeOverlay = document.querySelector('.overlay--home')

        if ("ontransitionend" in window) {
            console.log('browsser supports transitionend')
        }
    }, 
    init() {
        this.settings()
        if (this.homePage) {
            jQuery(document).ready(function () {
                jQuery('.home-content').pagepiling({
                    direction: 'horizontal',
                    navigation: {
                        'bulletsColor': '#fff',
                        'position': 'bottom',
                    },
                });
            });
            this.homePage.classList.remove('screen-reader-text')
            this.homeOverlay.classList.add('hidden')
            
            this.homeOverlay.addEventListener('transitionend', () => {
                setTimeout(() => {
                    this.homeOverlay.classList.add('behind')
                }, 250);
                    
            });
            this.homeLoader.classList.remove('visible')
            this.slider()
        }
        //boxLoaderTl.fromTo(boxLoaderRect, 1.5, { drawSVG: "0%", stroke: "#424242" }, { drawSVG: "100%", stroke: "#000000", immediateRender: false, ease: Power1.easeInOut }, "-=0.25")
    },
    slider() {
        this.slidesWrapper = document.querySelector('.home-content')
        //this.slidesWrapper.style.height = this.slidesWrapper.clientHeight + 'px'
        console.log(this.slidesWrapper.clientHeight)
        this.slides = document.querySelectorAll('.home-content .slide')
        // if (this.slides) {
        //     Array.from(this.slides).forEach((slide, i) => {
        //         if (i === 0) {
        //             slide.classList.add('active')
        //         }
        //     })
        // }

        
    }
}

export { HomePosts }