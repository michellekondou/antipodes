import { hasClass } from './helpers'

const SearchForm = {
    settings() {
        this.searchForm = document.querySelectorAll('form.woocommerce-product-search')
    },
    init() {
        const search_form = document.querySelector('.woocommerce-product-search')
        const search_btn = search_form.querySelector('.search-submit')
        const search_input = search_form.querySelector('.search-field')
        const nav_bar = document.getElementById('primary-menu')
        const search_trigger = search_form.querySelector('.search-trigger')
        const search_submit_header = document.querySelector('.woocommerce-product-search .search-submit')
        const search_submit_page = document.querySelector('.page-content .search-submit')

        if (search_submit_page) {
            search_submit_page.value = ''
        }

        search_trigger.addEventListener('click', (e) => {
            if (!hasClass(search_trigger, 'inactive')) {
                search_form.classList.add('active')
                search_input.classList.remove('visually-hidden')
                search_input.classList.add('open')
                search_trigger.classList.add('inactive')
                if (search_input.value !== '') {
                    search_trigger.classList.add('visually-hidden')
                    search_submit_header.classList.remove('visually-hidden')
                }
            } else {
                search_input.focus()
            }
        })

        search_input.addEventListener('transitionend', function activateSubmitBtn(e) {
            if (hasClass(search_input, 'open')) {
                search_input.focus()
            }
        }, false)

        if (search_input.value === '') {
            search_input.addEventListener('input', (e) => {
                search_trigger.classList.add('visually-hidden')
                search_submit_header.classList.remove('visually-hidden')
                search_input.setAttribute('placeholder', '')
            })
        } 

        search_form.addEventListener('submit', (e) => {
            if (search_input.value === '') {
                e.preventDefault();
                search_input.setAttribute('placeholder', 'Παρακαλώ συμπληρώστε το πεδίο!')
                search_input.focus()
            }
        })

        document.addEventListener('click', (e) => {
            if (!e.target.matches('.search-trigger') && !e.target.matches('.search-submit') && !e.target.matches('.search-field')) {
                if (hasClass(search_input, 'open')) {
                    search_input.classList.remove('open')
                    search_input.setAttribute('placeholder', 'Αναζήτηση βιβλίου')
                    search_input.value = ''
                    search_trigger.classList.remove('visually-hidden')
                    search_submit_header.classList.add('visually-hidden')
                    search_trigger.classList.remove('inactive')
                    search_input.addEventListener('transitionend', function activateSubmitBtn(e) {
                        search_form.classList.remove('active')
                        search_input.removeEventListener('transitionend', activateSubmitBtn, false)
                    }, false)
                }
            }
        })
        let scrollPos = 0
        window.addEventListener('scroll', (e) => {
            let vw = (window.innerWidth || document.documentElement.clientWidth)
            let documentTop = document.body.getBoundingClientRect().top;
            let search_trigger_position = search_trigger.getBoundingClientRect().top;
            // detects new state and compares it with the new one

            if (documentTop > scrollPos) {
                if (documentTop >= -search_trigger_position) {
                    search_form.classList.remove('visually-hidden')
                    if (hasClass(search_form, 'active')) {
                        search_input.focus()
                    }
                 }
            } else {
                if (documentTop <= -1) {
                    search_form.classList.add('visually-hidden')
                 }
            }
            // saves the new position for iteration.
            scrollPos = documentTop
        })



        window.addEventListener('resize', (e) => {
            let contactPosition = ''
            const searchTrigger = document.querySelector('.search-trigger')
            if (window.innerWidth > 1279) {
                contactPosition = document.querySelector('.menu-item-39')
                searchTrigger.style.left = contactPosition.getBoundingClientRect().right + 12 + 'px'
            } else {
                contactPosition = document.querySelector('.mobile-menu-trigger')
                searchTrigger.style.left = contactPosition.getBoundingClientRect().right - 102 + 'px'
            }
            searchTrigger.style.display = 'none'
            searchTrigger.style.top = contactPosition.getBoundingClientRect().top + contactPosition.getBoundingClientRect().height / 2 - 12 + 'px'
          
            searchTrigger.style.display = 'flex'
        })

        let contactPosition = ''
        const searchTrigger = document.querySelector('.search-trigger')
        if (window.innerWidth > 1279) {
            contactPosition = document.querySelector('.menu-item-39')
            searchTrigger.style.left = contactPosition.getBoundingClientRect().right + 12 + 'px'
        } else {
            contactPosition = document.querySelector('.mobile-menu-trigger')
            searchTrigger.style.left = contactPosition.getBoundingClientRect().right - 102 + 'px'
        }
        searchTrigger.style.display = 'none'
        searchTrigger.style.top = contactPosition.getBoundingClientRect().top + contactPosition.getBoundingClientRect().height / 2 - 12 + 'px'

        searchTrigger.style.display = 'flex'
    }
}

export default SearchForm