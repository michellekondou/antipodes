import { hasClass } from './helpers'
import CallPost from './ajax_test'
import IOlazy from './iolazy_local'

const LayoutSwitch = {
    init() {
        this.layout = document.querySelector('.post-type-archive-product .products, .tax-product_cat .products')
        if (this.layout) {
            this.list_button = document.querySelector('.layout-switch--list')
            this.thumbs_button = document.querySelector('.layout-switch--thumbs')
            this.spines_button = document.querySelector('.layout-switch--spines')

            const defaultLayout = () => {
                if (hasClass(document.body, 'search-results')) {
                    this.thumbs_button.classList.add('selected')
                    this.spines_button.classList.remove('selected')
                    this.list_button.classList.remove('selected')
                    localStorage.removeItem('layout')
                    localStorage.clear()
                    localStorage.setItem('layout', 'thumbs')
                    this.layout.setAttribute('data-layout', 'thumbs-layout')
                    const layoutParent = this.layout.parentElement
                    layoutParent.classList.remove('visibility-hidden')
                } else {
                    if (window.innerWidth > 1280) {
                        this.switchFunc()
                    } else {
                        this.thumbs_button.classList.add('selected')
                        localStorage.removeItem('layout')
                        localStorage.clear()
                        localStorage.setItem('layout', 'thumbs')
                        this.layout.setAttribute('data-layout', 'thumbs-layout')
                        const layoutParent = this.layout.parentElement
                        layoutParent.classList.remove('visibility-hidden')
                    }
                }
                
            }

            defaultLayout()

            window.addEventListener('resize', this.switchProductListOnResize.bind(this), true) 
        }
    },
    switchProductListOnResize(e) {
        if (window.innerWidth > 1280) {
            if (!localStorage.getItem('layout')) {
                const bookDetailsWrapper = document.querySelectorAll('.book__spine-details')
                Array.from(bookDetailsWrapper).forEach(item => {
                    item.style.width = ''
                })
            } else {
                if (!localStorage.getItem('layout') === 'thumbs') {
                    this.thumbs_button.classList.remove('selected')
                    this.spines_button.classList.add('selected')
                    this.list_button.classList.remove('selected')
                }
            }

            window.removeEventListener('resize', this.switchProductListOnResize.bind(this), true)
        } else {
            if (!localStorage.getItem('layout') || localStorage.getItem('layout') === 'list' ) {
                localStorage.removeItem('layout')
                localStorage.clear()
                localStorage.setItem('layout', 'thumbs')

                this.thumbs_button.classList.add('selected')
                this.spines_button.classList.remove('selected')
                this.list_button.classList.remove('selected')

                const layoutParent = this.layout.parentElement
                if (layoutParent) {
                    layoutParent.classList.remove('visibility-hidden')
                }

                CallPost.init('/vivlia/?orderby=date', 'thumbs-layout', '/vivlia/?orderby=date')
                window.removeEventListener('resize', this.switchProductListOnResize.bind(this), true)
            } else {
                return
            } 
        }
    },
    switchFunc() {
        const loader = document.querySelector('.loader')
        //set local storage
        this.list_button.addEventListener('click', ((e) => {
            e.preventDefault()
            this.thumbs_button.classList.remove('selected')
            this.spines_button.classList.remove('selected')
            //add list class to collection archive container
            e.target.classList.add('selected')
            this.layout.classList.remove('thumbs-layout')
            //remove and clear local storage
            localStorage.removeItem('layout')
            localStorage.clear()
            //set layout=list in local storage
            localStorage.setItem('layout', 'list')

            CallPost.init('/vivlia/?orderby=oldest_to_recent', 'list-layout')
        }))

        this.spines_button.addEventListener('click', ((e) => {
            e.preventDefault()
            //loader.classList.add('visible')
            this.thumbs_button.classList.remove('selected')
            this.list_button.classList.remove('selected')
            e.target.classList.add('selected')
            //remove and clear local storage
            localStorage.removeItem('layout')
            localStorage.clear()
            CallPost.init('/vivlia/?orderby=date', 'spines-layout', '/vivlia/?orderby=date')
        }))

        this.thumbs_button.addEventListener('click', ((e) => {
            e.preventDefault()
            //loader.classList.add('visible')
            this.list_button.classList.remove('selected')
            this.spines_button.classList.remove('selected')
            e.target.classList.add('selected')
            //remove and clear local storage
            localStorage.removeItem('layout')
            localStorage.clear()
            //set layout=list in local storage
            localStorage.setItem('layout', 'thumbs')
            CallPost.init('/vivlia/?orderby=date', 'thumbs-layout', '/vivlia/?orderby=date') 
        }))


        if(localStorage.getItem('layout')) {  
            console.log('local storage set: ',localStorage.getItem('layout')) 
            if (window.location.search === '') {
                console.log('no parameters in url')
                //remove and clear local storage
                localStorage.removeItem('layout')
                localStorage.clear()
                console.log('storage cleared')
                this.layout.parentElement && this.layout.parentElement.classList.remove('visibility-hidden')
                this.spines_button.classList.add('selected')
                this.layout.setAttribute('data-layout','spines-layout') 
            } else {
                if (localStorage.getItem('layout') === 'thumbs') {
                    this.spines_button.classList.remove('selected')
                    this.list_button.classList.remove('selected')
                    this.thumbs_button.classList.add('selected')
                    this.layout.setAttribute('data-layout','thumbs-layout') 
                    this.layout.parentElement && this.layout.parentElement.classList.remove('visibility-hidden')

                } else if (localStorage.getItem('layout') === 'list') {
                    if (window.location.search === '') {
                        console.log('no parameters in url')
                        window.location.search = '?orderby=oldest_to_recent'
                    }
                    this.list_button.classList.add('selected')
                    this.spines_button.classList.remove('selected')
                    this.thumbs_button.classList.remove('selected')
                    this.layout.setAttribute('data-layout','list-layout') 
                    this.layout.parentElement && this.layout.parentElement.classList.remove('visibility-hidden')
                }
            }   
            
        } else {
            console.log('no layout selected')
            this.list_button.classList.remove('selected')
            this.thumbs_button.classList.remove('selected')
            this.spines_button.classList.add('selected')
            
            if (this.layout.parentElement) {
                this.layout.parentElement.classList.remove('visibility-hidden')
            }
            this.layout.setAttribute('data-layout','spines-layout') 
        }
    },
    spinesLayout() {
        const spinesTrigger = document.querySelectorAll('.book__spine-image')
        
        if (spinesTrigger) {
            Array.from(spinesTrigger).forEach(item => {
                const spinesDetails = item.parentElement.querySelector('.book__spine-details')
                spinesDetails.style.width = item.parentElement.clientWidth + 'px'
                item.addEventListener('click', toggleDetails, true)
            })
            
            function toggleDetails(e) {
                e.preventDefault()
                const spinesDetails = e.target.parentElement.querySelector('.book__spine-details')
                spinesDetails.classList.toggle('visually-hidden')
                e.target.toggleAttribute('aria-expanded')
                if (hasClass(spinesDetails, 'visually-hidden')) {
                    e.target.setAttribute('aria-expanded',false)
                } else {
                    e.target.setAttribute('aria-expanded', true)
                }
            }
        }
        
    }
}

const AccordionMenu = {
    settings() {
        this.accordionTriggers = document.querySelectorAll('.book__spine-image')
    },
    init() {
        this.settings()
        this.addEventListeners()
    },
    addEventListeners() {
        Array.from(this.accordionTriggers).forEach(trigger => {
            const spinesDetails = trigger.parentElement.querySelector('.book__spine-details')
            spinesDetails.style.width = spinesDetails.parentElement.clientWidth + 'px'
        
            trigger.addEventListener('click', this.toggleAccordion.bind(this), true)
        })
    },
    toggleAccordion(e) {
        e.preventDefault()
        const trigger = e.target
        const spinesDetails = trigger.parentElement.querySelector('.book__spine-details')
        if (trigger.getAttribute('aria-expanded') === 'false') {
            const imgToLoad = spinesDetails.querySelector('.lazyload')
        
            if (imgToLoad.getAttribute('src') === 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7') {
                imgToLoad.setAttribute('src', imgToLoad.getAttribute('data-src'))
                imgToLoad.addEventListener('load', (e) => {
                    AccordionMenu.expandSection(spinesDetails)
                })
            } else {
                AccordionMenu.expandSection(spinesDetails) 
            }
             
            trigger.setAttribute('aria-expanded', true)
        } else {
            AccordionMenu.collapseSection(spinesDetails)
            trigger.setAttribute('aria-expanded', false)
        }
        //this.showUniqueSubmenu(e.target)
    },
    showUniqueSubmenu(trigger) {
        //show only one submenu at a time
        const hideBookDetails = (item) => {
            if (item.getAttribute('data-collapsed') === 'false') {
                AccordionMenu.collapseSection(item)
                item.setAttribute('aria-hidden', true)
                //item.parentElement.classList.remove('is-expanded')
                // const button = item.parentElement.querySelector('button')
                // if (button && button.hasAttribute('aria-expanded')) {
                //     button.removeAttribute('aria-expanded')
                // }
            }
        }
        Array.from(document.querySelectorAll('.book__spine-image')).filter((item) => {
            if ( item !== trigger ) {
                item.setAttribute('aria-expanded', false)
                const details = item.parentElement.querySelectorAll('.book__spine-details')
                Array.from(details).forEach(el => {
                    if (el.id === item.getAttribute('aria-controls')) {
                        //AccordionMenu.collapseSection(el)
                    }
                })
                // if (menu.getAttribute('id') !== trigger.getAttribute('aria-controls') && menu.getAttribute('id') !== trigger.getAttribute('data-target')) {
                //     hideMenu(menu)
                // }
            }
        })
    },
    collapseSection(element) {
        //get the height of the element's inner content, regardless of its actual size
        const sectionHeight = element.scrollHeight
        // temporarily disable all css transitions
        const elementTransition = element.style.transition
        element.style.transition = ''
        // on the next frame (as soon as the previous style change has taken effect),
        // explicitly set the element's height to its current pixel height, so we 
        // aren't transitioning out of 'auto'
        requestAnimationFrame(() => {
            element.style.height = sectionHeight + 'px'
            element.style.transition = elementTransition
            // on the next frame (as soon as the previous style change has taken effect),
            // have the element transition to height: 0
            requestAnimationFrame(() => {
                element.style.height = 0 + 'px'
            })
        })
        const collapseFunction = (e) => {
            // remove this event listener so it only gets triggered once
            element.classList.remove('transitioning')
            const arrowLink = element.querySelector('.arrow-link')
            arrowLink.classList.remove('visible')
            element.removeEventListener('transitionend', collapseFunction)
        }
        //when the next css transition finishes (which should be the one we just triggered)
        element.addEventListener('transitionend', collapseFunction)
        // mark the section as "currently collapsed"
        element.setAttribute('data-collapsed', 'true')
        element.setAttribute('aria-hidden', true)
        element.classList.remove('open') 
    },
    expandSection(element) {

        const expandWork = () => {
            console.log('run this function')
            let sectionHeight
            // get the height of the element's inner content, regardless of its actual size
            if (element.getAttribute('data-collapsed') !== 'false') {
                if (window.innerWidth < 1280) {
                    //console.log(window.innerWidth)
                    sectionHeight = element.scrollHeight + element.querySelector('.post-thumbnail img').scrollHeight
                } else {
                    sectionHeight = Math.max(element.scrollHeight, element.querySelector('.post-thumbnail img').scrollHeight)
                
                    // have the element transition to the height of its inner content
                    element.style.height = sectionHeight + 25 + 'px' //add 16 to the bottom of the ul
                    element.classList.add('open')
                    element.classList.add('transitioning')
                    

                    const expandFunction = (e) => {
                        const arrowLink = element.querySelector('.arrow-link')
                        arrowLink.classList.add('visible')
                        // remove this event listener so it only gets triggered once
                        element.removeEventListener('transitionend', expandFunction)
                    }
                    //when the next css transition finishes (which should be the one we just triggered)
                    element.addEventListener('transitionend', expandFunction)

                    // mark the section as "currently not collapsed"
                    element.setAttribute('data-collapsed', 'false');
                    element.setAttribute('aria-hidden', false)
                }
            }
            
        }

        expandWork()
        
    }
}

export { LayoutSwitch, AccordionMenu } 


// // //keyboard navigation in collection
// document.addEventListener('keydown', (event) => {
//     const keyName = event.key
//     const keyCode = event.keyCode
//     const nav_prev_link = document.querySelector('.nav-previous a')
//     const nav_next_link = document.querySelector('.nav-next a')

//     if (keyName === 'Control') {
//         // do not alert when only Control key is pressed.
//         return;
//     }

//     let post_navigation_link = false;

//     if (nav_prev_link !== null) {
//         if (keyName === 'ArrowRight' || keyCode === '39') {
//             post_navigation_link = nav_prev_link.getAttribute('href')
//         }
//     } 
//     if (nav_next_link !== null) {
//         if (keyName === 'ArrowLeft' || keyCode === '37') {
//             post_navigation_link = nav_next_link.getAttribute('href')
//         }
//     }

//     if (post_navigation_link) {
//         window.location = post_navigation_link;
//     }

// }, false);


// // //photoswipe
// document.addEventListener("DOMContentLoaded", function () {
//     // var flickity_img = document.querySelectorAll('.carousel-cell');
//     //----Start PhotoSwipe
//     var initPhotoSwipeFromDOM = function (gallerySelector) {

//         var parseThumbnailElements = function (el) {

//             var thumbElements = el.childNodes,
//                 numNodes = thumbElements.length,
//                 items = [],
//                 figureEl,
//                 linkEl,
//                 size,
//                 item;

//             for (var i = 0; i < numNodes; i++) {
//                 figureEl = thumbElements[i]; // <figure> element

//                 // include only element nodes 
//                 if (figureEl.nodeType !== 1) {
//                     continue;
//                 }

//                 linkEl = figureEl.children[0]; // <a> element
//                 console.log(linkEl)
//                 size = linkEl.getAttribute('data-size').split('x');

//                 // create slide object
//                 item = {
//                     src: linkEl.getAttribute('href'),
//                     w: parseInt(size[0], 10),
//                     h: parseInt(size[1], 10)
//                 };



//                 if (figureEl.children.length > 1) {
//                     // <figcaption> content
//                     item.title = figureEl.children[1].innerHTML;
//                 }

//                 if (linkEl.children.length > 0) {
//                     // <img> thumbnail element, retrieving thumbnail url
//                     item.msrc = linkEl.children[0].getAttribute('data-ie');
//                 }

//                 item.el = figureEl; // save link to element for getThumbBoundsFn
//                 items.push(item);
//             }

//             return items;
//         };

//         // find nearest parent element
//         var closest = function closest(el, fn) {
//             return el && (fn(el) ? el : closest(el.parentNode, fn));
//         };

//         // triggers when user clicks on thumbnail
//         var onThumbnailsClick = function (e) {
//             e = e || window.event;
//             e.preventDefault ? e.preventDefault() : e.returnValue = false;

//             var eTarget = e.target || e.srcElement;

//             // find root element of slide
//             var clickedListItem = closest(eTarget, function (el) {
//                 return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
//             });

//             if (!clickedListItem) {
//                 return;
//             }

//             // find index of clicked item by looping through all child nodes
//             // alternatively, you may define index via data- attribute
//             var clickedGallery = clickedListItem.parentNode,
//                 childNodes = clickedListItem.parentNode.childNodes,
//                 numChildNodes = childNodes.length,
//                 nodeIndex = 0,
//                 index;

//             for (var i = 0; i < numChildNodes; i++) {
//                 if (childNodes[i].nodeType !== 1) {
//                     continue;
//                 }

//                 if (childNodes[i] === clickedListItem) {
//                     index = nodeIndex;
//                     break;
//                 }
//                 nodeIndex++;
//             }



//             if (index >= 0) {
//                 // open PhotoSwipe if valid index found
//                 openPhotoSwipe(index, clickedGallery);
//             }
//             return false;
//         };

//         // parse picture index and gallery index from URL (#&pid=1&gid=2)
//         var photoswipeParseHash = function () {
//             var hash = window.location.hash.substring(1),
//                 params = {};

//             if (hash.length < 5) {
//                 return params;
//             }

//             var vars = hash.split('&');
//             for (var i = 0; i < vars.length; i++) {
//                 if (!vars[i]) {
//                     continue;
//                 }
//                 var pair = vars[i].split('=');
//                 if (pair.length < 2) {
//                     continue;
//                 }
//                 params[pair[0]] = pair[1];
//             }

//             if (params.gid) {
//                 params.gid = parseInt(params.gid, 10);
//             }

//             return params;
//         };

//         var openPhotoSwipe = function (index, galleryElement, disableAnimation, fromURL) {
//             var pswpElement = document.querySelectorAll('.pswp')[0],
//                 gallery,
//                 options,
//                 items;

//             items = parseThumbnailElements(galleryElement);

//             // define options (if needed)
//             options = {

//                 // define gallery index (for URL)
//                 galleryUID: galleryElement.getAttribute('data-pswp-uid'),

//                 getThumbBoundsFn: function (index) {
//                     // See Options -> getThumbBoundsFn section of documentation for more info
//                     var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
//                         pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
//                         rect = thumbnail.getBoundingClientRect();

//                     return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
//                 }

//             };

//             // PhotoSwipe opened from URL
//             if (fromURL) {
//                 if (options.galleryPIDs) {
//                     // parse real index when custom PIDs are used 
//                     // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
//                     for (var j = 0; j < items.length; j++) {
//                         if (items[j].pid == index) {
//                             options.index = j;
//                             break;
//                         }
//                     }
//                 } else {
//                     // in URL indexes start from 1
//                     options.index = parseInt(index, 10) - 1;
//                 }
//             } else {
//                 options.index = parseInt(index, 10);
//             }

//             // exit if index not found
//             if (isNaN(options.index)) {
//                 return;
//             }

//             if (disableAnimation) {
//                 options.showAnimationDuration = 0;
//             }

//             // Pass data to PhotoSwipe and initialize it
//             gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
//             gallery.init();
//         };

//         // loop through all gallery elements and bind events
//         var galleryElements = document.querySelectorAll(gallerySelector);

//         for (var i = 0, l = galleryElements.length; i < l; i++) {
//             galleryElements[i].setAttribute('data-pswp-uid', i + 1);
//             galleryElements[i].onclick = onThumbnailsClick;
//         }

//         // Parse URL and open gallery if it contains #&pid=3&gid=1
//         var hashData = photoswipeParseHash();
//         if (hashData.pid && hashData.gid) {
//             openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
//         }
//     };


//     initPhotoSwipeFromDOM('.magnifiable');

// });
// // //set the images in collection to resize to the size of the viewport
// window.addEventListener('load', () => {

//     const imageContainer = document.querySelector('.artwork .post-thumbnail')
//     if (imageContainer !== null) {
//         const imageContainerPortrait = document.querySelector('.artwork.portrait .post-thumbnail')
//         const imageContainerLandscape = document.querySelector('.artwork.landscape .post-thumbnail')
//         const imageContainerTop = imageContainer.getBoundingClientRect().top
//         const viewportHeight = (window.innerHeight || document.documentElement.clientHeight)
//         const imageContainerHeight = (viewportHeight - imageContainerTop - 25)
//         const artworkDetails = document.querySelector('.artwork .detail-container')

//         if (imageContainerPortrait !== null) {
//             imageContainer.style.height = imageContainerHeight + 'px'
//         }

//         if (imageContainerLandscape !== null) {
//             if (parseInt(imageContainerLandscape.style.height) < imageContainerHeight) {
//                 return;
//             } else {
//                 imageContainerLandscape.style.height = imageContainerHeight + 'px'
//             }
//         }
//     }
    
// })







