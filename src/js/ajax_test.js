"use strict"

import { hasClass } from './helpers'
import { LayoutSwitch, AccordionMenu } from './book_archive'
import IOlazy from './iolazy_local'
import { MobileMenu } from './navigation'
import SearchForm from './search_form'
import { fetch } from 'whatwg-fetch'


const CallPost = {
    init(url, layout, state) {
        this.makeRequestProductList(url, layout)
        this.appPushState(url, state)
    },
    makeRequestPage(url, mobileMenu, timer) {
        async function getData() {
            let response = await fetch(url)
            return await response.text()
        }

        getData()
            .then((data) => {
                cb(data)
            })
            .catch(function (err) {
                console.log('Failed to fetch page: ', err);
            })
      
        let cb = (data) => {
            const loader = document.querySelector('.loading')
            clearInterval(timer)
            loader.setAttribute('aria-hidden', true)
            if (hasClass(document.body, 'home') || window.innerWidth < 1280) {
                MobileMenu.closeMenu()
            }

            const isHome = document.body.classList.contains('home')

            this.siteHeader = document.querySelector('.site-header')
            this.siteHeader.classList.remove("menu-mobile-opened")
            // Initialize the DOM parser
            const parser = new DOMParser()
            // // Parse the text
            const doc = parser.parseFromString(data, "text/html")

            // Save doc elements to use
            const docTitle = doc.title
            const docBody = doc.querySelector('body')
            const docBodyClasses = docBody.classList.value || docBody.classList

            // //set the body classes accordingly
            const body = document.querySelector('body')

            body.setAttribute('class', docBodyClasses)
            if (mobileMenu) {
                body.classList.add('menu-transitioning')
            }

            const docHeader = doc.querySelector('.site-header')
            const docHeaderNav = docHeader.querySelector('.storefront-primary-navigation')
            const pageHeader = document.querySelector('.site-header')
            // Replace the main page content
            const docPage = doc.querySelector('.site')

            const page = document.querySelector('.site')

            page.innerHTML = docPage.innerHTML
            const pageContent = page.querySelector('.site-content')
            pageContent.classList.add('fade-in')

            //if we're on books call the following functions
            const productList = page.querySelector('.products-list')
            if (productList) {
                LayoutSwitch.init()
                AccordionMenu.init()
            }
            //hide home slider nav
            if (isHome) {
                const ppNav = document.querySelector('#pp-nav')
                if (ppNav) {
                    ppNav.remove()
                }
                docHeaderNav.classList.add('hidden')
            } else {
                if (productList) {
                    IOlazy.init(productList, '.lazyload', 0, '0px')
                }
            }
            
            
            const observeBody = new MutationObserver((mutations) => {
                mutations.forEach((mutation) => {
                    if (mutation.target.classList && mutation.target.classList.contains('menu-close-complete')) {
                        document.body.classList.remove('menu-close-complete')
                        pageHeader.innerHTML = docHeader.innerHTML
                       
                        MobileMenu.init()

                        body.classList.remove('menu-transitioning')
                        if (hasClass(document.body, 'home') || window.innerWidth < 1281) {
                            this.mobileMenuList = document.querySelector('#menu-main-navigation')
                            this.mobileMenuList.classList.add('nav-menu')
                        }
                    }
                });
            });

            observeBody.observe(document.querySelector('body'), {
                attributes: true
            });

            const observeHeaderNav = new MutationObserver((mutations) => {
                mutations.forEach((mutation) => {
                    const pageHeaderNav = pageHeader.querySelector('.storefront-primary-navigation')
                    if (Array.from(mutation.addedNodes).includes(pageHeaderNav)) {
                        setTimeout(() => {
                            pageHeaderNav.classList.add('visible')
                            pageHeaderNav.classList.remove('hidden')
                            const pageHeaderNavLinks = pageHeaderNav.querySelectorAll('a')
                            Array.from(pageHeaderNavLinks).filter(link => {
                                const linkParent = link.parentElement
                                linkParent.classList.remove('focus')
                                linkParent.classList.remove('current-menu-item')
                                linkParent.classList.remove('current_page_item')
                                if (link.getAttribute('href') === url.getAttribute('href')) {
                                    linkParent.classList.add('focus')
                                    linkParent.classList.add('current-menu-item')
                                    linkParent.classList.add('current_page_item')
                                }
                            })
                            
                            SearchForm.init()
                            if (productList) {
                                IOlazy.init(productList, '.lazyload', 0, '0px')
                            }
                            jQuery(function (n) { if ("undefined" == typeof wc_cart_fragments_params) return !1; var t = !0, o = wc_cart_fragments_params.cart_hash_key; try { t = "sessionStorage" in window && null !== window.sessionStorage, window.sessionStorage.setItem("wc", "test"), window.sessionStorage.removeItem("wc"), window.localStorage.setItem("wc", "test"), window.localStorage.removeItem("wc") } catch (w) { t = !1 } function a() { t && sessionStorage.setItem("wc_cart_created", (new Date).getTime()) } function s(e) { t && (localStorage.setItem(o, e), sessionStorage.setItem(o, e)) } var e = { url: wc_cart_fragments_params.wc_ajax_url.toString().replace("%%endpoint%%", "get_refreshed_fragments"), type: "POST", success: function (e) { e && e.fragments && (n.each(e.fragments, function (e, t) { n(e).replaceWith(t) }), t && (sessionStorage.setItem(wc_cart_fragments_params.fragment_name, JSON.stringify(e.fragments)), s(e.cart_hash), e.cart_hash && a()), n(document.body).trigger("wc_fragments_refreshed")) } }; function r() { n.ajax(e) } if (t) { var i = null; n(document.body).on("wc_fragment_refresh updated_wc_div", function () { r() }), n(document.body).on("added_to_cart removed_from_cart", function (e, t, n) { var r = sessionStorage.getItem(o); null !== r && r !== undefined && "" !== r || a(), sessionStorage.setItem(wc_cart_fragments_params.fragment_name, JSON.stringify(t)), s(n) }), n(document.body).on("wc_fragments_refreshed", function () { clearTimeout(i), i = setTimeout(r, 864e5) }), n(window).on("storage onstorage", function (e) { o === e.originalEvent.key && localStorage.getItem(o) !== sessionStorage.getItem(o) && r() }), n(window).on("pageshow", function (e) { e.originalEvent.persisted && (n(".widget_shopping_cart_content").empty(), n(document.body).trigger("wc_fragment_refresh")) }); try { var c = n.parseJSON(sessionStorage.getItem(wc_cart_fragments_params.fragment_name)), _ = sessionStorage.getItem(o), g = Cookies.get("woocommerce_cart_hash"), m = sessionStorage.getItem("wc_cart_created"); if (null !== _ && _ !== undefined && "" !== _ || (_ = ""), null !== g && g !== undefined && "" !== g || (g = ""), _ && (null === m || m === undefined || "" === m)) throw "No cart_created"; if (m) { var d = 1 * m + 864e5, f = (new Date).getTime(); if (d < f) throw "Fragment expired"; i = setTimeout(r, d - f) } if (!c || !c["div.widget_shopping_cart_content"] || _ !== g) throw "No fragment"; n.each(c, function (e, t) { n(e).replaceWith(t) }), n(document.body).trigger("wc_fragments_loaded") } catch (w) { r() } } else r(); 0 < Cookies.get("woocommerce_items_in_cart") ? n(".hide_cart_widget_if_empty").closest(".widget_shopping_cart").show() : n(".hide_cart_widget_if_empty").closest(".widget_shopping_cart").hide(), n(document.body).on("adding_to_cart", function () { n(".hide_cart_widget_if_empty").closest(".widget_shopping_cart").show() }) });
                        }, 0)
                    }
                })
            })

            observeHeaderNav.observe(document.querySelector('.site-header'), {
                childList: true,
            })
            
        }
    },
    makeRequestSingleProduct(url) {
        fetch(url)
            .then((response) => {
                // When the page is loaded convert it to text
                return response.text()
            })
            .then((data) => {
                // Initialize the DOM parser
                const parser = new DOMParser()
                // Parse the text
                const doc = parser.parseFromString(data, "text/html")
                // Parse the document title
                //const docTitle = doc.title
                // Parse ...
                const productContent = doc.querySelector('.product.type-product')
                const page = document.querySelector('.ajax-container')
                if (productContent) {
                    const docBody = productContent.innerHTML
                    if (page) {
                        page.innerHTML = docBody
                    }
                } 
            })
            .catch(function (err) {
                console.log('Failed to fetch page: ', err);
            })
    },
    makeRequestProductList(url, layout) {
        fetch(url)
            .then((response) => {
                // When the page is loaded convert it to text
                return response.text()
            })
            .then((data) => {
                // Initialize the DOM parser
                const parser = new DOMParser()
                const doc = parser.parseFromString(data, "text/html")
                const productList = doc.querySelector('.products-list')
                const sortMenu = doc.querySelector('.sorting-menu')
                const productListReplace = document.querySelector('.products-list')
                const sortMenuReplace = document.querySelector('.sorting-menu')
                const loader = document.querySelector('.loader')
                if (productList) {
                    const docBody = productList.innerHTML
                    if (productListReplace) {
                        productListReplace.innerHTML = docBody
                        productListReplace.querySelector('.products').setAttribute('data-layout', layout)
                        productListReplace.classList.remove('visibility-hidden')
                        sortMenuReplace.innerHTML = sortMenu.innerHTML
                        setTimeout(() => {
                            //loader.classList.remove('visible')
                        }, 500)

                        this.pagination(productListReplace)
                        this.sorting(productListReplace)
                        this.sortMenu(productListReplace)
                     
                        AccordionMenu.init()
                        IOlazy.init(productListReplace, '.lazyload', 0, '0px')
                    }
                }
                // Νεότερο > Παλαιότερο /vivlia/?orderby=date&paged=1
                // Παλαιότερο > Νεότερο /vivlia/?orderby=oldest_to_recent&paged=1
                // Άνα συγγραφέα /vivlia/?orderby=author&paged=1

            })
            .catch(function (err) {
                console.log('Failed to fetch page: ', err);
            })
    },
    appPushState(url, state) {
        const pageInfo = {
            title: state,
            url: window.location.href
        }

        window.history.pushState(pageInfo, null, url)
        //console.log(window.history.state.url)
    },
    pagination(element) {
        const paginationContainer = element.querySelectorAll('.page-numbers')
        Array.from(paginationContainer).forEach(el => {
            const productsList = el.closest('.products-list').querySelector('.products')
            if (el) {
                const paginationItems = el.querySelectorAll('li')
                Array.from(paginationItems).forEach(item => {
                    const link = item.querySelector('a')
                    if (link) {
                        link.addEventListener('click', loadPage.bind(this), true)

                        function loadPage(e) {
                            e.preventDefault()
                            this.makeRequestProductList(e.target.getAttribute('href'), productsList.getAttribute('data-layout'))
                            this.appPushState(e.target.getAttribute('href'))
                        }
                    }

                })
            }
        })

        const top = document.querySelector('.site-header')
        top.scrollIntoView()
        
    },
    sorting(element) {
        const sortOptionsContainer = element.querySelector('.orderby')
        const productsList = element.closest('.products-list').querySelector('.products')

        sortOptionsContainer.addEventListener('change', (e) => {
            e.preventDefault()
            this.makeRequestProductList(window.location.origin + window.location.pathname + '?orderby=' + e.target.value, productsList.getAttribute('data-layout'))
            this.appPushState(window.location.origin + window.location.pathname + '?orderby=' + e.target.value)
        }, false)

        if (sortOptionsContainer) {
            const sortOptions = sortOptionsContainer.querySelectorAll('option')
            Array.from(sortOptions).forEach(el => {
                if (el) {
                    el.addEventListener('click', (e) => {
                    }, true)
                    //loadPage.bind(this)
                    function loadPage(e) {
                        //e.preventDefault()
                        this.makeRequestProductList(el.getAttribute('value'))
                        this.appPushState(el.getAttribute('value'))
                    }
                }

            })
        } 
    },
    sortMenu(element) {
        const sortOptionsContainer = document.querySelector('.sorting-options')
        const productsList = element.querySelector('.products')
        const sortItems = sortOptionsContainer.querySelectorAll('li a')
        Array.from(sortItems).forEach(item => {
            item.classList.remove('selected')
            item.addEventListener('click', (e) => {
                e.preventDefault()
                e.target.classList.add('selected')
                this.makeRequestProductList(e.target.getAttribute('href'), productsList.getAttribute('data-layout'))
                this.appPushState(e.target.getAttribute('href'))
            }, false)
        })

        if (window.location.search === '?orderby=date' || window.location.search === '') {
            sortOptionsContainer.querySelector('.newest-to-oldest').classList.add('selected')
        } else if (window.location.search === '?orderby=oldest_to_recent') {
            sortOptionsContainer.querySelector('.oldest-to-newest').classList.add('selected')
        } else if (window.location.search === '?orderby=author') {
            sortOptionsContainer.querySelector('.by-author').classList.add('selected')
        }
    }
}

export default CallPost