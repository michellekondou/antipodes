<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package storefront
 */

?>
<?php if ( is_page(34) ) : 
	$antipodes_team = get_field('antipodes_team');
	$tagline = get_field('tagline');
	$manifest = get_field('manifest');

	if ( has_post_thumbnail() ) {
		$thumb_id = get_post_thumbnail_id();
		$image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
		// $image_s = wp_get_attachment_image_src( get_post_thumbnail_id( $thumb_id ), 'thumbnail');
		// $image_m = wp_get_attachment_image_src( get_post_thumbnail_id( $post->the_ID()), 'medium');
		// $image_ml = wp_get_attachment_image_src( get_post_thumbnail_id( $post->the_ID() ), 'medium_large');
		// $image_l = wp_get_attachment_image_src( get_post_thumbnail_id( $post->the_ID()), 'large');
	}
?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<section class="about__house about__layout">
			<h2 class="left"><?php _e( 'Ιστορικό', 'woocommerce' ) ?></h2>
			<div class="entry-content right">
				<?php the_content() ?>
			</div>
		</section>
		<?php 
			if ($tagline) :
		?>
		<section class="about__tagline about__layout">
			<?php if (has_post_thumbnail()) : ?>
			<div class="custom-image__container">
    			<div class="custom-image__inner">
					<?php the_post_thumbnail(); ?>
				</div>
			</div>
			<?php endif; ?>
			<div class="tagline">
				<?php echo $tagline; ?>
			</div>
		</section>
		<?php endif; ?>
		<?php 
			if ($antipodes_team) :
		?>
		<section class="about__team about__layout">
			<h2 class="left"><?php _e( 'Ομάδα', 'woocommerce' ) ?></h2>
			<div class="entry-content right">
			<?php 
				if( have_rows('antipodes_team') ):
			?>
			<?php 
				while ( have_rows('antipodes_team') ) : the_row();
				$title = get_sub_field('title');
				$text = get_sub_field('text');
			?>
				<div class="col">
					<h3><?php echo $title; ?></h3>
					<p><?php echo $text; ?></p>
				</div>
			<?php endwhile; ?>
			<?php endif; ?>
			</div>
		</section>
		<?php endif; ?>
		<?php 
			if ($manifest) :
		?>
		<section class="about__manifest about__layout">
			<h2 class="left"><?php _e( 'Μανιφέστο', 'woocommerce' ) ?></h2>
			<div class="entry-content right">
				<?php echo $manifest; ?>
			</div>
		</section>
		<?php endif; ?>
	</div>
<?php elseif( is_front_page() ) : 
	$home_posts = get_field('home_posts');
	if ($home_posts) : 
?>
	<div class="home-content">
		<?php 
			foreach( $home_posts as $post ): 
				setup_postdata($post);
				if ( has_post_thumbnail() ) {
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'full');
					$image_s = wp_get_attachment_image_src( get_post_thumbnail_id( $post ), 'thumbnail');
					$image_m = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'medium');
					$image_ml = wp_get_attachment_image_src( get_post_thumbnail_id( $post ), 'medium_large');
					$image_l = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'large');
					$thumb_id = get_post_thumbnail_id();
				}
				$index_image = get_field('index_image');
				$index_cover = get_field('image_home_slider');
				$book_author = get_field('book_author');
				$book_excerpt = get_the_excerpt();
		?>
		<div class="slide section">
			<div class="home-content__image">
				<?php if( get_post_type() === 'product' ) : ?>
					<?php if ($index_cover) : ?>
					<img src="<?php echo $index_cover['url']; ?>" class="" alt="<?php $index_cover['alt']?$index_cover['alt']:the_title(); ?>">	
					<?php else : ?>
					<img src="<?php echo $image_l[0]; ?>" class=""  alt="<?php $image_l['alt']?$image_l['alt']:the_title(); ?>">	
					<?php endif; ?>
				<?php endif; ?>
				<?php if( get_post_type() === 'anagnoseis' ) : ?>
					<img src="<?php echo $image_l[0]; ?>" class=""  alt="<?php $image_l['alt']?$image_l['alt']:the_title(); ?>">	
				<?php endif; ?>
				<?php if ($index_image) : ?>
					<img src="<?php echo $index_image['url']; ?>" class=""  alt="<?php $index_image['alt']?$index_image['alt']:the_title(); ?>">
				<?php endif; ?>
			</div>
			<div class="home-content__text">
				<section class="slide__content">
				<?php the_category('','',''); ?>
					<h2>
						<a href="<?php the_permalink(); ?>" rel="bookmark" class="post-link arrow-link colored">
							<?php the_title(); ?>
							<svg class="icon icon-arrow-right-small-white">
								<use xlink:href="/wp-content/themes/antipodes/public/svg/symbols.svg#icon-arrow-right-small-white" />
							</svg>
						</a>
					</h2>
					<?php if ($book_author) : ?>
					<p class="book__author">
					<?php
						foreach( $book_author as $post) :
						setup_postdata($post);
						$author_first_name = get_field('author_first_name');
						$author_last_name = get_field('author_last_name');
					?>
					<?php echo $author_first_name . ' ' . $author_last_name; ?>
					<?php endforeach; ?>
					</p>
					<?php wp_reset_postdata(); ?>
					<?php endif; ?>
					<?php if ($book_excerpt) : ?>
					<p><?php echo $book_excerpt; ?></p>
					<?php endif; ?>
				</section>
			</div>
		</div>
		<?php endforeach; ?>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pagePiling.js/1.5.6/jquery.pagepiling.min.js" integrity="sha256-rcU19aswtHEi8sO/TRfnTaLpdi+bvMAETOY9lKSKv5E=" crossorigin="anonymous"></script>
		<?php wp_reset_postdata(); ?>
	</div>
	
	<?php endif; ?>
	
<?php elseif( is_page(37) ) : ?> 
	<div class="contact__top">
		<div class="row">
			<h1><?php _e( 'εκδόσεις <br> αντίποδες', 'woocommerce' ) ?></h1>		
			<div class="entry-content">
				<?php the_content() ?>
			</div>
		</div>
	</div>
	<?php 
		$location = get_field('google_maps');
		if( !empty($location) ):
	?>

		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQEd88522j-JGpI6Wlhux-Bj2iW89J1GY"></script>
		<script type="text/javascript">
		(function($) {

			/*
			*  new_map
			*
			*  This function will render a Google Map onto the selected jQuery element
			*
			*  @type	function
			*  @date	8/11/2013
			*  @since	4.3.0
			*
			*  @param	$el (jQuery element)
			*  @return	n/a
			*/

			function new_map( $el ) {
				
				// var
				var $markers = $el.find('.marker');
				
				
				// vars
				var args = {
					zoom		: 16,
					center		: new google.maps.LatLng(0, 0),
					mapTypeId	: google.maps.MapTypeId.ROADMAP,
					styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
				};
				
				
				// create map	        	
				var map = new google.maps.Map( $el[0], args);
				
				
				// add a markers reference
				map.markers = [];
				
				
				// add markers
				$markers.each(function(){
					
					add_marker( $(this), map );
					
				});
				
				
				// center map
				center_map( map );
				
				
				// return
				return map;
				
			}

			/*
			*  add_marker
			*
			*  This function will add a marker to the selected Google Map
			*
			*  @type	function
			*  @date	8/11/2013
			*  @since	4.3.0
			*
			*  @param	$marker (jQuery element)
			*  @param	map (Google Map object)
			*  @return	n/a
			*/

			function add_marker( $marker, map ) {

				// var
				var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

				// create marker
				var marker = new google.maps.Marker({
					position	: latlng,
					map			: map
				});

				// add to array
				map.markers.push( marker );

				// if marker contains HTML, add it to an infoWindow
				if( $marker.html() )
				{
					// create info window
					var infowindow = new google.maps.InfoWindow({
						content		: $marker.html()
					});

					// show info window when marker is clicked
					google.maps.event.addListener(marker, 'click', function() {

						infowindow.open( map, marker );

					});
				}

			}

			/*
			*  center_map
			*
			*  This function will center the map, showing all markers attached to this map
			*
			*  @type	function
			*  @date	8/11/2013
			*  @since	4.3.0
			*
			*  @param	map (Google Map object)
			*  @return	n/a
			*/

			function center_map( map ) {

				// vars
				var bounds = new google.maps.LatLngBounds();

				// loop through all markers and create bounds
				$.each( map.markers, function( i, marker ){

					var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

					bounds.extend( latlng );

				});

				// only 1 marker?
				if( map.markers.length == 1 )
				{
					// set center of map
					map.setCenter( bounds.getCenter() );
					map.setZoom( 16 );
				}
				else
				{
					// fit to bounds
					map.fitBounds( bounds );
				}

			}

			/*
			*  document ready
			*
			*  This function will render each map when the document is ready (page has loaded)
			*
			*  @type	function
			*  @date	8/11/2013
			*  @since	5.0.0
			*
			*  @param	n/a
			*  @return	n/a
			*/
			// global var
			var map = null;

			$(document).ready(function(){

				$('.acf-map').each(function(){

					// create map
					map = new_map( $(this) );

				});

			});

		})(jQuery);
		</script>
		<div class="contact__map">
			<div class="acf-map" <?php echo (!empty($location))? "style='width: 100%; height: 400px; margin: 20px 0;'":""?>>
				<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
			</div>
		</div>
	<?php endif; ?>
	<?php 
		$text_submission_text = get_field('text_submission_text');
		if( !empty($text_submission_text) ):
	?>
		<div class="contact__text">
			<h2><?php _e( 'Διαδικασία υποβολής προτάσεων και χειρογράφων', 'woocommerce' ) ?></h2>
			<article><?php echo $text_submission_text; ?></article>
		</div>
	<?php endif; ?>
	<?php 
		$contact_form = get_field('contact_form');
		$contact_form_text = get_field('contact_form_text');
		if( !empty($contact_form) ):
	?>
		<div class="contact__form">
			<h2><?php _e( 'Φόρμα <br> επικοινωνίας', 'woocommerce' ) ?></h2>
			<article><?php echo $contact_form_text; ?></article>
			<?php echo $contact_form; ?>
		</div>
	<?php endif; ?>
	
<?php else : ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	/**
	 * Functions hooked in to storefront_page add_action
	 *
	 * @hooked storefront_page_header          - 10
	 * @hooked storefront_page_content         - 20
	 */
	do_action( 'storefront_page' );
	?>

</article><!-- #post-## -->
<?php endif; ?>