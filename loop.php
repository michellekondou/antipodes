<?php
/**
 * The loop template file.
 *
 * Included on pages like index.php, archive.php and search.php to display a loop of posts
 * Learn more: https://codex.wordpress.org/The_Loop
 *
 * @package storefront
 */

//do_action( 'storefront_loop_before' );

//while ( have_posts() ) :
	//the_post();

	/**
	 * Include the Post-Format-specific template for the content.
	 * If you want to override this in a child theme, then include a file
	 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
	 */
	//get_template_part( 'content', get_post_format() );

//endwhile;

/**
 * Functions hooked in to storefront_paging_nav action
 *
 * @hooked storefront_paging_nav - 10
 */
//do_action( 'storefront_loop_after' );

if ( has_post_thumbnail() ) {
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'full');
	$image_s = wp_get_attachment_image_src( get_post_thumbnail_id( $post ), 'thumbnail');
	$image_m = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'medium');
	$image_ml = wp_get_attachment_image_src( get_post_thumbnail_id( $post ), 'medium_large');
	$image_l = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'large');
	$news_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'news_thumbnail_l');
}
?>
<article id="post-<?php the_ID(); ?>" class="vertical-grid">
	<figure class="thumbnail post-thumbnail">
		<a href="<?php the_permalink()?>" title="<?php echo the_title(); ?>">
			<img
				src="<?php echo $news_thumbnail[0]; ?>"
				class="attachment-post-thumbnail size-post-thumbnail wp-post-image lazyload"
				alt="<?php the_title(); ?>">
		</a>
	</figure>

	<div class="post-details">
		<h2 class="post-title"><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h2>
		<div class="post-taxonomies">
			<?php the_category('','',''); ?>
		</div>
		<div class="post-excerpt"><?php the_excerpt(); ?></div>
		<a href="<?php the_permalink(); ?>" rel="bookmark" class="post-link arrow-link colored">
			Δείτε περισσότερα
			<svg class="icon icon-arrow-right-small-black">
				<use xlink:href="/wp-content/themes/antipodes/public/svg/symbols.svg#icon-arrow-right-small-black" />
			</svg>
		</a>
	</div>
</article>	
