<?php
/**
 * Template used to display post content on single pages.
 *
 * @package storefront
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if ( get_post_type() == 'authors' ) : ?>
		<div class="author-top">
			<figure class="author-photo">
				<?php if ( has_post_thumbnail() ) : ?>
					<?php the_post_thumbnail(); ?>
				<?php else : ?>
					<img
						src="https://antipodes.cg-dev.eu/wp-content/uploads/author-placeholder.png"
						class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
						alt="<?php the_title(); ?>">
				<?php endif; ?>
			</figure>
			<article class="author-cv">
				<h2 class="entry-title"><?php the_field('author_first_name'); ?> <?php the_field('author_last_name'); ?></h2>
				<?php the_field('author_cv'); ?>
			</article>
		</div>
		<section class="related-books">
		<?php 
			$books = get_posts(array(
				'post_type' => 'product',
				'post_status' => 'publish',
				'meta_query' => array(
					array(
						'key' => 'book_author', // name of custom field //συγγραφεις
						'value' => '"' . get_the_ID() . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
						'compare' => 'LIKE'
					)
				)
			));
			// vars
			$field = get_field_object('gender');
			$value = $field['value'];
			if(!$value) {
				$gender = 'Male';
			} else {
				$gender = $value;
			}
		?>
        <?php if( $books ): 
			$featured_book = $books[0];
        ?>
            <h2 class="lined-heading"><span class="line"></span><span class="text"><?php if ($gender === 'Male') : ?>Βιβλία <?php else : ?>Βιβλία <?php endif; ?></span></h2>
            <div class="related-books__container">
				<?php
					$post = $featured_book;
					setup_postdata( $post );
					$book_author = get_field('book_author');
					$book_author_IDs = array();
					$link_color = get_field('book_custom_color');
					foreach( $book_author as $author ) {
						$book_author_IDs[] = $author->ID;
					}
				?>
				<?php 
					if ($link_color) :
				?>
					<style type="text/css">
						h2.book__author a,
						.tags-awards .book__posted_in a,
						.arrow-link.colored {
							color: <?php echo $link_color; ?>!important;
						}
					</style>
				<?php endif; ?>
				<article class="related-books__featured">
					<h2 class="book__title"><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h2>
					<?php 
					$book_author_IDs_total = count($book_author_IDs);
					if ($book_author_IDs_total === 1) : ?>
					<?php foreach( $book_author_IDs as $book_author_ID ) :
						$author_first_name = get_field('author_first_name', $book_author_ID);
						$author_last_name = get_field('author_last_name', $book_author_ID);
						$author_first_name_genitive = get_field('author_first_name_genitive', $book_author_ID);
						$author_last_name_genitive = get_field('author_last_name_genitive', $book_author_ID);
						$author_gender = get_field('author_gender', $book_author_ID);
						$author_slug = get_post_field( 'post_name', $book_author_ID );
						
						if ($author_first_name_genitive) {
							$author_first_name_display = $author_first_name_genitive;
						} else {
							$author_first_name_display = $author_first_name;
						}

						if ($author_last_name_genitive) {
							$author_last_name_display = $author_last_name_genitive;
						} else {
							$author_last_name_display = $author_last_name;
						}
						
						if ($author_gender === 'Male') {
							$gender_pronoun = 'του';
						} else {
							$gender_pronoun = 'της';
						}
					?>
					<h3 class="book__author"><span class="gender-pronoun"><?php echo $gender_pronoun; ?></span> <a href="<?php echo site_url() . '/authors' . '/' .$author_slug; ?>" rel="bookmark" class="colored" style="color:<?php echo $link_color;?>!important;"><?php echo $author_first_name_display . ' ' . $author_last_name_display; ?></a></h3>
					<?php endforeach; ?>
					<?php else : ?>
					<h3 class="book__author"><span class="gender-pronoun">των </span>
					<?php foreach( $book_author_IDs as $book_author_ID ) :
						$author_first_name = get_field('author_first_name', $book_author_ID);
						$author_last_name = get_field('author_last_name', $book_author_ID);
						$author_first_name_genitive = get_field('author_first_name_genitive', $book_author_ID);
						$author_last_name_genitive = get_field('author_last_name_genitive', $book_author_ID);
						$author_gender = get_field('author_gender', $book_author_ID);
						$author_slug = get_post_field( 'post_name', $book_author_ID );
						
						if ($author_first_name_genitive) {
							$author_first_name_display = $author_first_name_genitive;
						} else {
							$author_first_name_display = $author_first_name;
						}

						if ($author_last_name_genitive) {
							$author_last_name_display = $author_last_name_genitive;
						} else {
							$author_last_name_display = $author_last_name;
						}
					?>
					
					<a href="<?php echo site_url() . '/authors' . '/' .$author_slug; ?>" rel="bookmark" class="colored" style="color:<?php echo $link_color;?>!important;"><?php echo $author_first_name_display . ' ' . $author_last_name_display; ?></a><span class="comma">,</span>
					<?php endforeach; ?>
					</h3>
					<?php endif; ?>
					
					<div class="book__excerpt">
						<div class="book__excerpt__text"><?php the_excerpt(); ?></div>
						<a href="<?php the_permalink(); ?>" rel="bookmark" class="arrow-link">
							Επισκεφθείτε τη σελίδα του βιβλίου 
							<svg class="icon icon-arrow-right-small-white">
								<use xlink:href="/wp-content/themes/antipodes/public/svg/symbols.svg#icon-arrow-right-small-white" />
							</svg>
						</a>
					</div>
				</article>
				
				<div class="related-books__spines">
					<?php
						foreach( $books as $post ): 
						setup_postdata($post);
						$book_spine_image = get_field('book_spine_image_vertical');  
							if ( has_post_thumbnail() ) {
								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'full');
								$image_s = wp_get_attachment_image_src( get_post_thumbnail_id( $post ), 'thumbnail');
								$image_m = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'medium');
								$image_ml = wp_get_attachment_image_src( get_post_thumbnail_id( $post ), 'medium_large');
								$image_l = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'large');
								$thumb_id = get_post_thumbnail_id();
							}
						$book_related_author = get_field('book_author');
						$book_author_IDs = array();
						$link_color = get_field('book_custom_color');
						foreach( $book_related_author as $author ) {
							$book_author_IDs[] = $author->ID;
						}
						
					?>
					<article class="related-book__trigger">
						<?php if($book_spine_image) : ?>
							<a class="related-book__trigger__btn" href="<?php the_permalink()?>"><img class="book_spine_image" src="<?php echo $book_spine_image["url"]; ?>" alt="<?php the_title(); ?>"></a>
						<?php else : ?>
							<a class="related-book__trigger__btn" href="<?php the_permalink()?>"><img class="book_spine_image" src="https://antipodes.cg-dev.eu/wp-content/uploads/spine-placeholder-vertical-e1557913569973.jpg" alt="<?php the_title(); ?>"></a>
						<?php endif; ?>	
						<div  class="related-book__details">
							<h2 class="book__title"><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h2>
							<?php 
								$book_author_IDs_total = count($book_author_IDs);
								if ($book_author_IDs_total === 1) : ?>
							<?php foreach( $book_author_IDs as $book_author_ID ) :
								$author_first_name = get_field('author_first_name', $book_author_ID);
								$author_last_name = get_field('author_last_name', $book_author_ID);
								$author_first_name_genitive = get_field('author_first_name_genitive', $book_author_ID);
								$author_last_name_genitive = get_field('author_last_name_genitive', $book_author_ID);
								$author_gender = get_field('author_gender', $book_author_ID);
								$author_slug = get_post_field( 'post_name', $book_author_ID );
								
								if ($author_first_name_genitive) {
									$author_first_name_display = $author_first_name_genitive;
								} else {
									$author_first_name_display = $author_first_name;
								}

								if ($author_last_name_genitive) {
									$author_last_name_display = $author_last_name_genitive;
								} else {
									$author_last_name_display = $author_last_name;
								}
								
								if ($author_gender === 'Male') {
									$gender_pronoun = 'του';
								} else {
									$gender_pronoun = 'της';
								}
							?>
							<h3 class="book__author"><span class="gender-pronoun"><?php echo $gender_pronoun; ?></span> <a href="<?php echo site_url() . '/authors' . '/' .$author_slug; ?>" rel="bookmark" class="colored" style="color:<?php echo $link_color;?>;"><?php echo $author_first_name_display . ' ' . $author_last_name_display; ?></a></h3>
							<?php endforeach; ?>
							<?php else : ?>
							<h3 class="book__author"><span class="gender-pronoun">των </span>
							<?php foreach( $book_author_IDs as $book_author_ID ) :
								$author_first_name = get_field('author_first_name', $book_author_ID);
								$author_last_name = get_field('author_last_name', $book_author_ID);
								$author_first_name_genitive = get_field('author_first_name_genitive', $book_author_ID);
								$author_last_name_genitive = get_field('author_last_name_genitive', $book_author_ID);
								$author_gender = get_field('author_gender', $book_author_ID);
								$author_slug = get_post_field( 'post_name', $book_author_ID );
								
								if ($author_first_name_genitive) {
									$author_first_name_display = $author_first_name_genitive;
								} else {
									$author_first_name_display = $author_first_name;
								}

								if ($author_last_name_genitive) {
									$author_last_name_display = $author_last_name_genitive;
								} else {
									$author_last_name_display = $author_last_name;
								}
							?>
							<a href="<?php echo site_url() . '/authors' . '/' .$author_slug; ?>" rel="bookmark"><?php echo $author_first_name_display . ' ' . $author_last_name_display; ?></a><span class="comma">,</span>
							<?php endforeach; ?>
							</h3>
							<?php endif; ?>
							<div class="book__excerpt">
								<div class="book__excerpt__text"><?php the_excerpt(); ?></div>
								<a href="<?php the_permalink(); ?>" rel="bookmark" class="arrow-link">
									Επισκεφθείτε τη σελίδα του βιβλίου 
									<svg class="icon icon-arrow-right-small-white">
										<use xlink:href="/wp-content/themes/antipodes/public/svg/symbols.svg#icon-arrow-right-small-white" />
									</svg>
								</a>
							</div>
						</div>
					</article>
					<?php endforeach; ?>
					<?php wp_reset_postdata(); ?>
				</div>
			</div>
		<?php endif; ?>
		</section>
		
		<?php
		/**
		 * Advanced custom fields
		 * author_reviews
		 * 
		 */
		if( have_rows('author_reviews') ): ?>
		<section class="book-reviews">
			<h2 class="lined-heading"><span class="line"></span><span class="text"><?php _e( 'Κριτικές', 'woocommerce' ) ?></span></h2>
			<div class="main-carousel">
			<?php // loop through the rows of data
			while ( have_rows('author_reviews') ) : the_row();
				// display a sub field value
				$review_text = get_sub_field('review_text');
				$review_author = get_sub_field('review_author'); 
				$review_source = get_sub_field('review_source');
			?>
			<div class="row carousel-cell">
				<blockquote>
						<?php echo $review_text; ?>
				</blockquote>
				<p class="book-review__source"><?php echo $review_author; ?>,  <em><?php echo $review_source; ?></em></p>
			</div>
			<?php endwhile; ?>
			</div>
		</section>  
		<?php else :
			// no rows found
			endif;
		?>

		<?php
		/**
		 * Advanced custom fields
		 * author_reviews
		 * 
		 */
		if( have_rows('author_interviews') ): ?>
		<section class="book-reviews author-interviews">
			<h2 class="lined-heading"><span class="line"></span><span class="text"><?php _e( 'Συνεντεύξεις', 'woocommerce' ) ?></span></h2>
			<div class="main-carousel">
			<?php // loop through the rows of data
			while ( have_rows('author_interviews') ) : the_row();
				// display a sub field value
				$interview_title = get_sub_field('title');
				$interview_text = get_sub_field('text'); 
				$interview_url = get_sub_field('link');
			?>
			<div class="row carousel-cell">
				<h2><?php echo $interview_title; ?></h2>
				<?php echo $interview_text; ?>
				<a href="<?php echo $interview_url; ?>" rel="bookmark" class="arrow-link colored">
					Διαβάστε τη συνέντευξη 
					<svg class="icon icon-arrow-right-small-black">
						<use xlink:href="/wp-content/themes/antipodes/public/svg/symbols.svg#icon-arrow-right-small-black" />
					</svg>
				</a>
			</div>
			<?php endwhile; ?>
			</div>
		</section>  
		<?php else :
			// no rows found
			endif;
		?>

		<?php $author_related_posts = get_field('author_related_posts'); ?>
		<?php 
			/**
			 * Advanced custom fields
			 * Author Related Events
			 * 
			 */
		if( $author_related_posts ): ?>
			<section class="related-posts">
				<h2 class="lined-heading"><span class="line"></span><span class="text"><?php echo 'Σχετικές εκδηλώσεις'; ?></span></h2>
				<div class="horizontal-grid">
				<?php
					foreach( $author_related_posts as $post ): ?>
					<?php setup_postdata($post); 
						//event fields
						if ( has_post_thumbnail() ) {
							$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'full');
							$image_s = wp_get_attachment_image_src( get_post_thumbnail_id( $post ), 'thumbnail');
							$image_m = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'medium');
							$image_ml = wp_get_attachment_image_src( get_post_thumbnail_id( $post ), 'medium_large');
							$image_l = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'large');
							$news_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'news_thumbnail_m');
						}
					?>
						<article class="vertical-grid">
							<figure class="thumbnail post-thumbnail">
								<a href="<?php the_permalink()?>" title="<?php echo the_title(); ?>">
									<img
										src="<?php echo $news_thumbnail[0]; ?>"
										class="attachment-post-thumbnail size-post-thumbnail wp-post-image lazyload"
										alt="<?php the_title(); ?>">
								</a>
							</figure>
							
							<div class="post-details">
								<h2 class="post-title"><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h2>
								<div class="post-excerpt"><?php the_excerpt(); ?></div>
								<a href="<?php the_permalink(); ?>" rel="bookmark" class="post-link arrow-link colored">
									Δείτε περισσότερα
									<svg class="icon icon-arrow-right-small-black">
										<use xlink:href="/wp-content/themes/antipodes/public/svg/symbols.svg#icon-arrow-right-small-black" />
									</svg>
								</a>
							</div>		
						</article>
					<?php endforeach; ?>
					<?php wp_reset_postdata(); ?>
				</div>
			</section>
		<?php endif; ?>
	
	
	<?php elseif ( get_post_type() == 'anagnoseis' ) : ?>
		<article class="anagnoseis__intro">
			<h2><?php echo the_title(); ?></h2>
			<?php echo the_content(); ?>
		</article>
		<section class="anagnoseis__list-container visibility-hidden">
			<h3 class="screen-reader-text">Συλλογή βιβλίων στις αναγνώσεις</h3>
			<div class="anagnoseis__list">
				<?php 
					$anagnoseis_books = get_field('anagnoseis_books');
					$i = 0;
					if ($anagnoseis_books) :
					foreach( $anagnoseis_books as $post ): 
						setup_postdata($post);
						if ( has_post_thumbnail() ) {
							$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'full');
							$image_s = wp_get_attachment_image_src( get_post_thumbnail_id( $post ), 'thumbnail');
							$image_m = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'medium');
							$image_ml = wp_get_attachment_image_src( get_post_thumbnail_id( $post ), 'medium_large');
							$image_l = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'large');
							$thumb_id = get_post_thumbnail_id();
						}
						$i++;
						$book_spine_image = get_field('book_spine_image_vertical');
						
				?>	
				<article class="anagnoseis__item">
					<?php if ( has_post_thumbnail() ) : ?>
						<a href="<?php the_permalink(); ?>" class="post-thumbnail__container">
							<figure class="thumbnail post-thumbnail">
								<img
									data-src="<?php echo $image_ml[0]; ?>"
									src="<?php echo $image_ml[0]; ?>"
									class="attachment-post-thumbnail size-post-thumbnail wp-post-image book-cover"
									height="<?php echo $image_ml[2]; ?>"
									alt="<?php the_title(); ?>">
								<figcaption class="visually-hidden"><?php the_title( '<h2 class="entry-title">', '</h2>' ); ?></figcaption>
							</figure>
            </a>
						<?php else : ?>
						<a href="<?php the_permalink(); ?>" class="post-thumbnail__container">
							<img 
								data-src="https://antipodes.cg-dev.eu/wp-content/plugins/woocommerce/assets/images/placeholder.png"
								src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" 
								alt="Δείκτης τοποθέτησης"
								class="woocommerce-placeholder wp-post-image book-cover lazyload"
							>
						</a>
					<?php endif; ?>
					<div class="anagnoseis__item--menu">
						<span class="anagnoseis__item--number"><?php //echo $i; ?></span>
						<aside class="anagnoseis__item--menu-content">
							<p class="carousel-status"></p>
							<ol class="flickity-page-dots anagnoseis"></ol>
							<a href="<?php the_permalink(); ?>" class="anagnoseis__item--book-link">
								Επισκεφθείτε τη σελίδα του βιβλίου
							</a>
						</aside>
					</div>
					<div class="anagnoseis__item--excerpt">
						<h2><?php the_title(); ?></h2>
						<?php
							/**
							 * Advanced custom fields
							 * Book Reviews
							 * 
							 */
							if( have_rows('book_excerpts_anagnoseis') ):
						?>
						<div class="anagnoseis__carousel">
							<?php // loop through the rows of data
								while ( have_rows('book_excerpts_anagnoseis') ) : the_row();
								$title = get_sub_field('title');
								$text = get_sub_field('text');
							?>
							<div class="slide carousel-cell">
								<h3><?php echo $title; ?></h3>
								<?php echo $text; ?>
							</div>
							<?php endwhile; ?>
						</div>
						<?php endif; ?>
					</div>
				</article>
				<?php 
					endforeach; 
					endif;
				?>
			</div>
			<?php wp_reset_postdata(); ?>
		</section>

	<?php else : ?>
	<?php 
		$post_subtitle = get_field('post_subtitle');
		$event_title = get_field('event_title');
		$event_date = get_field('event_date');
		$event_venue = get_field('event_venue');
		$event_image = get_field('event_image');
	?>
	<figure class="post-thumbnail">
		<?php the_post_thumbnail(); ?>
	</figure>
	<div class="post-details flex">
		<aside class="post-sidebar">
      <?php if( $event_image): ?>
			<img src="<?php echo $event_image["url"]; ?>" alt="<?php echo $event_title; ?>">
  <?php endif; ?>
			<?php if ($event_date || $event_venue || $event_title) : ?>
			<hr>
			<?php endif; ?>
			<span><?php echo $event_date; ?></span>
			<span><?php echo $event_venue; ?></span>
			<br>
			<h3><?php echo $event_title; ?></h3>
		</aside>
		<article class="post-content">
			<h1><?php echo the_title(); ?></h1>
			<h2><?php echo $post_subtitle; ?></h2>
			<?php echo the_content(); ?>
		</article>
	</div>

	<?php
		$latest_post_id = '';
		$args = array(
			'posts_per_page' => 3,
			'post_type' => 'post'
		);
		$query = new WP_Query( $args );
	?>
	<?php if ( $query->have_posts()) : ?>
	<h2 class="lined-heading"><span class="line"></span><span class="text"><?php echo 'Σχετικά Άρθρα'; ?></span></h2>
	<div class="horizontal-grid">
		<?php while( $query->have_posts() ) : ?>
		<?php
			$query->the_post();
			if ( has_post_thumbnail() ) {
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'full');
				$image_s = wp_get_attachment_image_src( get_post_thumbnail_id( $post ), 'thumbnail');
				$image_m = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'medium');
				$image_ml = wp_get_attachment_image_src( get_post_thumbnail_id( $post ), 'medium_large');
				$image_l = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'large');
				$news_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post), 'news_thumbnail_m');
			}
			$post_subtitle = get_field('post_subtitle');
			$event_title = get_field('event_title');
			$event_date = get_field('event_date');
			$event_venue = get_field('event_venue');
			$event_image = get_field('event_image');
		?>
		<article  class="vertical-grid">
			<figure class="thumbnail post-thumbnail">
				<a href="<?php the_permalink()?>" title="<?php echo the_title(); ?>">
					<img
						src="<?php echo $news_thumbnail[0]; ?>"
						class="attachment-post-thumbnail size-post-thumbnail wp-post-image lazyload"
						alt="<?php the_title(); ?>">
				</a>
			</figure>

			<div class="post-details">
				<h2 class="post-title"><a href="<?php the_permalink()?>" title="<?php echo the_title(); ?>"><?php the_title(); ?></a></h2>
				<h3><?php echo $post_subtitle; ?></h3>
				<div class="post-excerpt"><?php the_excerpt(); ?></div>
				<a href="<?php the_permalink(); ?>" rel="bookmark" class="post-link arrow-link colored">
					Δείτε περισσότερα 
					<svg class="icon icon-arrow-right-small-black">
						<use xlink:href="/wp-content/themes/antipodes/public/svg/symbols.svg#icon-arrow-right-small-black" />
					</svg>
				</a>
			</div>
		</article>
		<?php endwhile; ?>
	</div>	
	<?php endif; ?>
	<?php endif; ?>

</div><!-- #post-## -->
<script src="<?php echo esc_url( site_url( '/' ) . 'wp-content/themes/antipodes/src/js/flickity.pkgd.min.js' ); ?>"></script>
<script src="<?php echo esc_url( site_url( '/' ) . 'wp-content/themes/antipodes/public/js/flickity.bundle.js' ); ?>"></script>