<?php
/**
 * Antipodes functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package antipodes
 */
// function enqueue_pollyfils() {
// 	wp_register_script( 'pollyfils', get_stylesheet_directory_uri() . '/public/assets/polyfills.bundle.js');
// 	wp_enqueue_script( 'pollyfils', false, array(), false, true );
// }
// add_action( 'wp_enqueue_scripts', 'enqueue_pollyfils' );

// function enqueue_iolazy() {
// 	wp_register_script( 'iolazy', get_stylesheet_directory_uri() . '/public/js/iolazy.bundle.js');
// 	wp_enqueue_script( 'iolazy', false, array(), false, true );
// }
// add_action( 'wp_enqueue_scripts', 'enqueue_iolazy' );

function enqueue_appjs() {
	wp_register_script( 'app', get_stylesheet_directory_uri() . '/public/assets/app.bundle.js?v=1');
	wp_enqueue_script( 'app', false, array(), null, true );

	//wp_enqueue_script( 'app', get_stylesheet_directory_uri() . '/public/assets/app.bundle.js', array(), null, true );
}
add_action( 'wp_enqueue_scripts', 'enqueue_appjs' );

function antipodes_styles() {
	//wp_enqueue_style( 'manifold-style', get_stylesheet_uri() );
    //wp_enqueue_style( 'app', get_stylesheet_directory_uri() . '/public/assets/app.css', array(), null, 'screen' );  
}
add_action( 'wp_enqueue_scripts', 'antipodes_styles' );

function get_product_list() {
	global $wp_query;
	
	$args = array(
		'post_type'             => 'product',
		'post_status'           => 'publish',
		'ignore_sticky_posts'   => 1,
		'posts_per_page'        => 4
	);
	$loop = new WP_Query( $args );

	//wp_localize_script( 'app', 'object_name', $translation_array );

	wp_localize_script( 'app', 'ajax_test_params', array(
		'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
		'posts' => json_encode( $loop->query_vars ), // everything about your loop is here
		'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
		'max_page' => $loop->max_num_pages
	) );
}

add_action( 'wp_enqueue_scripts', 'get_product_list' );

function get_product_list_ajax_handler(){
	// prepare our arguments for the query
	$args = json_decode( stripslashes( $_POST['query'] ), true );
	$args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
	$args['post_status'] = 'publish';
 
	// it is always better to use WP_Query but not here
	//query_posts( $args );

	var_dump(query_posts( $args ));
 
	// it is always better to use WP_Query but not here
	query_posts( $args );
 
	if( have_posts() ) :
 
		// run the loop
		while( have_posts() ): the_post(); 
		echo the_title();
		?>
			
			<?php //wc_get_template_part( 'woocommerce/archive-product' ); ?>

 		<?php endwhile;
 
	endif;
	die; // here we exit the script and even no wp_reset_query() required!
}
add_action('wp_ajax_get_product_list', 'get_product_list_ajax_handler'); // wp_ajax_{action}

//Change product segment to book
add_filter('gettext', 'translate_text'); 
add_filter('ngettext', 'translate_text');

function translate_text($translated) { 
	$translated = str_ireplace('Products', 'Books', $translated); 
	$translated = str_ireplace('Product Categories', 'Book Categories', $translated); 
	$translated = str_ireplace('Coupon', 'Book Coupon', $translated); 
	return $translated; 
}

add_action( 'init', 'create_post_type');


//the following is not working
function get_the_declined_date( $date_format, $timestamp = NULL ) {

    global $wp_locale;

    if ( $timestamp == NULL ) {
        $date = get_the_date( $date_format );
    } else{ 
        $date = date_i18n( $date_format, $timestamp );
    }

    if ( 'on' === _x( 'off', 'decline months names: on or off' ) ){

        $months          = $wp_locale->month;
        $months_genitive = $wp_locale->month_genitive;
         
        foreach ( $months as $key => $month ) {
            $months[ $key ] = $month;
        }
         
        foreach ( $months_genitive as $key => $month ) {
            $months_genitive[ $key ] = ' ' . $month;
        }
       
        $date = str_replace( $months, $months_genitive, $date );

        return $date;

    }else{

        return $date;
    }

}

function create_post_type() {
	/**
	 * Post Type: Authors.
	 */

	$labels = array(
		'name'               => _x( 'Authors', 'general name of the post type' ),
		'singular_name'      => _x( 'Author', 'name for one object of this post type' ),
		'add_new'            => _x( 'Add new', 'author' ),
		'add_new_item'       => __( 'Add new author' ),
		'edit_item'          => __( 'Edit Author' ),
		'new_item'           => __( 'New Author' ),
		'all_items'          => __( 'All Authors' ),
		'view_item'          => __( 'Show Author' ),
		'search_items'       => __( 'Search Author' ),
	);

	$args = array(
		"labels" => $labels,
		"public" => true,
		'show_in_rest' => true,
		'supports' =>  array( 'title', 'editor', 'thumbnail' ), // Adds support for comments, revesions, etc
     	'has_archive' =>  true, //Enables the custom post type archive at http://mysite.com/post-type/
     	'hierarchical' =>  true, //Enables the custom post type to have a hierarchy
		'rewrite' => array( 'slug' =>  _x('authors', 'URL slug')), //To be able to translate the custom post slug using WPML
	);

	register_post_type( "authors", $args );
	
	/**
	 * Post Type: Anagnoseis.
	 */

	$labels = array(
		'name'               => _x( 'Anagnoseis', 'general name of the post type' ),
		'singular_name'      => _x( 'Anagnoseis', 'name for one object of this post type' ),
		'add_new'            => _x( 'Add new', 'Anagnoseis Post' ),
		'add_new_item'       => __( 'Add new anagnoseis post' ),
		'edit_item'          => __( 'Edit Anagnoseis Post' ),
		'new_item'           => __( 'New Anagnoseis Post' ),
		'all_items'          => __( 'All Anagnoseis Posts' ),
		'view_item'          => __( 'Show Anagnoseis Post' ),
		'search_items'       => __( 'Search Anagnoseis Posts' ),
	);

	$args = array(
		"labels" => $labels,
		"public" => true,
		'show_in_rest' => true,
		'supports' =>  array( 'title', 'editor', 'thumbnail' ), // Adds support for comments, revesions, etc
     	'has_archive' =>  true, //Enables the custom post type archive at http://mysite.com/post-type/
     	'hierarchical' =>  true, //Enables the custom post type to have a hierarchy
		'rewrite' => array( 'slug' =>  _x('anagnoseis', 'URL slug')), //To be able to translate the custom post slug using WPML
	);

    register_post_type( "anagnoseis", $args );
}

//declare WooCommerce support using the add_theme_support function to override woocommerce templates
// function antipodes_add_woocommerce_support() {
//     add_theme_support( 'antipodes' );
// }
// add_action( 'after_setup_theme', 'antipodes_add_woocommerce_support' );\



/**
 * woo_hide_page_title
 *
 * Removes the "shop" title on the main shop page
 *
 * @access      public
 * @since       1.0 
 * @return      void
*/
function woo_hide_page_title() {
	
	return false;
	
}
add_filter( 'woocommerce_show_page_title' , 'woo_hide_page_title' );

/**
 * Edit single product summary area
 */
function single_product_summary_remove_hook(){
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
	//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_output_related_products', 20 );
}
add_action( 'woocommerce_single_product_summary', 'single_product_summary_remove_hook', 1 );

function after_single_product_summary_remove_hook(){
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
}
add_action( 'woocommerce_after_single_product_summary', 'after_single_product_summary_remove_hook', 1 );

/**
 * Add meta after title and contributors
 */
function single_product_summary_add_meta(){
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 5 );
}
//add_action( 'woocommerce_single_product_summary', 'single_product_summary_add_meta', 1 );

/**
 * Remove quantity for single product page
 */
function remove_qty_field() {
    global  $post;
    //get_product id
    $product = wc_get_product( $post->ID );
    //get attributes
    $attributes = $product->get_attributes();
    //product type
    $product_type = $product->is_type( 'variable' );

    //if Product is variable 
    if( $product_type && array_key_exists( 'select-quantity', $attributes ) ){
        //echo '<style type="text/css">.quantity, .buttons_added { width:0; height:0; display: none; visibility: hidden; }</style>';
    } 
     else{
      //echo '<style type="text/css">.quantity, .buttons_added { margin-bottom:15px;}</style>';
    }
}
add_filter( 'woocommerce_before_single_variation', 'remove_qty_field', 10, 2 );

/**
 * Remove product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 				// Remove the reviews tab
    //unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;
}


/**
 * Rename product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {

	//$tabs['description']['title'] = __( 'More Information' );		// Rename the description tab
	//$tabs['reviews']['title'] = __( 'Ratings' );				// Rename the reviews tab
	$tabs['additional_information']['title'] = __( 'στοιχεία βιβλίου' );	// Rename the additional information tab

	return $tabs;
}

/**
 * Edit site branding
 */
function storefront_site_branding_remove_hook(){
	remove_action( 'storefront_header', 'storefront_site_branding', 20 );
	add_action('storefront_header', 'antipodes_site_branding', 20);
}

function antipodes_site_branding(){
	// your custom navigation code goes here

		echo '<a href="/" class="antipodes-logo-white">
				<svg class="icon logo-antipodes_large" x="0px" y="0px" width="141.732px" height="141.732px" viewBox="0 0 141.732 141.732" enable-background="new 0 0 141.732 141.732" xml:space="preserve">
					<path id="circle_sb" fill="#030406" d="M127.56,70.863c0,31.314-25.383,56.695-56.697,56.695c-31.306,0-56.689-25.381-56.689-56.695
					c0-31.306,25.384-56.689,56.689-56.689C102.177,14.174,127.56,39.558,127.56,70.863z"/>
					<g>
						<path fill="#FFFFFF" d="M74.778,39.336v-1.8c-0.514,0.688-1.189,1.232-2.029,1.632s-1.683,0.6-2.531,0.6
							c-1.039,0-1.972-0.2-2.795-0.6c-0.824-0.4-1.524-0.944-2.101-1.632c-0.528-0.64-0.937-1.376-1.224-2.208
							c-0.289-0.832-0.432-1.712-0.432-2.64c0-0.928,0.131-1.78,0.396-2.556c0.264-0.776,0.7-1.516,1.308-2.22
							c1.328-1.536,2.977-2.304,4.944-2.304c0.911,0,1.751,0.196,2.519,0.588c0.77,0.393,1.416,0.964,1.945,1.716v-1.872h2.928v13.296
							H74.778z M74.683,32.665c0-0.56-0.098-1.08-0.289-1.56s-0.455-0.896-0.791-1.248c-0.336-0.352-0.74-0.628-1.213-0.828
							c-0.473-0.2-0.988-0.3-1.547-0.3c-0.593,0-1.133,0.104-1.621,0.312s-0.908,0.492-1.26,0.852c-0.352,0.36-0.623,0.792-0.815,1.296
							s-0.288,1.044-0.288,1.62c0,0.544,0.104,1.052,0.312,1.524c0.207,0.472,0.491,0.88,0.852,1.224
							c0.359,0.344,0.775,0.612,1.248,0.804c0.472,0.192,0.979,0.288,1.523,0.288c0.576,0,1.104-0.1,1.584-0.3s0.889-0.48,1.225-0.84
							c0.336-0.36,0.6-0.78,0.791-1.26S74.683,33.24,74.683,32.665z"/>
						<path fill="#FFFFFF" d="M47.958,63.336h-2.496l-5.375-13.296h3.552l3.096,8.736l3.048-8.736h3.553L47.958,63.336z"/>
						<path fill="#FFFFFF" d="M76.351,52.873v10.464h-3.191V52.873h-2.521v-2.832h8.113v2.832H76.351z"/>
						<path fill="#FFFFFF" d="M100.159,48.816h-3.096l1.439-3.48h3.287L100.159,48.816z M97.087,63.336V50.041h3.191v13.296H97.087z"/>
						<path fill="#FFFFFF" d="M41.562,87.337V76.872h-5.664v10.465h-3.192V74.04h12.049v13.297H41.562z"/>
						<path fill="#FFFFFF" d="M77.731,80.712c0,0.977-0.189,1.896-0.564,2.761c-0.377,0.864-0.885,1.612-1.525,2.243
							c-0.639,0.633-1.391,1.133-2.256,1.5c-0.863,0.369-1.783,0.553-2.76,0.553c-0.959,0-1.867-0.188-2.724-0.564
							c-0.856-0.375-1.601-0.884-2.231-1.523c-0.633-0.641-1.137-1.389-1.512-2.244c-0.377-0.855-0.564-1.764-0.564-2.725
							c0-0.991,0.188-1.92,0.564-2.783c0.375-0.864,0.887-1.616,1.535-2.256c0.648-0.641,1.408-1.145,2.28-1.512
							c0.872-0.369,1.804-0.553,2.796-0.553c0.975,0,1.889,0.188,2.736,0.564c0.848,0.375,1.584,0.888,2.207,1.535
							c0.625,0.648,1.117,1.404,1.477,2.269S77.731,79.753,77.731,80.712z M74.538,80.712c0-0.543-0.1-1.059-0.299-1.547
							c-0.201-0.488-0.477-0.912-0.828-1.272c-0.354-0.36-0.766-0.644-1.236-0.853c-0.473-0.207-0.988-0.312-1.549-0.312
							c-0.496,0-0.988,0.116-1.476,0.349c-0.488,0.231-0.908,0.523-1.26,0.875c-0.736,0.784-1.104,1.704-1.104,2.76
							c0,0.528,0.096,1.033,0.289,1.513c0.191,0.479,0.459,0.899,0.803,1.26s0.752,0.645,1.225,0.853c0.472,0.207,0.98,0.312,1.523,0.312
							c0.561,0,1.08-0.1,1.561-0.3s0.896-0.477,1.248-0.828s0.623-0.768,0.816-1.248C74.442,81.792,74.538,81.272,74.538,80.712z"/>
						<path fill="#FFFFFF" d="M109.675,80.784c0,0.912-0.18,1.785-0.541,2.616c-0.359,0.832-0.852,1.563-1.475,2.196
							c-0.625,0.632-1.357,1.137-2.197,1.512c-0.84,0.376-1.73,0.564-2.676,0.564c-0.943,0-1.832-0.185-2.664-0.553
							c-0.832-0.367-1.559-0.863-2.184-1.488c-0.623-0.623-1.119-1.352-1.488-2.184c-0.367-0.832-0.551-1.72-0.551-2.664
							c0-0.832,0.123-1.604,0.371-2.316c0.248-0.711,0.588-1.344,1.02-1.896s0.938-1.012,1.512-1.38c0.576-0.367,1.201-0.632,1.873-0.792
							l-3.049-2.063v-2.736h10.32v2.808h-5.424l1.848,1.2c0.225,0.145,0.512,0.316,0.865,0.516c0.352,0.201,0.723,0.437,1.115,0.709
							c0.393,0.271,0.783,0.592,1.176,0.959c0.393,0.369,0.748,0.793,1.068,1.272s0.58,1.024,0.779,1.632
							C109.575,79.305,109.675,80.001,109.675,80.784z M106.458,80.784c0-0.496-0.096-0.968-0.287-1.416
							c-0.191-0.447-0.453-0.84-0.779-1.176c-0.328-0.336-0.713-0.604-1.152-0.805c-0.441-0.199-0.916-0.299-1.428-0.299
							c-0.496,0-0.965,0.1-1.404,0.299c-0.441,0.201-0.824,0.469-1.152,0.805s-0.588,0.729-0.779,1.176
							c-0.193,0.448-0.289,0.92-0.289,1.416s0.096,0.969,0.289,1.416c0.191,0.448,0.451,0.836,0.779,1.164s0.711,0.588,1.152,0.78
							c0.439,0.192,0.908,0.288,1.404,0.288c0.512,0,0.986-0.096,1.428-0.288c0.439-0.192,0.824-0.452,1.152-0.78
							c0.326-0.328,0.588-0.716,0.779-1.164C106.362,81.753,106.458,81.28,106.458,80.784z"/>
						<path fill="#FFFFFF" d="M59.839,108.973c-0.297,0.568-0.693,1.061-1.188,1.477c-0.496,0.416-1.064,0.748-1.704,0.996
							c-0.641,0.247-1.312,0.372-2.016,0.372c-0.656,0-1.272-0.125-1.848-0.372c-0.576-0.248-1.08-0.564-1.513-0.948
							s-0.776-0.812-1.032-1.284s-0.384-0.932-0.384-1.38c0-0.848,0.212-1.532,0.636-2.052c0.424-0.521,0.939-0.884,1.549-1.093
							c-0.625-0.255-1.109-0.636-1.453-1.14s-0.516-1.188-0.516-2.052c0-0.464,0.116-0.924,0.348-1.38
							c0.232-0.456,0.557-0.872,0.973-1.248s0.904-0.68,1.464-0.912c0.56-0.231,1.168-0.348,1.824-0.348c0.704,0,1.372,0.124,2.004,0.371
							c0.632,0.249,1.192,0.581,1.68,0.996c0.488,0.417,0.872,0.908,1.152,1.477s0.42,1.164,0.42,1.788h-2.735
							c-0.017-0.192-0.089-0.385-0.217-0.576c-0.128-0.192-0.288-0.368-0.48-0.528c-0.191-0.159-0.412-0.288-0.659-0.384
							c-0.249-0.096-0.509-0.144-0.78-0.144c-0.561,0-1,0.136-1.32,0.407c-0.32,0.272-0.479,0.648-0.479,1.128
							c0,0.912,0.792,1.368,2.376,1.368h0.672v2.256h-0.864c-0.272,0-0.552,0.028-0.84,0.084c-0.288,0.057-0.548,0.145-0.78,0.265
							s-0.42,0.272-0.563,0.456s-0.216,0.412-0.216,0.684c0,0.464,0.188,0.84,0.563,1.128s0.844,0.433,1.404,0.433
							c0.272,0,0.528-0.049,0.769-0.145c0.239-0.096,0.447-0.224,0.623-0.384s0.32-0.336,0.432-0.528
							c0.112-0.191,0.177-0.384,0.193-0.576h2.951C60.282,107.809,60.134,108.405,59.839,108.973z"/>
						<path fill="#FFFFFF" d="M88.794,102.457c-0.688-1.152-1.713-1.729-3.072-1.729c-0.576,0-1.1,0.101-1.572,0.301
							s-0.875,0.476-1.211,0.827c-0.336,0.353-0.598,0.765-0.781,1.236c-0.184,0.473-0.275,0.988-0.275,1.548
							c0,0.593,0.104,1.104,0.312,1.536c0.207,0.433,0.496,0.812,0.863,1.141s0.82,0.6,1.355,0.815c0.537,0.216,1.125,0.404,1.766,0.564
							c0.654,0.16,1.271,0.364,1.848,0.611c0.576,0.249,1.076,0.557,1.5,0.925s0.76,0.8,1.008,1.296c0.248,0.495,0.371,1.071,0.371,1.728
							c0,0.399-0.08,0.784-0.24,1.152c-0.16,0.367-0.344,0.695-0.551,0.983c-0.24,0.336-0.514,0.648-0.816,0.937h-3.793
							c0.434-0.353,0.816-0.696,1.152-1.032c0.289-0.288,0.549-0.597,0.781-0.924c0.23-0.328,0.348-0.628,0.348-0.9
							c0-0.479-0.205-0.855-0.613-1.128c-0.406-0.272-0.971-0.512-1.691-0.72c-0.432-0.128-0.859-0.248-1.283-0.36
							c-0.424-0.111-0.836-0.235-1.236-0.372c-0.4-0.136-0.779-0.291-1.141-0.468c-0.359-0.176-0.684-0.392-0.971-0.647
							c-0.77-0.656-1.32-1.437-1.656-2.341c-0.336-0.903-0.504-1.852-0.504-2.844c0-0.976,0.188-1.888,0.562-2.736
							c0.377-0.848,0.885-1.587,1.525-2.22c0.639-0.632,1.391-1.128,2.256-1.488c0.863-0.359,1.775-0.539,2.736-0.539
							c0.734,0,1.455,0.116,2.16,0.348c0.703,0.232,1.355,0.563,1.955,0.996c0.6,0.432,1.119,0.944,1.561,1.536
							c0.439,0.592,0.756,1.248,0.947,1.968H88.794z"/>
					</g>
				</svg>
			</a>';
    
}
add_action( 'storefront_header', 'storefront_site_branding_remove_hook', 1 );

/**
 * Remove site search
 */
function storefront_product_search_remove_hook(){
    remove_action( 'storefront_header', 'storefront_product_search', 40 );
}
add_action( 'storefront_header', 'storefront_product_search_remove_hook', 1 );

/**
 * Edit header cart
 */

function storefront_header_cart_remove_hook(){
	remove_action( 'storefront_header', 'storefront_header_cart', 60 );
}
add_action( 'storefront_header', 'storefront_header_cart_remove_hook', 1 );

function antipodes_header_cart(){
    /**
	 * Display Header Cart
	 *
	 * @since  1.0.0
	 * @uses  storefront_is_woocommerce_activated() check if WooCommerce is activated
	 * @return void
	 */
	
		if ( storefront_is_woocommerce_activated() ) {
			if ( is_cart() ) {
				$class = 'current-menu-item';
			} else {
				$class = '';
			}
		?>
		<ul id="site-header-cart" class="site-header-cart menu">
			<li class="<?php echo esc_attr( $class ); ?>">
				<a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'storefront' ); ?>">
					<?php echo wp_kses_post( WC()->cart->get_cart_subtotal() ); ?> 
					<span class="count"><?php echo wp_kses_data( sprintf( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), 'storefront' ), WC()->cart->get_cart_contents_count() ) ); ?></span>
				</a>
			</li>
			<li>
				<?php the_widget( 'WC_Widget_Cart', 'title=' ); ?>
			</li>
		</ul>
			<?php
		}
}
add_action( 'storefront_header', 'antipodes_header_cart', 60 );

/**
 * Remove breadcrumb
 */
function woocommerce_breadcrumb_remove_hook(){
	remove_action( 'storefront_before_content', 'woocommerce_breadcrumb', 10 );
}
add_action( 'storefront_before_content', 'woocommerce_breadcrumb_remove_hook', 1 );

/**
 * Remove storefront secondary nav
 */

function remove_storefront_default_actions() {
	remove_action( 'storefront_header' , 'storefront_secondary_navigation' , 30 );
}
add_action( 'init', 'remove_storefront_default_actions' );

/**
 * Edit site footer
 */
function storefront_footer_remove_hook(){
	remove_action( 'storefront_footer', 'storefront_credit', 20 );
	add_action('storefront_footer', 'antipodes_footer_branding', 20);
}
add_action( 'storefront_footer', 'storefront_footer_remove_hook', 1 );

function antipodes_footer_branding(){
	// your custom navigation code goes here

	echo '<a href="/" class="antipodes-logo-white">
			<svg class="icon logo-antipodes_small" x="0px" y="0px" width="141.732px" height="141.732px" viewBox="0 0 141.732 141.732" enable-background="new 0 0 141.732 141.732" xml:space="preserve">
				<path id="circle_footer" fill="#030406" d="M127.56,70.863c0,31.314-25.383,56.695-56.697,56.695c-31.306,0-56.689-25.381-56.689-56.695
				c0-31.306,25.384-56.689,56.689-56.689C102.177,14.174,127.56,39.558,127.56,70.863z"/>
				<g>
					<path fill="#FFFFFF" d="M74.778,39.336v-1.8c-0.514,0.688-1.189,1.232-2.029,1.632s-1.683,0.6-2.531,0.6
						c-1.039,0-1.972-0.2-2.795-0.6c-0.824-0.4-1.524-0.944-2.101-1.632c-0.528-0.64-0.937-1.376-1.224-2.208
						c-0.289-0.832-0.432-1.712-0.432-2.64c0-0.928,0.131-1.78,0.396-2.556c0.264-0.776,0.7-1.516,1.308-2.22
						c1.328-1.536,2.977-2.304,4.944-2.304c0.911,0,1.751,0.196,2.519,0.588c0.77,0.393,1.416,0.964,1.945,1.716v-1.872h2.928v13.296
						H74.778z M74.683,32.665c0-0.56-0.098-1.08-0.289-1.56s-0.455-0.896-0.791-1.248c-0.336-0.352-0.74-0.628-1.213-0.828
						c-0.473-0.2-0.988-0.3-1.547-0.3c-0.593,0-1.133,0.104-1.621,0.312s-0.908,0.492-1.26,0.852c-0.352,0.36-0.623,0.792-0.815,1.296
						s-0.288,1.044-0.288,1.62c0,0.544,0.104,1.052,0.312,1.524c0.207,0.472,0.491,0.88,0.852,1.224
						c0.359,0.344,0.775,0.612,1.248,0.804c0.472,0.192,0.979,0.288,1.523,0.288c0.576,0,1.104-0.1,1.584-0.3s0.889-0.48,1.225-0.84
						c0.336-0.36,0.6-0.78,0.791-1.26S74.683,33.24,74.683,32.665z"/>
					<path fill="#FFFFFF" d="M47.958,63.336h-2.496l-5.375-13.296h3.552l3.096,8.736l3.048-8.736h3.553L47.958,63.336z"/>
					<path fill="#FFFFFF" d="M76.351,52.873v10.464h-3.191V52.873h-2.521v-2.832h8.113v2.832H76.351z"/>
					<path fill="#FFFFFF" d="M100.159,48.816h-3.096l1.439-3.48h3.287L100.159,48.816z M97.087,63.336V50.041h3.191v13.296H97.087z"/>
					<path fill="#FFFFFF" d="M41.562,87.337V76.872h-5.664v10.465h-3.192V74.04h12.049v13.297H41.562z"/>
					<path fill="#FFFFFF" d="M77.731,80.712c0,0.977-0.189,1.896-0.564,2.761c-0.377,0.864-0.885,1.612-1.525,2.243
						c-0.639,0.633-1.391,1.133-2.256,1.5c-0.863,0.369-1.783,0.553-2.76,0.553c-0.959,0-1.867-0.188-2.724-0.564
						c-0.856-0.375-1.601-0.884-2.231-1.523c-0.633-0.641-1.137-1.389-1.512-2.244c-0.377-0.855-0.564-1.764-0.564-2.725
						c0-0.991,0.188-1.92,0.564-2.783c0.375-0.864,0.887-1.616,1.535-2.256c0.648-0.641,1.408-1.145,2.28-1.512
						c0.872-0.369,1.804-0.553,2.796-0.553c0.975,0,1.889,0.188,2.736,0.564c0.848,0.375,1.584,0.888,2.207,1.535
						c0.625,0.648,1.117,1.404,1.477,2.269S77.731,79.753,77.731,80.712z M74.538,80.712c0-0.543-0.1-1.059-0.299-1.547
						c-0.201-0.488-0.477-0.912-0.828-1.272c-0.354-0.36-0.766-0.644-1.236-0.853c-0.473-0.207-0.988-0.312-1.549-0.312
						c-0.496,0-0.988,0.116-1.476,0.349c-0.488,0.231-0.908,0.523-1.26,0.875c-0.736,0.784-1.104,1.704-1.104,2.76
						c0,0.528,0.096,1.033,0.289,1.513c0.191,0.479,0.459,0.899,0.803,1.26s0.752,0.645,1.225,0.853c0.472,0.207,0.98,0.312,1.523,0.312
						c0.561,0,1.08-0.1,1.561-0.3s0.896-0.477,1.248-0.828s0.623-0.768,0.816-1.248C74.442,81.792,74.538,81.272,74.538,80.712z"/>
					<path fill="#FFFFFF" d="M109.675,80.784c0,0.912-0.18,1.785-0.541,2.616c-0.359,0.832-0.852,1.563-1.475,2.196
						c-0.625,0.632-1.357,1.137-2.197,1.512c-0.84,0.376-1.73,0.564-2.676,0.564c-0.943,0-1.832-0.185-2.664-0.553
						c-0.832-0.367-1.559-0.863-2.184-1.488c-0.623-0.623-1.119-1.352-1.488-2.184c-0.367-0.832-0.551-1.72-0.551-2.664
						c0-0.832,0.123-1.604,0.371-2.316c0.248-0.711,0.588-1.344,1.02-1.896s0.938-1.012,1.512-1.38c0.576-0.367,1.201-0.632,1.873-0.792
						l-3.049-2.063v-2.736h10.32v2.808h-5.424l1.848,1.2c0.225,0.145,0.512,0.316,0.865,0.516c0.352,0.201,0.723,0.437,1.115,0.709
						c0.393,0.271,0.783,0.592,1.176,0.959c0.393,0.369,0.748,0.793,1.068,1.272s0.58,1.024,0.779,1.632
						C109.575,79.305,109.675,80.001,109.675,80.784z M106.458,80.784c0-0.496-0.096-0.968-0.287-1.416
						c-0.191-0.447-0.453-0.84-0.779-1.176c-0.328-0.336-0.713-0.604-1.152-0.805c-0.441-0.199-0.916-0.299-1.428-0.299
						c-0.496,0-0.965,0.1-1.404,0.299c-0.441,0.201-0.824,0.469-1.152,0.805s-0.588,0.729-0.779,1.176
						c-0.193,0.448-0.289,0.92-0.289,1.416s0.096,0.969,0.289,1.416c0.191,0.448,0.451,0.836,0.779,1.164s0.711,0.588,1.152,0.78
						c0.439,0.192,0.908,0.288,1.404,0.288c0.512,0,0.986-0.096,1.428-0.288c0.439-0.192,0.824-0.452,1.152-0.78
						c0.326-0.328,0.588-0.716,0.779-1.164C106.362,81.753,106.458,81.28,106.458,80.784z"/>
					<path fill="#FFFFFF" d="M59.839,108.973c-0.297,0.568-0.693,1.061-1.188,1.477c-0.496,0.416-1.064,0.748-1.704,0.996
						c-0.641,0.247-1.312,0.372-2.016,0.372c-0.656,0-1.272-0.125-1.848-0.372c-0.576-0.248-1.08-0.564-1.513-0.948
						s-0.776-0.812-1.032-1.284s-0.384-0.932-0.384-1.38c0-0.848,0.212-1.532,0.636-2.052c0.424-0.521,0.939-0.884,1.549-1.093
						c-0.625-0.255-1.109-0.636-1.453-1.14s-0.516-1.188-0.516-2.052c0-0.464,0.116-0.924,0.348-1.38
						c0.232-0.456,0.557-0.872,0.973-1.248s0.904-0.68,1.464-0.912c0.56-0.231,1.168-0.348,1.824-0.348c0.704,0,1.372,0.124,2.004,0.371
						c0.632,0.249,1.192,0.581,1.68,0.996c0.488,0.417,0.872,0.908,1.152,1.477s0.42,1.164,0.42,1.788h-2.735
						c-0.017-0.192-0.089-0.385-0.217-0.576c-0.128-0.192-0.288-0.368-0.48-0.528c-0.191-0.159-0.412-0.288-0.659-0.384
						c-0.249-0.096-0.509-0.144-0.78-0.144c-0.561,0-1,0.136-1.32,0.407c-0.32,0.272-0.479,0.648-0.479,1.128
						c0,0.912,0.792,1.368,2.376,1.368h0.672v2.256h-0.864c-0.272,0-0.552,0.028-0.84,0.084c-0.288,0.057-0.548,0.145-0.78,0.265
						s-0.42,0.272-0.563,0.456s-0.216,0.412-0.216,0.684c0,0.464,0.188,0.84,0.563,1.128s0.844,0.433,1.404,0.433
						c0.272,0,0.528-0.049,0.769-0.145c0.239-0.096,0.447-0.224,0.623-0.384s0.32-0.336,0.432-0.528
						c0.112-0.191,0.177-0.384,0.193-0.576h2.951C60.282,107.809,60.134,108.405,59.839,108.973z"/>
					<path fill="#FFFFFF" d="M88.794,102.457c-0.688-1.152-1.713-1.729-3.072-1.729c-0.576,0-1.1,0.101-1.572,0.301
						s-0.875,0.476-1.211,0.827c-0.336,0.353-0.598,0.765-0.781,1.236c-0.184,0.473-0.275,0.988-0.275,1.548
						c0,0.593,0.104,1.104,0.312,1.536c0.207,0.433,0.496,0.812,0.863,1.141s0.82,0.6,1.355,0.815c0.537,0.216,1.125,0.404,1.766,0.564
						c0.654,0.16,1.271,0.364,1.848,0.611c0.576,0.249,1.076,0.557,1.5,0.925s0.76,0.8,1.008,1.296c0.248,0.495,0.371,1.071,0.371,1.728
						c0,0.399-0.08,0.784-0.24,1.152c-0.16,0.367-0.344,0.695-0.551,0.983c-0.24,0.336-0.514,0.648-0.816,0.937h-3.793
						c0.434-0.353,0.816-0.696,1.152-1.032c0.289-0.288,0.549-0.597,0.781-0.924c0.23-0.328,0.348-0.628,0.348-0.9
						c0-0.479-0.205-0.855-0.613-1.128c-0.406-0.272-0.971-0.512-1.691-0.72c-0.432-0.128-0.859-0.248-1.283-0.36
						c-0.424-0.111-0.836-0.235-1.236-0.372c-0.4-0.136-0.779-0.291-1.141-0.468c-0.359-0.176-0.684-0.392-0.971-0.647
						c-0.77-0.656-1.32-1.437-1.656-2.341c-0.336-0.903-0.504-1.852-0.504-2.844c0-0.976,0.188-1.888,0.562-2.736
						c0.377-0.848,0.885-1.587,1.525-2.22c0.639-0.632,1.391-1.128,2.256-1.488c0.863-0.359,1.775-0.539,2.736-0.539
						c0.734,0,1.455,0.116,2.16,0.348c0.703,0.232,1.355,0.563,1.955,0.996c0.6,0.432,1.119,0.944,1.561,1.536
						c0.439,0.592,0.756,1.248,0.947,1.968H88.794z"/>
				</g>
			</svg>
		</a>';
	
}
add_action( 'storefront_footer', 'storefront_footer_remove_hook', 1 );

/**
 * Remove storefron handheld footer bar
 */

function antipodes_remove_storefront_handheld_footer_bar() {
  remove_action( 'storefront_footer', 'storefront_handheld_footer_bar', 999 );
}

add_action( 'init', 'antipodes_remove_storefront_handheld_footer_bar' );

/**
 * Edit sorting options 
 */
function antipodes_woocommerce_catalog_orderby( $orderby ) {
	// Add "Sort by date: oldest to newest" to the menu
	// We still need to add the functionality that actually does the sorting
	$orderby['oldest_to_recent'] = __( 'Παλαιότερο > Νεότερο', 'woocommerce' );
	// Change the default "Sort by newness" to "Sort by date: newest to oldest"
	$orderby["date"] = __('Νεότερο > Παλαιότερο', 'woocommerce');
	// Add new option and rename
	$orderby['author'] = __('Ανά συγγραφέα', 'woocommerce');
	// Remove price & price-desc
	unset($orderby["price"]);
	unset($orderby["price-desc"]);
	unset($orderby["price-asc"]);
	unset($orderby["popularity"]);
	unset($orderby["rating"]);
	unset($orderby["menu_order"]);
	return $orderby;
}
add_filter( 'woocommerce_catalog_orderby', 'antipodes_woocommerce_catalog_orderby', 20 );

// Edit orderby parameters
function antipodes_woocommerce_get_catalog_ordering_args( $args ) {
	$orderby_value = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
	if ( 'oldest_to_recent' == $orderby_value ) {
		$args['orderby'] = 'meta_value';
		$args['order']   = 'ASC';
		$args['meta_key'] = 'book_first_edition';
	}

	if ( 'date' == $orderby_value ) {
		$args['orderby'] = 'meta_value';
		$args['order']   = 'DESC';
		$args['meta_key'] = 'book_first_edition';
	}

	if ( 'author' == $orderby_value ) {
		$args['orderby'] = 'meta_value';
		$args['order']   = 'ASC';
		$args['meta_key'] = 'book_author_name';
	}
	return $args;
}
add_filter( 'woocommerce_get_catalog_ordering_args', 'antipodes_woocommerce_get_catalog_ordering_args', 20 );


/**
 * Save author last name to a hidden field for sorting
 */
function save_author_name ( $post_id ) {
	echo 'here';
	$author_id = get_field( 'book_author', $post_id );
	var_dump($author_id);
	if ( $author_id ) {
		update_post_meta( $post_id, 'book_author_name', get_post_field( 'post_title', $author_id[0] ) );
	}
	
}
add_action( 'acf/save_post', 'save_author_name', 20 );

function wc_rest_api_call($id) {
	$store_api_url = 'https://antipodes.cg-dev.eu/wp-json/wc/v2';
	$consumer_key = 'ck_919e7b0bc4e3ed3632a65da3baf48bd0769421f2';
	$consumer_secret = 'cs_e4e4508abf2724d1ecb3c1d6127911760b057fc2';
	$auth_header[] = "Authorization: Basic ". base64_encode($consumer_key. ':' .$consumer_secret);
	$ch = curl_init($store_api_url. "/products" . "/" . $id);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $auth_header);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$response = curl_exec($ch);
	curl_close($ch);

	if ($response) { 
		$arr_product = json_decode($response);
		var_dump($arr_product);
	}
}

add_image_size( 'news_thumbnail_m', 586, 205, true );
add_image_size( 'news_thumbnail_l', 887, 311, true );
add_image_size( 'spine_horizontal_xld', 1342);//xlarge desktop
add_image_size( 'spine_horizontal_ld', 1183); //large desktop
add_image_size( 'spine_horizontal_md', 865); //medium desktop

function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyBVI-z9Thfqq5zxxEJWlhLz1YkJduOZ4U4');
}

add_action('acf/init', 'my_acf_init');

function set_posts_per_page_for_authors( $query ) {
  if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'authors' ) ) {
    $query->set( 'posts_per_page', '100' );
  }
}
add_action( 'pre_get_posts', 'set_posts_per_page_for_authors' );

function my_pre_get_posts( $query ) {
	
	// do not modify queries in the admin
	if( is_admin() ) {
		
		return $query;
		
	}
	// only modify queries for 'event' post type
	if(is_archive()) {

		if( isset($query->query_vars['post_type']) ) {
			if ($query->query_vars['post_type'] == 'authors') {
				$query->set('orderby', array('post_title' => 'ASC'));
			} 
		}
	}
	// return
	return $query;

}

add_action('pre_get_posts', 'my_pre_get_posts');

// order archive by title
add_filter('pre_get_posts', 'order_archive_by_title');
function order_archive_by_title($q) {
	if ( $q->is_archive && isset($q->query['post_type']) && in_array($q->query['post_type'], array('authors')) ) {
		$q->set('orderby', 'title');
		$q->set('order', 'ASC');
	}
	return $q;
}

//  function __search_by_title_only( $search, $wp_query ) {
//         global $wpdb;
//         if ( empty( $search ) )
//             return $search; // skip processing - no search term in query
//         $q = $wp_query->query_vars;
//         $n = ! empty( $q['exact'] ) ? '' : '%';
//         $search =
//         $searchand = '';
//         foreach ( (array) $q['search_terms'] as $term ) {
//             $term = esc_sql( like_escape( $term ) );
//             $search .= "{$searchand}($wpdb->posts.post_title LIKE '{$n}{$term}{$n}')";
//             $searchand = ' AND ';
//          }
//          if ( ! empty( $search ) ) {
//              $search = " AND ({$search}) ";
//              if ( ! is_user_logged_in() )
//                  $search .= " AND ($wpdb->posts.post_password = '') ";
//               }
//           return $search;
// }
// add_filter( 'posts_search', '__search_by_title_only', 500, 2 );

// /wp-json/wp/v2/products?orderby=meta_value_num&meta_key=book_first_edition

remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
remove_action( 'admin_print_styles', 'print_emoji_styles' );

//deregister plugin scripts
add_action( 'wp_print_scripts', 'deregister_js', 100 );

function deregister_js() {
	//only load contact form 7 on crete-university-press
	if ( !is_page('contact') ) {
		wp_deregister_script( 'contact-form-7' );
	}

	if ( !is_product() && !is_page('wishlist')) {
		//yith wishlist plugin
		//wp_deregister_script( 'YIT_Asset' );
		//wp_deregister_script( 'jquery-yith-wcwl' );
		wp_deregister_script( 'jquery-selectBox' );
		wp_deregister_script( 'prettyPhoto' );
		
		//wp_deregister_script( 'jquery-yith-wcwl-user' );
	}

	wp_deregister_script( 'storefront-handheld-footer-bar');
}

//deregister plugin styles
add_action( 'wp_print_styles', 'deregister_css', 100 );
function deregister_css() {
	//only load contact form 7 on crete-university-press
	if ( !is_page('contact') ) {
		wp_deregister_style( 'contact-form-7' );
	}

	if ( !is_product() && !is_page('wishlist') ) {
		//wishlist styles
		//wp_deregister_style( 'yith-wcwl-main' );
		wp_deregister_style( 'jquery-selectBox' );
		//wp_deregister_style( 'yith-wcwl-font-awesome' );
		wp_deregister_style( 'woocommerce_prettyPhoto_css' );
	}

	wp_deregister_style('storefront-fonts-css');
	wp_deregister_style( 'noticons-css' );
	wp_deregister_style( 'wpcom-notes-admin-bar-css' );
}

//add_filter( 'jetpack_implode_frontend_css', '__return_false', 99 );

// Disable Gutenberg editor.
add_filter('use_block_editor_for_post_type', '__return_false', 10);
// Don't load Gutenberg-related stylesheets.
add_action( 'wp_enqueue_scripts', 'remove_block_css', 100 );
function remove_block_css() {
    wp_dequeue_style( 'wp-block-library' ); // WordPress core
    wp_dequeue_style( 'wp-block-library-theme' ); // WordPress core
    wp_dequeue_style( 'wc-block-style' ); // WooCommerce
    wp_dequeue_style( 'storefront-gutenberg-blocks' ); // Storefront theme
}

//add_action( 'wp_enqueue_scripts', 'remove_jetpack_css', 99 );
function remove_jetpack_css() {
	wp_dequeue_style( 'storefront-jetpack-widgets' ); // Widgets

}

add_action( 'wp_enqueue_scripts', 'remove_mailchimp_js', 100 );
function remove_mailchimp_js() {
	wp_dequeue_script('mailchimp-woocommerce-public');
	wp_deregister_script( 'mailchimp-woocommerce-public' );
	wp_dequeue_script('mailchimp-for-woocommerce');
	wp_deregister_script( 'mailchimp-for-woocommerce' );
}

// Remove Google Font from Storefront theme

function antipodes_child_styles(){

wp_dequeue_style('storefront-fonts');

}

add_action( 'wp_enqueue_scripts', 'antipodes_child_styles', 999);
	

//* Enqueue scripts and styles
add_action( 'wp_enqueue_scripts', 'crunchify_disable_woocommerce_loading_css_js' );
 
function crunchify_disable_woocommerce_loading_css_js() {
 
	// Check if WooCommerce plugin is active
	if( function_exists( 'is_woocommerce' ) ){
 
		// Check if it's any of WooCommerce page
		if(! is_woocommerce() && ! is_cart() && ! is_checkout() ) { 		
			
			## Dequeue WooCommerce styles
			// wp_dequeue_style('woocommerce-layout'); 
			// wp_dequeue_style('woocommerce-general'); 
			// wp_dequeue_style('woocommerce-smallscreen'); 	
 
			## Dequeue WooCommerce scripts
			//wp_dequeue_script('wc-cart-fragments');
			// wp_dequeue_script('woocommerce'); 
			// wp_dequeue_script('wc-add-to-cart'); 
		
			// wp_deregister_script( 'js-cookie' );
			// wp_dequeue_script( 'js-cookie' );

			//wp_deregister_script( 'jquery-blockui' );
 
		}
	}	
}

//add_action( 'woocommerce_after_add_to_cart_button', 'sd_display_bulk_discount_table' );

function sd_display_bulk_discount_table() {

	global $woocommerce, $post, $product;

	$array_rule_sets = get_post_meta( $post->ID, '_pricing_rules', true );

	$tempstring = '';

	if ( $array_rule_sets && is_array( $array_rule_sets ) && sizeof( $array_rule_sets ) > 0 ) {

	$tempstring .= '<table>';

	$tempstring .= '<th>Quantity</th><th>Discount</th>';

	foreach( $array_rule_sets as $pricing_rule_sets ) {

		foreach ( $pricing_rule_sets['rules'] as $key => $value ) {

			switch ( $pricing_rule_sets['rules'][$key]['type'] ) {

			    case 'percentage_discount':

			    $woosymbol = '%';

			    break;

			    case 'price_discount':

			    $woosymbol = get_woocommerce_currency_symbol();

			    break;

			}

		$tempstring .= '<tr>';

		$tempstring .= '<td>'.$pricing_rule_sets['rules'][$key]['from']." - ".$pricing_rule_sets['rules'][$key]['to']."</td>";

		$tempstring .= '<td><span class="amount">' . $pricing_rule_sets['rules'][$key]['amount'] . "" . $woosymbol . "</span></td>";

		$tempstring .= '</tr>';


		}

	}

	$tempstring .= "</table>";

	echo $tempstring;

	}

}

function antipodes_remove_product_zoom_support() {
	remove_theme_support( 'wc-product-gallery-zoom' );
}
add_action( 'after_setup_theme', 'antipodes_remove_product_zoom_support', 100 );

//disable shipping calculator
// 1 Disable State
add_filter( 'woocommerce_shipping_calculator_enable_state', '__return_false' );
 
// 2 Disable City
add_filter( 'woocommerce_shipping_calculator_enable_city', '__return_false' );
 
// 3 Disable Postcode
add_filter( 'woocommerce_shipping_calculator_enable_postcode', '__return_false' );

/**
 * Hide shipping rates when free shipping is available.
 * Updated to support WooCommerce 2.6 Shipping Zones.
 *
 * @param array $rates Array of rates found for the package.
 * @return array
 */
function my_hide_shipping_when_free_is_available( $rates ) {
	$free = array();

	foreach ( $rates as $rate_id => $rate ) {
		if ( 'free_shipping' === $rate->method_id ) {
			$free[ $rate_id ] = $rate;
			break;
		}
	}

	return ! empty( $free ) ? $free : $rates;
}

add_filter( 'woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 100 );

//add_filter( 'get_the_terms', 'custom_product_cat_terms', 20, 3 );
function custom_product_cat_terms( $terms, $post_id, $taxonomy ){
    // HERE below define your excluded product categories Term IDs in this array
    $category_ids = array( 42, 43 );

    if( ! is_product() ) // Only single product pages
        return $terms;

    if( $taxonomy != 'product_cat' ) // Only product categories custom taxonomy
        return $terms;

    foreach( $terms as $key => $term ){
        if( in_array( $term->term_id, $category_ids ) ){
            unset($terms[$key]); // If term is found we remove it
        }
    }
    return $terms;
}


add_action(
    'after_setup_theme',
    function() {
        add_theme_support( 'html5', [ 'script', 'style' ] );
    }
);


//Implement Facebook pixel

function add_facebook_pixel(){
?>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '367639397433505');
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1"
src="https://www.facebook.com/tr?id=367639397433505&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<?php
}
add_action('wp_head', 'add_facebook_pixel');




// FB PiXELS Footer script Events
add_action( 'wp_footer', 'fbpixels_add_to_cart_script' );
function fbpixels_add_to_cart_script(){

    // HERE set the product currency code
    $currency = 'EUR';

    ## 0. Common script -  On ALL Pages
    ?>
    <script>
    fbq('track', 'ViewContent');
    <?php

    ## 1. On Checkout page
    if ( ! is_wc_endpoint_url( 'order-received' ) && is_checkout() ) {
    ?>
    fbq('track', 'InitiateCheckout', {
     value: 0.00,
    currency: 'EUR',
    });
    <?php

    ## 2. On Order received (thankyou)
    } elseif ( is_wc_endpoint_url( 'order-received' ) ) {
    global $wp;

    // Get the Order ID from Query vars
    $order_id  = absint( $wp->query_vars['order-received'] );

    if ( $order_id > 0 ){

    // Get an instance of the WC_Order object
    $order = wc_get_order( $order_id );
    $item_sku = array();
    $order = wc_get_order( $order_id ); 

    foreach ($order->get_items() as $item) {
    
      $item_sku[] = $item->get_product()->get_sku();
    }
    ?>
    fbq('track', 'Purchase', {
        value:    <?php echo $order->get_total(); ?>,
        currency: 'EUR',
        content_ids: '<?php echo implode(", ", $item_sku);?>'
    });
    
    <?php
    }
    ## 3. Other pages - (EXCEPT Order received and Checkout)
    }

    ?>
   
    <?php
    
    ## For single product pages - Normal add to cart
    if( is_product() && isset($_POST['add-to-cart']) ){
        global $product;
         $product_id;
        // variable product
        if( $product->is_type('variable') && isset($_POST['variation_id']) ) {
            $product_id =  $product->get_sku(); //$_POST['variation_id'];
            $price = number_format( wc_get_price_including_tax( wc_get_product($_POST['variation_id']) ), 2 );
        }

        // Simple product
        elseif ( $product->is_type('simple') ) {
            $product_id = $product->get_sku(); 
            $price = number_format( wc_get_price_including_tax( $product ), 2 );
        }
        $quantity = isset($_POST['quantity']) ? $_POST['quantity'] : 1;

        ?>
        fbq('track', 'AddToCart', {
            value:    <?php echo $price; ?>,
            currency: '<?php echo $currency; ?>',
            content_ids: '<?php echo $product_id?>'
        });
        <?php
    }
    ?>
    </script>
    <?php
}